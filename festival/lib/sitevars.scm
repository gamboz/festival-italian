(if (getenv 'FESTIVAL) 
	(set! rootFESTIVAL (getenv 'FESTIVAL))
	(set! rootFESTIVAL "../../"))	   

;;PATH VOCI
(set! system-voice-path (list (path-as-directory  (string-append rootFESTIVAL "festival/lib/voices"))))

;;FILE TEMPORANEI
(set! temp_file_dir (string-append rootFESTIVAL "temp/")) 

;;PATH LIBRERIE
(set! lib_scm_dir (string-append rootFESTIVAL "festival/lib/italian_scm/"))

;;MODULO MBROLA
(set! mbrola_progname (string-append rootFESTIVAL "festival/bin/mbrola.exe")) 

;;PATH DATABASE MBROLA
(set! mbrola_dbs (string-append rootFESTIVAL "festival/lib/voices/italian/"))

;; MODALITA' DEBUG:
(set! debug_mode nil)

;; TEMPORARY
(require 'it_define)

