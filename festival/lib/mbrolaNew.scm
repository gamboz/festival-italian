(print "Sono in E:/Italian/italian_scm/italian_mbrola.scm")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Support for MBROLA as an external module.
;;;


(define (return_tmp_file)
  "Apertura file nella directory temporanea del sistema"
  (if (defvar temp_file_dir)
      (string-append temp_file_dir (basename (make_tmp_filename)))
      (make_tmp_filename))) 

(define (return_mbrola_dbs)
  (if (defvar mbrola_dbs)
      mbrola_dbs
      (if (getenv 'MBROLA_DBS)
	  (getenv 'MBROLA_DBS)
	  (cdr (assoc current-voice voice-locations)))))

;;Se non e' definita la variabile usa mbrola di default
(if (not (defvar mbrola_progname))    
      (set! mbrola_progname "mbrola"))

(define (FileXml2Wav xmlFile)
  "Sintesi con intonazione e prosodia + save wav: 
Utilizzo: (FileXml2Wav  xmlFile)"
  (set! text_mode t)
  (set! APML t)

  (defvar conceptUtt (Utterance Concept nil))
  (utt.load conceptUtt xmlFile)
  (if (defvar debug_mode) 
      (if debug_mode 
	  (let ()
	    (print "Relation_Tree Affective: " )
	    (print (utt.relation_tree conceptUtt 'Affective))
	    (print "Relation_Tree SemStructure: " )
	    (print (utt.relation_tree conceptUtt 'SemStructure)))))
  (set! frasetxt (utt.synth conceptUtt))

  (set! text_mode nil)
  (set! APML nil) 
  conceptUtt
  )

(define (MBROLA_Synth utt)
  "(MBROLA_Synth UTT)
  Synthesize using MBROLA as external module.  Basically dump the info
  from this utterance. Call MBROLA and reload the waveform into utt.
  [see MBROLA]"
  
  (set! phoFileT  (string-append temp_file_dir "TempFileX.pho"))
  (set! wavFileT  (string-append temp_file_dir "TempFile.wav"))
  
    (let ()
    (save_segments_mbrola utt phoFileT)
    (set! comando (string-append mbrola_progname " -e " 
			   mbrola_database " "
			   phoFileT " "
			   wavFileT))
    	;(print comando)
	(system comando)
 
    (utt.import.wave utt wavFileT)    
    (apply_hooks after_synth_hooks utt)    
    (if (defvar debug_mode) 
	(if debug_mode 
	    (let ()
    		(print comando)
	  	(print (string-append "Sintetizzato il file " wavFileT))
		)
	) ; if debug_mode
     ) ; end if (defvar debug_mode
    utt))



(define (FileXml2WavOld xmlFile wavFile)
  "Sintesi con intonazione e prosodia + save wav: 
Utilizzo: (FileXml2Wav  xmlFile wavFile)"
  (set! text_mode t)
  (set! APML t) 
  (defvar conceptUtt (Utterance Concept nil))
  (utt.load conceptUtt xmlFile)
  (if (defvar debug_mode) 
      (if debug_mode 
	  (let ()
	    (print "Relation_Tree Affective: " )
	    (print (utt.relation_tree conceptUtt 'Affective))
	    (print "Relation_Tree SemStructure: " )
	    (print (utt.relation_tree conceptUtt 'SemStructure)))))
  (set! frasetxt (utt.synth conceptUtt))
  ;;(utt.play frasetxt)
  (utt.save.wave frasetxt wavFile)
  (if (defvar debug_mode) 
      (if debug_mode 
	  (print (string-append "Sintetizzato il file " wavFile))
  	))
  (set! text_mode nil)
  (set! APML nil) 
  conceptUtt
  )

(define (MBROLA_SynthOld utt)
  "(MBROLA_Synth UTT)
  Synthesize using MBROLA as external module.  Basically dump the info
  from this utterance. Call MBROLA and reload the waveform into utt.
  [see MBROLA]"
  
  (let ((filename (return_tmp_file)))
    (set! filePho (string-append filename ".pho"))
    (save_segments_mbrola utt filePho)
    (set! fileWav (string-append filename ".wav"))
    (system (string-append mbrola_progname " " 
			   mbrola_database " "
			   filePho " "
			   fileWav))
    (utt.import.wave utt fileWav)    
    (apply_hooks after_synth_hooks utt)    
    (if (defvar debug_mode) 
	(if debug_mode 
	    (let ()
	      (print (string-append "Mbrola progname: " mbrola_progname))
	      (print (string-append "Mbrola database: " mbrola_database))
	      (print (string-append "File segmenti: " filePho))
	      (print (string-append "File audio: " fileWav)))
	    (begin
    		(delete-file filePho)
    		(delete-file fileWav))
	) ; if debug_mode
     ) ; end if (defvar debug_mode
    utt))

(define (save_segments_mbrola utt filename)
  "(save_segments_mbrola UTT FILENAME)
  Save segment information in MBROLA format in filename.  The format is
  phone duration (ms) [% position F0 target]*. [see MBROLA]"
   
  (if (and (defvar text_mode) text_mode)
      (begin
	(set! word_var_old " ")
	) ; begin
      )
	
  (let ((fd (fopen filename "w")) new_seg_name)
    (mapcar
     (lambda (segment)
 
       (set! old_seg_name (item.feat segment 'name))  
       
       (if (equal? old_seg_name "#")    ;Modifica per mappare # in _
	   (set! new_seg_name "_")
	   (begin
	     (set! new_seg_name old_seg_name)
	     
	     (if (and (defvar text_mode) text_mode) 
		 (begin
		   ;;(set! tempvar (item.next tempvar))
		   ;;(print (item.name segment))
		   (set! sylvar (item.relation.parent segment 'SylStructure)) 
		   (set! wordvar (item.relation.parent sylvar 'SylStructure)) 
		   (set! word_var_name (item.feat wordvar 'name))
		   
		   (set! semstruct (item.relation.parent wordvar 'SemStructure)) 	; ok
		   (set! affect (item.relation.parent wordvar 'Affective))
		   (if (equal? affect nil)
		       (set! tag_name (item.feat semstruct 'name))	; ok
		       ;;(set! tag_name (item.feat affect 'name))
		       (set! tag_name (item.feat affect 'type))
		       )  ; end if
		   
  		   (if (defvar debug_mode) 
      			(if debug_mode 
			(print (string-append "Segmento= " old_seg_name "  Parola= " word_var_name  "  tag= " tag_name))))

		   (if (equal? word_var_name word_var_old)
		       ()
		       (begin
  		   	(if (defvar debug_mode) 
      				(if debug_mode 
			 	(print (string-append word_var_name " " word_var_old))))
			 (format fd "; word= %s   tag= %s" word_var_name  tag_name)
			 (terpri fd)
			 (set! word_var_old word_var_name)
			 )
		       )

		   )
		 )
	     ) ; fine begin
	   ) ; fine if
	 
       (set! par (item.relation.parent segment 'Segctrl))
    

       (save_seg_mbrola_entry 
	new_seg_name
	(item.feat segment 'segment_start)
	(item.feat segment 'segment_duration)
	(mapcar
	 (lambda (targ_item)
	   (list
	    (item.feat targ_item "pos")
	    (item.feat targ_item "f0")))
	 
	 (item.relation.daughters segment 'Target)) ;; list of targets

	(if par 
	    (list 

	     (list "Vol" (item.feat par "Vol"))
	     (list "SpTilt" (item.feat par "SpTilt"))
	     (list "Shim" (item.feat par "Shim"))
	     (list "Jit" (item.feat par "Jit"))
	     (list "AspNoise" (item.feat par "AspNoise"))
	     (list "F0Flut" (item.feat par "F0Flut"))
	     (list "AmpFlut" (item.feat par "AmpFlut"))
	     (list "SpWarp" (item.feat par "SpWarp"))
	     ) ;fine list
	    ); fine if
	fd))
     (utt.relation.items utt 'Segment))
    
    (fclose fd)))


(define (save_seg_mbrola_entry name start dur targs emotive fd)
  "(save_seg_mbrola_entry ENTRY NAME START DUR TARGS EMOTIVE FD)
  Entry contains, (name duration num_targs start 1st_targ_pos 1st_targ_val emotive) where emotive is a list of low-level controls."
  
  (format fd "%s %d " name (nint (* dur 1000)))
  (if targs     ;; if there are any targets
      (mapcar
       (lambda (targ) ;; targ_pos and targ_val
	 (let ((targ_pos (car targ))
	       (targ_val (car (cdr targ))))
	   
	   (format fd "%d %d " 
		   (nint (* 100 (/ (- targ_pos start) dur))) ;; % pos of target
		   (nint (parse-number targ_val)))           ;; target value
	   ))
       targs))
  
  
  (if emotive 
      (mapcar
       (lambda (x)
         (set! y (car (cdr x))) ;y = valore della feature
	 (if (not (equal? y 0)) (format fd "%s 0 %1.5f 100 %1.5f " (car x) y y) )
	 ) ;fine lambda
       emotive
       ); fine mapcar
      );fine if emotive
  
  
  (terpri fd) ;; 1 mandata a capo
  )


(provide 'italian_mbrola)
