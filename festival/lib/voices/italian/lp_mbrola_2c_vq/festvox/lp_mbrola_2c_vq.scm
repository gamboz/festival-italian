;;LP MBROLA 2C VQ
;;; Add the directory contains general italian stuff to load-path
(defvar lp_mbrola_2c_vq_dir (cdr (assoc 'lp_mbrola_2c_vq voice-locations)))

(set! load-path (cons (path-append lp_mbrola_2c_vq_dir "festvox/") load-path))

;; for common italian modules
(if (defvar lib_scm_dir)
    (set! load-path (cons lib_scm_dir load-path))
    (set! load-path (cons (path-append libdir "italian_scm/") load-path))
    )

;Load italian scm files
(require 'italian_require)

;APML voice quality maps
(require 'maps_lp)

;; CART files...
(require 'ifd_it_car-tree_dur-2c_dur_MATLAB_t0)
(require 'tree_PaIntE_CW_vq_mel_NOTOBI)


;;VOICE ResEt....
(define (italian_voice_reset)
  "(italian_voice_reset)
Reset global variables back to previous voice."
  (set! token.prepunctuation italian_previous_tok_prepunc)
  (set! token.punctuation italian_previous_tok_punc) ;F_MOD per l'italiano devo toglier l'accento ' dalla punteggiatura e non dalla prepunteggiatura
  (load (path-append libdir "synthesis.scm"));;per togliere i set dei moduli da noi utilizzati
)

;;;  Full voice definition 
(define (voice_lp_mbrola_2c_vq)
"(voice_italian_lp)
Set up synthesis for Female Italian speaker: Loredana Panato"
  (voice_reset)  
  (Parameter.set 'Language 'italian)

  ;; Phone set
  (Parameter.set 'PhoneSet 'italian)
  (PhoneSet.select 'italian)

  ;; numeric expansion
  (Parameter.set 'Token_Method 'Token_Any)
  (set! token_to_words italian_token_to_words)

  ;; Because of use of ' for accents remove it from prepunctuation
  (set! italian_previous_tok_prepunc token.prepunctuation)
  (set! italian_previous_tok_punc token.punctuation)
  (set! token.prepunctuation "\"'`({[")  ;)F_MOD ;default "\"'`({[")
  (set! token.punctuation "\".,:;!?(){}[]") ;F_MOD ;default "\"'`.,:;!?(){}[]" ....fabio ho tolto '`
  (set! token.whitespace " \t\n\r") ;fabio ha tolto il meno da whithe space per i numeri
   
  ;; Postlexical rules
  (set! postlex_vowel_reduce_cart_tree  nil)
  (set! postlex_rules_hooks nil)

  ;; No pos prediction (get it from lexicon)
  (set! pos_lex_name nil)
  (set! pos_supported nil) ;; well not real pos anyhow

  ;; Lexicon selection
  ;;(lex.select "italian")
  (lex.select "otherlex")  
  ;(lex.select "otherlex_irst")

  ;;Part of speech Function Word
  (set! guess_pos italian_guess_pos)
  
  ;; Phrasing prediction
  (Parameter.set 'Phrase_Method 'cart_tree)
  (set! phrase_cart_tree italian_phrase_cart_tree)
   
  ;; Pauses prediction
  (Parameter.set 'Pause_Method Classic_Pauses_mB)
  
  ;; Accent and tone prediction
  ;;(Parameter.set 'Int_Method 'Intonation_Tree)
  (Parameter.set 'Int_Method 'General)
  (set! int_accent_cart_tree simple_italian_accent_tree)

  ;; Duration prediction ;;;;;;;;;;;;;;
  (Parameter.set 'Duration_Method 'Tree_ZScores)
  (set! duration_ph_info ifd_it_car-tree_dur-2c::phone_durs) 
  (set! duration_cart_tree ifd_it_car-tree_dur-2c::zdur_tree)

  ;; Predizione 'CodeWord
  (set! PaIntE_CW_vq_tree carini_PaIntE_CW_vq_tree_2c)
  ;;Target f0 prediction
  (Parameter.set 'Int_Target_Method 'painte_to_f0)
  (set! mel_streach 1.0) ; 2.2;1.9
  (set! hz_streach 1.0);2.15) ; 2.2;1.9
  (set! pitch_male2female_conversion t)
  (Param.set 'default_start_f0 (hz2mel 104.792))

  ;(Param.set 'default_pr_base '60) ;;'60)
  ;(Param.set 'default_pr_top '350)
  
  ;; Waveform synthesizer: diphones
  ;; Waveform synthesizer: MBROLA it4 diphones
  (Parameter.set 'Synth_Method MBROLA_Synth)
  ;(set! mbrola_progname "mbrola")
  (set! mbrola_database 
	(format 
	 nil
	 "%s%s"
	  (return_mbrola_dbs) "it4/it4"
	 ))

  (require 'italian_module_flush_2c_vq)
   ;; set callback to restore some original values changed by the italian voice
  (set! current_voice_reset italian_voice_reset)
  (set! current-voice 'lp_mbrola_2c_vq)
)

 

(proclaim_voice
 'lp_mbrola_2c_vq
 '((language italian)
   (gender female)
   (dialect none)
   (description
    "This voice provides a Italian female voice using a MBROLA diphone synthesis method.  The lexicon
     is provived by a set of letter to sound rules producing pronunciation
     accents and syllabification.  The durations, intonation and
     prosodic phrasing are minimal but are acceptable for simple
     examples.")))

(provide 'lp_mbrola_2c_vq)
