;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Sequenza dei moduli da utilizzare per l'Italiano 
;;; 


(defUttType Text
  (Initialize utt)
  (Text utt)
  (Token_POS utt)
  (Token utt)
  (Token_punct utt);FA
  (Function_Word utt);FA
  (POS utt)
  (Phrasify utt)
  (Phrase_Type utt);;FA
  (Word utt)
  (Word_dopo_num utt);FA
  (Word_split_pos utt);FA
  (Pauses utt)
  (Intonation utt)
  (Position_FW utt);;FA
  ;(My_Pauses utt);per il momento no pause prima di tutte FW ;;FA
  (PostLex utt)
  (Duration utt)
  (Int_Targets utt)
  (MAP_segments utt);FA
  (Wave_Synth utt)
  ;(Stampa utt) ;;FA
  )


(defUttType Tokens   ;; This is used in tts_file, Tokens will be preloaded
  (Token_POS utt)    ;; when utt.synth is called
  (Token utt)
  (Token_punct utt);FA
  (Function_Word utt);FA        
  (POS utt)
  (Phrasify utt)
  (Phrase_Type utt);;FA
  (Word utt)
  (Word_dopo_num utt);FA
  (Word_split_pos utt);FA
  (Pauses utt)
  (Intonation utt)
  (Position_FW utt);;FA
  (PostLex utt)
  (Duration utt)
  (Int_Targets utt)
  (MAP_segments utt);FA
  (Wave_Synth utt)
  )
  
  
  (defUttType Concept  ;; rather gradious name for when information has
  	;; been preloaded (probably XML) to give a word
  	;; relation (SOLE and APML uses this) 
        ;; 
  	;(print "Concept")
  	;(print "Function_Word")
  	(Function_Word utt);FA
  	;(print "POS")
  	(POS utt)
  	;(print "Phrasify")
  	(Phrasify utt)
  	;(print "Phrase_Type")
  	(Phrase_Type utt);;FA
  	;(print "Word")
  	(Word utt)
  	;(print "Pauses")
  	(Pauses utt)
  	;(print "Intonation")
  	(Intonation utt)
  	;(print "Position_FW")
  	(Position_FW utt);;FA
  	;(print "PostLex")
  	(PostLex utt)
  	;(print "Duration ")
  	(Duration utt)
  	;(print "Int_Targets")
  	(Int_Targets utt)
	;(print "MAP_segments")
  	(MAP_segments utt);FA
  	;(print "Wave_Synth")
	(EmotivePostProcessing utt)		;; support for vsml (Enrico Marchetto)
  	(Wave_Synth utt))
    
  
  
  
  
;non mettere il (provide 'italian_module_flush) perch� deve caricarlo ogni volta. 