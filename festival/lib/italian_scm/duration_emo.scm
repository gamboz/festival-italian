
(define (Duration_DScores utt)
"(Duration_DZScores utt)
Predicts duration to segments using the CART tree in duration_logzscore_tree
and duration_logzscore_tree_silence which produces a zscore of the log
duration.  The variable duration_logzscore_ph_info contains (log) means
and std for each phone in the set."
  (let ((silence (car (car (cdr (assoc 'silences (PhoneSet.description))))))
	ldurinfo)
    (mapcar
     (lambda (s)
       (set! Ndurinfo
	     (wagon s duration_cart_tree))
       (set! Edurinfo  
	     (wagon s Eduration_cart_tree))
       
       (set! Estd (- (car (last Ndurinfo))   (car (last Edurinfo)) ))

       (if (defvar debug_mode) 
	   (if debug_mode 
	       (format t "Ndurinfo %s Edurinfo %s Estd %s\n" (car (last Ndurinfo)) (car (last Edurinfo)) Estd)))
       
;       (set! stdmin -1)
;       (set! stdmax 1)

;       (if (< Estd stdmin)
;	   (set! Estd stdmin))

;       (if (> Estd stdmax)
;	   (set! Estd stdmax))
	   
 
      ; (format t "%s %s %s\n" (car (last Ndurinfo)) (car (last Edurinfo)) Estd)
       
       (set! Ndur (duration_unzscore 
		   (read-from-string (item.name s))
		   (car (last Ndurinfo))
		   duration_ph_info))
       (set! Edur (duration_unzscore 
		   (read-from-string  (item.name s))
		   Estd 
		   Eduration_ph_info))

       (if (defvar debug_mode) 
		   (if debug_mode 
		       (format t "Ndur %s Edur %s \n" Ndur Edur)))

       (set! dur (+ (* Efactor Edur) (* (- 1 Efactor) Ndur)))
       (set! dur (* dur (duration_find_stretch s)))
       ;(print dur)
       (if (< dur 0.05)
	   (set! dur 0.05))

       ;(print (item.feat s "p.end"))
       (item.set_feat s "end" (+ dur (item.feat s "p.end")))
       ;(print (item.feat s 'end))
       )

     (utt.relation.items utt 'Segment))
    utt))

(set! seg_dur_info  duration_ph_info)

(define (zscore_dur seg)
  (let ((di (assoc_string (item.name seg) seg_dur_info)))
    (cond
     ((not di)
      (format stderr "zscore_dur: %s no info found\n" 
	      (item.name seg))
      (/ (- 0.100
	    (car (cdr di)))
	 (car (cdr (cdr di))))      )
     (t
      (/ (- (item.feat seg "segment_duration")
	    (car (cdr di)))
	 (car (cdr (cdr di))))))))

