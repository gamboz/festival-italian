;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Sequenza dei moduli da utilizzare per l'Italiano 
;;; 

(defUttType Text
  ;(print "Inizialize")
  (Initialize utt)
  ;(print "Text")
  (Text utt)
  ;(print "Token_POS")
  (Token_POS utt)
  ;(print "Token")
  (Token utt)
  ;(print "Token_punct")
  (Token_punct utt);FA
  ;(print "POS")
  (POS utt)
  ;(print "Phrasify")
  (Phrasify utt)
  ;(print "Phrase_type")
  (Phrase_Type utt);;FA
  ;(print "Word")
  (Word utt)
  ;(print "Word_dopo_num")
  (Word_dopo_num utt);FA
  ;(print "Word_split_pos")
  (Word_split_pos utt);FA
  
  ;(print "Pauses")
  (Pauses utt)
  ;(print "Intonation")
  (Intonation utt)
  ;(print "PostLex")
  (PostLex utt)
  ;(print "Duration")
  (Duration utt)
  ;(print "PaIntE_CW_NUM_syl")
  (PaIntE_CW_vq_syl utt);(PaIntE_CW_NUM_syl utt)
  ;(print "Set_PaIntE_CW_NUM_syl")
  ;(Set_PaIntE_CW_NUM_syl utt)
  ;(print "painte_pr_denorm_events")
  ;(painte_pr_denorm_events utt)
  ;(print "Int_Targets")
  (Int_Targets utt)
  (mel2frq_target utt)
  ;(print "MAP_segments")
  (MAP_segments utt);FA
  ;(print "Wave_Synth")
  (Wave_Synth utt))


(defUttType Tokens   ;; This is used in tts_file, Tokens will be preloaded
  ;; when utt.synth is called
  ;(print "Token_POS")
  (Token_POS utt)
  ;(print "Token")
  (Token utt)
  ;(print "Token_punct")
  (Token_punct utt);FA
  ;(print "POS")
  (POS utt)
  ;(print "Phrasify")
  (Phrasify utt)
  ;(print "Phrase_type")
  (Phrase_Type utt);;FA
  ;(print "Word")
  (Word utt)
  ;(print "Word_dopo_num")
  (Word_dopo_num utt);FA
  ;(print "Word_split_pos")
  (Word_split_pos utt);FA
  
  ;(print "Pauses")
  (Pauses utt)
  ;(print "Intonation")
  (Intonation utt)
  ;(print "PostLex")
  (PostLex utt)
  ;(print "Duration")
  (Duration utt)
  ;(print "PaIntE_CW_NUM_syl")
  (PaIntE_CW_vq_syl utt) ;(PaIntE_CW_NUM_syl utt)
  ;(print "Set_PaIntE_CW_NUM_syl")
;  (Set_PaIntE_CW_NUM_syl utt)
  ;(print "painte_pr_denorm_events")
  ;(painte_pr_denorm_events utt)
  ;(print "Int_Targets")
  (Int_Targets utt)
(mel2frq_target utt)
  ;(print "MAP_segments")
  (MAP_segments utt);FA
  ;(print "Wave_Synth")
  (Wave_Synth utt))

  
   (defUttType Concept  
  ;; rather gradious name for when information has
  ;; been preloaded (probably XML) to give a word
  ;; relation (SOLE uses this)
  ;(print "POS")
(POS utt)
  ;(print "Phrasify")
  (Phrasify utt)
  ;(print "Phrase_type")
  (Phrase_Type utt);;FA
  ;(print "Word")
  (Word utt)
  ;(print "Word_dopo_num")
  ;(Word_dopo_num utt);FA
  ;(print "Word_split_pos")
  (Word_split_pos utt);FA
 
  ;(print "Pauses")
  (Pauses utt)
  ;(print "Intonation")
  (Intonation utt)
  ;(print "PostLex")
  (PostLex utt)
  ;(print "Duration")
  (Duration utt)
  ;(print "PaIntE_CW_NUM_syl")
  (PaIntE_CW_vq_syl utt)
  ;(print "Set_PaIntE_CW_NUM_syl")
  ;(Set_PaIntE_CW_NUM_syl utt)
  ;(print "painte_pr_denorm_events")
  ;(painte_pr_denorm_events utt)
  ;(print "Int_Targets")
  (Int_Targets utt)
  ;(print "MAP_segments")
  (mel2frq_target utt)
  (MAP_segments utt);FA
  (EmotivePostProcessing utt)		;; support for vsml (Enrico Marchetto)
  ;(print "Wave_Synth")
  (Wave_Synth utt))
  
;non mettere il (provide 'italian_module_flush) perch� deve caricarlo ogni volta. 
