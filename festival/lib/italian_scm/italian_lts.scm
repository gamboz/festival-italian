;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Regole di Trascrizione fonemica per l'italiano.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DA ADESSO APPLICO LE REGOLE DI GRAF.IT, METTO COME COMMENTO  ;;;
;;; IL NUMERO DELLA REGOLA PER IDENTIFICARLA                     ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  Down cases with accents 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(lts.ruleset
 italian_downcase
 ( 
 (PARQUACHIUSA "]")
 ;(VNOACCENT a e i o u E O)
 )
 (
  
  ;;;;;;;;;;;;;;;
  ;;VOCALI ACCENTATE
  ;;Apostrofo
 
 ( [ a "'" ] = � )
 ( [ e "'" ] = � ) ; non viene considerato il caso di: e' (=�)
 ( [ i "'" ] = � )
 ( [ o "'" ] = � )
 ( [ u "'" ] = � )
 ;;Accento Grave 
 ( [ a "`" ] = � )
 ( [ e "`" ] = � ); risolve il caso di e` (=�)
 ( [ i "`" ] = � )
 ( [ o "`" ] = � )
 ( [ u "`" ] = � )

  ;;VOCALI ACCENTATE a fine parola
  ;;Apostrofo
 ( [ a "'" ] # = � )
 ( [ e "'" ] # = � ) ; non viene considerato il caso di: e' (=�)
 ( [ i "'" ] # = � )
 ( [ o "'" ] # = � )
 ( [ u "'" ] # = � )
 ;;Accento Grave 
 ( [ a "`" ] # = � )
 ( [ e "`" ] # = � ); risolve il caso di e` (=�)
 ( [ i "`" ] # = � )
 ( [ o "`" ] # = � )
 ( [ u "`" ] # = � )

    
  ;;VOCALI ACCENTATE codifica ANSI (per l'input da file)
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ;;VOCALI ACCENTATE codifica OEM (per l'input da tastiera)
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ;;;;;LETTERE NORMALI
  ( [ a ] = a )
  ( [ e ] = e )
  ( [ i ] = i )
  ( [ o ] = o )
  ( [ u ] = u )
  ( [ b ] = b )
  ( [ c ] = c )
  ;ANSI
  ( [ "�" ] = s )
  ( [ "�" ] = e )
  ( [ "�" ] = u )
  
  ;OEM
  ( [ "�" ] = s )
  ( [ "�" ] = e )
  ( [ "�" ] = u )
  
  ( [ d ] = d )
  ( [ f ] = f )
  ( [ g ] = g )
  ( [ h ] = h )
  ( [ j ] = j )
  ( [ k ] = k )
  ( [ l ] = l )
  ( [ m ] = m )
  ( [ n ] = n )
  ( [ � ] = � )
  ( [ p ] = p )
  ( [ q ] = q )
  ( [ r ] = r )
  ( [ s ] = s )
  ( [ t ] = t )
  ( [ v ] = v )
  ( [ w ] = w )
  ( [ x ] = x )
  ( [ y ] = y )
  ( [ z ] = z )
  ;;; SIMBOLI e PUNTEGGIATURA in ordine ascii
  ( [ "\!" ] = "\!" )
  ( [ "\"" ] = "\"" )
  ;;ANSI
  ( [ "\�" ] = "\"" )
  ( [ "\�" ] = "\"" )
  
  
 
  ( [ "\#" ] = "\#" )
  ( [ "\$" ] = "\$" )
  ( [ "\�" ] = "\�" )
  ( [ "\%" ] = "\%" )
  ( [ "\&" ] = "\&" )
  ( [ "\'" ] = "\'" )
  ( [ "\(" ] = "\(" )
  ( [ "\)" ] = "\)" )
  ( [ "\*" ] = "\*" )
  ( [ "\+" ] = "\+" )
  ( [ "\," ] = "\," )
  ( [ "\-" ] = "\-" )
  ( [ "\." ] = "\." )
  ( [ "\/" ] = "\/" )
  ( [ "\:" ] = "\:" )
  ( [ "\;" ] = "\;" )
  ( [ "\<" ] = "\<" )
  ( [ "\=" ] = "\=" )
  ( [ "\>" ] = "\>" )
  ( [ "\?" ] = "\?" )
  ( [ "\@" ] = "\@" )
  ( [ "\[" ] = "\[" )
  ( [ "\\" ] = "\\" )
  ( [ PARQUACHIUSA ] = "\]" )
  ( [ "\^" ] = "\^" )
  ( [ "\_" ] = "\_" )
  ( [ "\`" ] = "\`" )
  ( [ "\{" ] = "\{" )
  ( [ "\|" ] = "\|" )
  ( [ "\}" ] = "\}" )
  ( [ "\~" ] = "\~" )
    
  ( [ "�" ] = "�" ) 
  ( [ "�" ] = "�" ) ;oem di �

  ( [ "�" ] = "'" ) 
  ( [ "�" ] = "'" ) ;oem di �



  ;;MAIUSCOLE 
  ;;;;;;;;;;;;;;;
  ;;VOCALI ACCENTATE 
  ;;Apostrofo
 ( [ A "'" ] = � )
 ( [ E "'" ] = � ) ; non viene considerato il caso di: e' (=�)
 ( [ I "'" ] = � )
 ( [ O "'" ] = � )
 ( [ U "'" ] = � )
 ;;Accento Grave 
 ( [ A "`" ] = � )
 ( [ E "`" ] = � ); risolve il caso di e` (=�)
 ( [ I "`" ] = � )
 ( [ O "`" ] = � )
 ( [ U "`" ] = � )
 
  ;;VOCALI ACCENTATE a fine parola
  ;;Apostrofo
 ( [ A "'" ] # = � )
 ( [ E "'" ] # = � ) ; non viene considerato il caso di: e' (=�)
 ( [ I "'" ] # = � )
 ( [ O "'" ] # = � )
 ( [ U "'" ] # = � )
 ;;Accento Grave 
 ( [ A "`" ] # = � )
 ( [ E "`" ] # = � ); risolve il caso di e` (=�)
 ( [ I "`" ] # = � )
 ( [ O "`" ] # = � )
 ( [ U "`" ] # = � )
 
    
  ;;VOCALI ACCENTATE codifica ANSI (per l'input da file)
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  
  ;;VOCALI ACCENTATE codifica OEM (per l'input da tastiera)
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  ( [ � ] = � )
  
  ;;;;;LETTERE NORMALI
  ( [ A ] = a )
  ( [ E ] = e )
  ( [ I ] = i )
  ( [ O ] = o )
  ( [ U ] = u )
  
  ( [ B ] = b )
  ( [ C ] = c )
  ( [ D ] = d )
  ( [ F ] = f )
  ( [ G ] = g )
  ( [ H ] = h )
  ( [ J ] = j )
  ( [ K ] = k )
  ( [ L ] = l )
  ( [ M ] = m )
  ( [ N ] = n )
  ;;ANSI
  ( [ � ] = � )
  ;;OEM
  ( [ � ] = � )

  ( [ P ] = p )
  ( [ Q ] = q )
  ( [ R ] = r )
  ( [ S ] = s )
  ( [ T ] = t )
  ( [ V ] = v )
  ( [ W ] = w )
  ( [ X ] = x )
  ( [ Y ] = y )
  ( [ Z ] = z )

 ( [ 0 ] = 0 )
 ( [ 1 ] = 1 )
 ( [ 2 ] = 2 )
 ( [ 3 ] = 3 )
 ( [ 4 ] = 4 )
 ( [ 5 ] = 5 )
 ( [ 6 ] = 6 )
 ( [ 7 ] = 7 )
 ( [ 8 ] = 8 )
 ( [ 9 ] = 9 )
 ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;   Main letter to sound rules
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(lts.ruleset
;  Name of rule set
 italian
(
(VOK a e i o u � � � � � � � � � �)
)
(

 ( c c [ i ] VOK = i3 )                                               ;; 02.95
 ( g g [ i ] VOK = i3 )                                               ;; 03.00
 ( n g [ i ] VOK = i3 )                                               ;; 03.05
 ( g l [ i ] VOK = i3 )                                               ;; 03.10
 ( g n [ i ] VOK = i3 )                                               ;; 03.15
 ;Apostrofo
 ;( [ a "'" ] # = a1 )
 ;( [ e "'" ] # = e1 ) ; non viene considerato il caso di: e' (=�)
 ;( [ i "'" ] # = i1 )
 ;( [ o "'" ] # = o1 )
 ;( [ u "'" ] # = u1 )
 ;;Accento Grave 
 ;( [ a "`" ] # = a1 )
 ;( [ e "`" ] # = E1 ); risolve il caso di e` (=�)
 ;( [ i "`" ] # = i1 )
 ;( [ o "`" ] # = O1 )
 ;( [ u "`" ] # = u1 )
 
 ;;ANSI 
 ( [ � ] = a1 )
 ( [ � ] = a1 )
 ( [ � ] = e1s )
 ( [ � ] = E1s )
 ( [ � ] = i1 )
 ( [ � ] = i1 )
 ( [ � ] = o1s )  
 ( [ � ] = O1s )
 ( [ � ] = u1 )
 ( [ � ] = u1 )
 ;;OEM
 ;( [ � ] = a1 )
 ;( [ � ] = a1 )
 ;( [ � ] = e1 )
 ;( [ � ] = E1 )
 ;( [ � ] = i1 )
 ;( [ � ] = i1 )
 ;( [ � ] = o1 )  
 ;( [ � ] = O1 )
 ;( [ � ] = u1 )
 ;( [ � ] = u1 )
 
 ( [ a ] = a )
 ( [ e ] = e )
 ( [ i ] = i )
 ( [ o ] = o )
 ( [ u ] = u )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )

 ;( [ "(" ] = ",")         					      ;; 00.00     ;pausa la parentesi
 ;( [ ")" ] = ",")                                           ;; 00.01     ;pausa la parentesi
 ( [ "'" ] = apo )   						      ;; 02.17     ;sostituzione dell'apostrofo con il pi�                                       
 ( [ "`" ] = apo )   						      ;; 02.17     ;sostituzione dell'apostrofo con il pi�                                       
 
 ;( [ ":" ] = ":" )       ;;; ATT: invece di cancellare i due punti li ho lasciati perche' servono per i numeri!
 ;( [ "\"" ] = )    						      ;;00.02 e 00.03
 ( [ "~" ] = )
 
 ( [ 0 ] = 0 )
 ( [ 1 ] = 1 )
 ( [ 2 ] = 2 )
 ( [ 3 ] = 3 )
 ( [ 4 ] = 4 )
 ( [ 5 ] = 5 )
 ( [ 6 ] = 6 )
 ( [ 7 ] = 7 )
 ( [ 8 ] = 8 )
 ( [ 9 ] = 9 )
))



(lts.ruleset
;  Name of rule set
 trasmorph
 ((NIL nil)) 
 (
 ;;ANSI 
 ( [ � ] = a "'" )
 ( [ � ] = a "`" )
 ( [ � ] = e "'" )
 ( [ � ] = e "`" )
 ( [ � ] = i "'" )
 ( [ � ] = i "`" )
 ( [ � ] = o "'" )  
 ( [ � ] = o "`" )
 ( [ � ] = u "'" )
 ( [ � ] = u "`" )
 
 ( [ a ] = a )
 ( [ e ] = e )
 ( [ i ] = i )
 ( [ o ] = o )
 ( [ u ] = u )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )

 ;( [ "(" ] = ",")         					      ;; 00.00     ;pausa la parentesi
 ;( [ ")" ] = ",")                                           ;; 00.01     ;pausa la parentesi
 ( [ "'" ] = "'" )   						      ;; 02.17     ;sostituzione dell'apostrofo con il pi�                                       
 ( [ "`" ] = "`" )   						      ;; 02.17     ;sostituzione dell'apostrofo con il pi�                                       
 
 ;( [ ":" ] = ":" )       ;;; ATT: invece di cancellare i due punti li ho lasciati perche' servono per i numeri!
 ;( [ "\"" ] = )    						      ;;00.02 e 00.03
 ( [ "~" ] = )
 ( [ 0 ] = 0 )
 ( [ 1 ] = 1 )
 ( [ 2 ] = 2 )
 ( [ 3 ] = 3 )
 ( [ 4 ] = 4 )
 ( [ 5 ] = 5 )
 ( [ 6 ] = 6 )
 ( [ 7 ] = 7 )
 ( [ 8 ] = 8 )
 ( [ 9 ] = 9 )
))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  Stress assignment in unstress words by rewrite rules
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PER OGNI INSIEME DI REGOLE CERCO DI APPLICARLE TRAMITE UN    ;;;
;;; LTS.RULESET SEGUITO (SE POSSIBILE) DAL NUMERO DEL GRUPPO     ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress1a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (VNOACCENTROUNDALTA e E a)
  (VNOACCENTDOLCALTA a o O)
  (VNOACCENTALTABAS e E o O)
  (VNOACCENTROUNDACCB i a)
  (VNOACCENTBASACCAB i o O)
  (VNOACCENTBAS i e E o O u)
  (VNOACCENTBASACCM i e E u)
  (KNONAS j w p t k b d g f s S v z ts tS dz dZ l L r)
  
)
;  Rules
(
 ;; le regole 00.04 e 00.05 le salto!
 ;; la regola 00.06 invece e' gia' stata implementata con la punteggiatura
 ;; lo stesso dicasi per le regole 01.xx e 02.(00 05 10 15)
 ;; per le regole 01.46 e 02.16 si possono scrivere un paio di righe nel
 ;; ruleset precedente

 ( VNOACCENT KNONAS m [ i ] VNOACCENTROUNDALTA # = i1 )               ;; 03.21
 ( VNOACCENT m [ i ] VNOACCENTROUNDALTA # = i1 )                      ;; 03.21
 ( VNOACCENT p [ i ] VNOACCENTROUNDALTA # = i1 )                      ;; 03.22
 ( VNOACCENT l [ i ] VNOACCENTROUNDALTA # = i1 )                      ;; 03.23
 ( VNOACCENT n [ i ] VNOACCENTROUNDALTA # = i1 )                      ;; 03.24
 ( a r d [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 03.20
 ( g r a f [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.35
 ( p l e g [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.40
 ( a r c h [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.45
 ( s c o p [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.70
 ( r e s t [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.75
 ( c r a z [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.80
 ( c a n z [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.85
 ( m a n z [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.90
 ( r a n z [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 03.95
 ( e s t r [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 04.00
 ( r r a g [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 04.05
 ( a l a c [ i ] VNOACCENTROUNDALTA # = i1 )                          ;; 04.10
 ( f o b [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.15
 ( p e d [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.20
 ( o d [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 04.25
 ( r o f [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.35
 ( s o f [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.40
 ( f a g [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.45
 ( a l g [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.50
 ( g o g [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.55
 ( l o g [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.60
 ( e r g [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.65
 ( u r g [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 04.70
 ( r a s [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 06.55
 ( t a s [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 06.60
 ( v a s [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 06.65
 ( r VNOACCENTDOLCALTA r [ i ] VNOACCENTROUNDALTA # = i1 )            ;; 06.90
 ( g VNOACCENTDOLCALTA r [ i ] VNOACCENTROUNDALTA # = i1 )            ;; 06.95
 ( VNOACCENTROUNDALTA t r [ i ] VNOACCENTROUNDALTA # = i1 )           ;; 07.00
 ( t t r [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 07.05
 ( g e s [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 07.10
 ( h e s [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 07.15
 ( o e s [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 07.20
 ( t e s [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 07.25
 ( VNOACCENTALTABAS s s [ i ] VNOACCENTROUNDALTA # = i1 )             ;; 07.30
 ( VNOACCENTROUNDACCB s t [ i ] VNOACCENTROUNDALTA # = i1 )           ;; 07.35
 ( b a z [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 07.40
 ( m a z [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 07.45
 ( a z z [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 07.50
 ( i c h [ i ] VNOACCENTBASACCAB # = i1 )                             ;; 07.55
 ( VNOACCENTBAS l l [ i ] VNOACCENTBASACCAB # = i1 )                  ;; 07.60
 ( o l [ i ] VNOACCENTBASACCAB # = i1 )                               ;; 07.65
 ( e n t [ i ] VNOACCENTBASACCAB # = i1 )                             ;; 07.85
 ( VNOACCENTBASACCM s t [ i ] VNOACCENTBASACCAB # = i1 )              ;; 07.90
 ( VNOACCENTALTABAS t t [ i ] VNOACCENTBASACCAB # = i1 )              ;; 07.95
 ( o l l [ i ] VNOACCENTROUNDALTA # = i1 )                            ;; 08.00
 ( d r [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 08.05
 ( e r [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 08.10
 ( g r [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 08.15
 ( VNOACCENTBASACCB s [ i ] VNOACCENTROUNDALTA # = i1 )               ;; 08.20
 ( p s [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 08.25
 ( r s [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 08.30
 ( VNOACCENTROUNDACCB t [ i ] VNOACCENTROUNDALTA # = i1 )             ;; 08.35
 ( n t [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 08.40
 ( r t [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 08.45
 ( t t [ i ] VNOACCENTROUNDALTA # = i1 )                              ;; 08.50
 ( i t [ i ] VNOACCENTBASACCAB # = i1 )                               ;; 08.55
 ( VNOACCENT v [ i ] VNOACCENTROUNDALTA # = i1 )                      ;; 08.60

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress1b
;  Sets used in the rules
(
  (VNOACCENTROUNDALTA e E a)
  (VNOACCENTBASACCAB i o O)
 )
;  Rules
(
 ( c a r d [ i1 ] VNOACCENTROUNDALTA # = i )                          ;; 03.25
 ( u a r d [ i1 ] VNOACCENTROUNDALTA # = i )                          ;; 03.30
 ( f a n z [ i1 ] VNOACCENTROUNDALTA # = i )                          ;; 03.96
 ( t o d [ i1 ] VNOACCENTROUNDALTA # = i )                            ;; 04.30
 ( p o l [ i1 ] VNOACCENTBASACCAB # = i )                             ;; 07.70
 ( r o l [ i1 ] VNOACCENTBASACCAB # = i )                             ;; 07.75
 ( s o l [ i1 ] VNOACCENTBASACCAB # = i )                             ;; 07.80

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress1c
;  Sets used in the rules
(
  (VNOACCENTALTA i e E a o O)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS * i VNOACCENTALTA # = i1 )                              ;; 08.65
 ( [ e ] KONS * i VNOACCENTALTA # = e1 )                              ;; 08.65
 ( [ E ] KONS * i VNOACCENTALTA # = E1 )                              ;; 08.65
 ( [ a ] KONS * i VNOACCENTALTA # = a1 )                              ;; 08.65
 ( [ o ] KONS * i VNOACCENTALTA # = o1 )                              ;; 08.65
 ( [ O ] KONS * i VNOACCENTALTA # = O1 )                              ;; 08.65
 ( [ u ] KONS * i VNOACCENTALTA # = u1 )                              ;; 08.65  
 ( [ i ] KONS # = i1 )                                                ;; 08.70
 ( [ e ] KONS # = e1 )                                                ;; 08.70
 ( [ E ] KONS # = E1 )                                                ;; 08.70
 ( [ a ] KONS # = a1 )                                                ;; 08.70
 ( [ o ] KONS # = o1 )                                                ;; 08.70
 ( [ O ] KONS # = O1 )                                                ;; 08.70
 ( [ u ] KONS # = u1 )                                                ;; 08.70

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress4a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (VNOACCENTROUNDALTA e E a)
  (VNOBASACCAB i o O i1 o1 O1)
  (VNOACCENTDOLCALTA a o O)
  (VNOACCENTALTABAS e E o O)
  (VNOACCENTROUNDACCB i a)
  (VNOACCENTBASACCAB i o O)
  (VNOACCENTBASACCM i e E u)
  (VNOACCENTBASACCB i o O u)
  (VNOACCENTALTA i e E a o O)
  (KNONASBORD p t k b d g r)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
   
)
;  Rules
(
;;;Regole commentate perch� utilizzano la caratteristica PENULT di Piero, che non abbbiamo implementato, ma SONO STATE PRESE IN CONSIDERAZIONE IN ALTRO MODO
; ( [ i ] KONS * VNOACCENT KONS * # = i3 )                             ;; 08.75
; ( [ e ] KONS * VNOACCENT KONS * # = i3 )                             ;; 08.75
; ( [ E ] KONS * VNOACCENT KONS * # = i3 )                             ;; 08.75
; ( [ a ] KONS * VNOACCENT KONS * # = i3 )                             ;; 08.75
; ( [ o ] KONS * VNOACCENT KONS * # = i3 )                             ;; 08.75
; ( [ O ] KONS * VNOACCENT KONS * # = i3 )                             ;; 08.75
; ( [ u ] KONS * VNOACCENT KONS * # = i3 )                             ;; 08.75
; ( [ i1 ] KONS * VNOACCENT KONS * # = i23 )                           ;; 08.75
; ( [ e1 ] KONS * VNOACCENT KONS * # = e23 )                           ;; 08.75
; ( [ E1 ] KONS * VNOACCENT KONS * # = E23 )                           ;; 08.75
; ( [ a1 ] KONS * VNOACCENT KONS * # = a23 )                           ;; 08.75
; ( [ o1 ] KONS * VNOACCENT KONS * # = o23 )                           ;; 08.75
; ( [ O1 ] KONS * VNOACCENT KONS * # = O23 )                           ;; 08.75
; ( [ u1 ] KONS * VNOACCENT KONS * # = u23 )                           ;; 08.75
; ( [ i ] KONS q u VNOACCENT KONS * # = i3 )                           ;; 08.76
; ( [ e ] KONS q u VNOACCENT KONS * # = e3 )                           ;; 08.76
; ( [ E ] KONS q u VNOACCENT KONS * # = E3 )                           ;; 08.76
; ( [ a ] KONS q u VNOACCENT KONS * # = a3 )                           ;; 08.76
; ( [ o ] KONS q u VNOACCENT KONS * # = o3 )                           ;; 08.76
; ( [ O ] KONS q u VNOACCENT KONS * # = O3 )                           ;; 08.76
; ( [ u ] KONS q u VNOACCENT KONS * # = u3 )                           ;; 08.76
; ( [ i1 ] KONS q u VNOACCENT KONS * # = i23 )                         ;; 08.76
; ( [ e1 ] KONS q u VNOACCENT KONS * # = e23 )                         ;; 08.76
; ( [ E1 ] KONS q u VNOACCENT KONS * # = E23 )                         ;; 08.76
; ( [ a1 ] KONS q u VNOACCENT KONS * # = a23 )                         ;; 08.76
; ( [ o1 ] KONS q u VNOACCENT KONS * # = o23 )                         ;; 08.76
; ( [ O1 ] KONS q u VNOACCENT KONS * # = O23 )                         ;; 08.76
; ( [ u1 ] KONS q u VNOACCENT KONS * # = u23 )                         ;; 08.76
 ( c [ u ] i t VNOACCENTBASACCAB # = u1 )                             ;; 08.80
 ( c [ i ] d VNOACCENTROUNDACCB # = i1 )                              ;; 08.85
 ( i b [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 08.90
 ( m b [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 08.95
 ( r b [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 09.00
 ( l c [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 09.05
 ( o c [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 09.10
 ( r c [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 09.15
 ( s c [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 09.20
 ( u c [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 09.25
 ( d [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 09.30
 ( v [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 09.35
 ( f [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 09.50
 ( g [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 09.55
 ( l [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 09.70
 ( a m [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 09.80
 ( n [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 09.85
 ( p [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 09.95
 ( h [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 10.15
 ( r [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 10.20
 ( s [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 10.25
 ( t [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 10.30
 ( u p [ i ] t VNOACCENTBASACCAB # = i1 )                             ;; 10.35
 ( z [ i ] t VNOACCENTBASACCAB # = i1 )                               ;; 10.40
 ( [ e ] n d i t VNOACCENTROUNDALTA # = e1 )                          ;; 10.45
 ( g [ e ] n i t VNOACCENTROUNDALTA # = e1 )                          ;; 10.50
 ( [ i ] c i t VNOACCENTROUNDALTA # = i1 )                            ;; 10.55
 ( [ e ] c i t VNOACCENTROUNDALTA # = e1 )                            ;; 10.55
 ( [ E ] c i t VNOACCENTROUNDALTA # = E1 )                            ;; 10.55
 ( [ o ] f i t VNOACCENT # = o1 )                                     ;; 10.60
 ( [ i ] p i t e # = i1 )                                             ;; 10.65
 ( [ e ] p i t e # = e1 )                                             ;; 10.65
 ( [ E ] p i t e # = E1 )                                             ;; 10.65
 ( [ a ] p i t e # = a1 )                                             ;; 10.65
 ( [ o ] p i t e # = o1 )                                             ;; 10.65
 ( [ O ] p i t e # = O1 )                                             ;; 10.65
 ( [ u ] p i t e # = u1 )                                             ;; 10.65
 ( [ i ] t VNOACCENTROUNDALTA # = i1 )                                ;; 10.70
 ( [ e ] u r i c h e # = e1 )                                         ;; 10.75
 ( [ e ] u r i c VNOACCENTDOLC # = e1 )                               ;; 10.80
 ( [ a ] u s t i c h e # = e1 )                                       ;; 10.85
 ( [ a ] u s t i c VNOACCENTDOLC # = e1 )                             ;; 10.90
 ( [ a ] i c h VNOACCENTDOLC # = a1 )                                 ;; 10.95
 ( [ o ] i c h VNOACCENTDOLC # = o1 )                                 ;; 10.95
 ( [ O ] i c h VNOACCENTDOLC # = O1 )                                 ;; 10.95
 ( [ a ] i c VNOACCENTDOLC # = a1 )                                   ;; 11.00
 ( [ o ] i c VNOACCENTDOLC # = o1 )                                   ;; 11.00
 ( [ O ] i c VNOACCENTDOLC # = O1 )                                   ;; 11.00
 ( d [ i ] g h VNOACCENTDOLC # = i1 )                                 ;; 11.05
 ( f [ i ] g h VNOACCENTDOLC # = i1 )                                 ;; 11.10
 ( r [ i ] g h VNOACCENTDOLC # = i1 )                                 ;; 11.15
 ( s [ i ] g h VNOACCENTDOLC # = i1 )                                 ;; 11.20
 ( d [ i ] g VNOACCENTDOLC # = i1 )                                   ;; 11.25
 ( f [ i ] g VNOACCENTDOLC # = i1 )                                   ;; 11.30
 ( r [ i ] g VNOACCENTDOLC # = i1 )                                   ;; 11.35
 ( s [ i ] g VNOACCENTDOLC # = i1 )                                   ;; 11.40
 ( [ i ] KONS + i KNONASBORD VNOACCENT # = i1 )                       ;; 11.45
 ( [ e ] KONS + i KNONASBORD VNOACCENT # = e1 )                       ;; 11.45
 ( [ E ] KONS + i KNONASBORD VNOACCENT # = E1 )                       ;; 11.45
 ( [ a ] KONS + i KNONASBORD VNOACCENT # = a1 )                       ;; 11.45
 ( [ o ] KONS + i KNONASBORD VNOACCENT # = o1 )                       ;; 11.45
 ( [ O ] KONS + i KNONASBORD VNOACCENT # = O1 )                       ;; 11.45
 ( [ u ] KONS + i KNONASBORD VNOACCENT # = u1 )                       ;; 11.45
 ( [ i ] KONS + i KNONASBORD h VNOACCENT # = i1 )                     ;; 11.45
 ( [ e ] KONS + i KNONASBORD h VNOACCENT # = e1 )                     ;; 11.45
 ( [ E ] KONS + i KNONASBORD h VNOACCENT # = E1 )                     ;; 11.45
 ( [ a ] KONS + i KNONASBORD h VNOACCENT # = a1 )                     ;; 11.45
 ( [ o ] KONS + i KNONASBORD h VNOACCENT # = o1 )                     ;; 11.45
 ( [ O ] KONS + i KNONASBORD h VNOACCENT # = O1 )                     ;; 11.45
 ( [ u ] KONS + i KNONASBORD h VNOACCENT # = u1 )                     ;; 11.45
 ( [ i ] KONS q u i KNONASBORD VNOACCENT # = i1 )                     ;; 11.45
 ( [ e ] KONS q u i KNONASBORD VNOACCENT # = e1 )                     ;; 11.45
 ( [ E ] KONS q u i KNONASBORD VNOACCENT # = E1 )                     ;; 11.45
 ( [ a ] KONS q u i KNONASBORD VNOACCENT # = a1 )                     ;; 11.45
 ( [ o ] KONS q u i KNONASBORD VNOACCENT # = o1 )                     ;; 11.45
 ( [ O ] KONS q u i KNONASBORD VNOACCENT # = O1 )                     ;; 11.45
 ( [ u ] KONS q u i KNONASBORD VNOACCENT # = u1 )                     ;; 11.45
 ( [ i ] KONS q u i KNONASBORD h VNOACCENT # = i1 )                   ;; 11.45
 ( [ e ] KONS q u i KNONASBORD h VNOACCENT # = e1 )                   ;; 11.45
 ( [ E ] KONS q u i KNONASBORD h VNOACCENT # = E1 )                   ;; 11.45
 ( [ a ] KONS q u i KNONASBORD h VNOACCENT # = a1 )                   ;; 11.45
 ( [ o ] KONS q u i KNONASBORD h VNOACCENT # = o1 )                   ;; 11.45
 ( [ O ] KONS q u i KNONASBORD h VNOACCENT # = O1 )                   ;; 11.45
 ( [ u ] KONS q u i KNONASBORD h VNOACCENT # = u1 )                   ;; 11.45

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)

(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress4b
;  Sets used in the rules
(
  (VNOACCENTBASACCAB i o O)
)
;  Rules
(
 ( e d [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 09.40
 ( e v [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 09.45
 ( o f [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 09.60
 ( o g [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 09.65
 ( c l [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 09.75
 ( e l [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 09.77
 ( e n [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 09.90
 ( a p [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 10.00
 ( e p [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 10.05
 ( l p [ i1 ] t VNOACCENTBASACCAB # = i )                             ;; 10.10

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress4c
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
  (KNONASBORD p t k b d g r)
)
;  Rules
(
 ( [ i ] KONS + i KNONASBORD VNOACCENT # = i1 )                       ;; 11.45
 ( [ e ] KONS + i KNONASBORD VNOACCENT # = e1 )                       ;; 11.45
 ( [ E ] KONS + i KNONASBORD VNOACCENT # = E1 )                       ;; 11.45
 ( [ a ] KONS + i KNONASBORD VNOACCENT # = a1 )                       ;; 11.45
 ( [ o ] KONS + i KNONASBORD VNOACCENT # = o1 )                       ;; 11.45
 ( [ O ] KONS + i KNONASBORD VNOACCENT # = O1 )                       ;; 11.45
 ( [ u ] KONS + i KNONASBORD VNOACCENT # = u1 )                       ;; 11.45
 ( [ i ] KONS + i KNONASBORD h VNOACCENT # = i1 )                     ;; 11.45
 ( [ e ] KONS + i KNONASBORD h VNOACCENT # = e1 )                     ;; 11.45
 ( [ E ] KONS + i KNONASBORD h VNOACCENT # = E1 )                     ;; 11.45
 ( [ a ] KONS + i KNONASBORD h VNOACCENT # = a1 )                     ;; 11.45
 ( [ o ] KONS + i KNONASBORD h VNOACCENT # = o1 )                     ;; 11.45
 ( [ O ] KONS + i KNONASBORD h VNOACCENT # = O1 )                     ;; 11.45
 ( [ u ] KONS + i KNONASBORD h VNOACCENT # = u1 )                     ;; 11.45

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress4d
;  Sets used in the rules
(
  (VDOLCNOACCENT i e E)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i1 ] KONS + i c VDOLCNOACCENT # = i1 )                           ;; 11.50
 ( [ e1 ] KONS + i c VDOLCNOACCENT # = e1 )                           ;; 11.50
 ( [ E1 ] KONS + i c VDOLCNOACCENT # = E1 )                           ;; 11.50
 ( [ e1s ] KONS + i c VDOLCNOACCENT # = e1s )                           ;; 11.50
 ( [ E1s ] KONS + i c VDOLCNOACCENT # = E1s )                           ;; 11.50
 ( [ a1 ] KONS + i c VDOLCNOACCENT # = a1 )                           ;; 11.50
 ( [ o1 ] KONS + i c VDOLCNOACCENT # = o1 )                           ;; 11.50
 ( [ O1 ] KONS + i c VDOLCNOACCENT # = O1 )                           ;; 11.50
 ( [ o1s ] KONS + i c VDOLCNOACCENT # = o1s )                           ;; 11.50
 ( [ O1s ] KONS + i c VDOLCNOACCENT # = O1s )                           ;; 11.50
 ( [ u1 ] KONS + i c VDOLCNOACCENT # = u1 )                           ;; 11.50
 ( [ i1 ] KONS + i g VDOLCNOACCENT # = i1 )                           ;; 11.55
 ( [ e1 ] KONS + i g VDOLCNOACCENT # = e1 )                           ;; 11.55
 ( [ E1 ] KONS + i g VDOLCNOACCENT # = E1 )                           ;; 11.55
 ( [ e1s ] KONS + i g VDOLCNOACCENT # = e1s )                           ;; 11.55
 ( [ E1s ] KONS + i g VDOLCNOACCENT # = E1s )                           ;; 11.55
 ( [ a1 ] KONS + i g VDOLCNOACCENT # = a1 )                           ;; 11.55
 ( [ o1 ] KONS + i g VDOLCNOACCENT # = o1 )                           ;; 11.55
 ( [ O1 ] KONS + i g VDOLCNOACCENT # = O1 )                           ;; 11.55
 ( [ o1s ] KONS + i g VDOLCNOACCENT # = o1s )                           ;; 11.55
 ( [ O1s ] KONS + i g VDOLCNOACCENT # = O1s )                           ;; 11.55
 
 ( [ u1 ] KONS + i g VDOLCNOACCENT # = u1 )                           ;; 11.55

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s ) 
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress5a
;  Sets used in the rules
(
  (VNOACCENTDOLC a o O u)
  (VDOLCNOACCENT i e E)
)
;  Rules
(
 ( b r [ a ] c h e # = a1 )                                           ;; 11.70
 ( b r [ a ] c a # = a1 )                                             ;; 11.75
 ( [ i ] a c h VDOLCNOACCENT # = i1 )                                 ;; 11.80
 ( [ i ] a c VNOACCENTDOLC # = i1 )                                   ;; 11.85
 ( [ a ] u c h VDOLCNOACCENT # = a1 )                                 ;; 11.90
 ( [ e ] u c h VDOLCNOACCENT # = e1 )                                 ;; 11.95
 ( [ u ] c h VDOLCNOACCENT # = u1 )                                   ;; 12.00
 ( [ a ] u c VNOACCENTDOLC # = a1 )                                   ;; 12.05
 ( [ e ] u c VNOACCENTDOLC # = e1 )                                   ;; 12.10
 ( [ u ] c VNOACCENTDOLC # = u1 )                                     ;; 12.15
 ( [ e ] c h VDOLCNOACCENT # = e1 )                                   ;; 12.20
 ( [ e ] c VNOACCENTDOLC # = e1 )                                     ;; 12.25

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
) 


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress5b
;  Sets used in the rules
(
  (VNOACCENTROUNDALTA e E a)
  (VDOLCNOACCENT i e E)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + VNOACCENTROUNDALTA c h VDOLCNOACCENT # = i1 )         ;; 12.30
 ( [ e ] KONS + VNOACCENTROUNDALTA c h VDOLCNOACCENT # = e1 )         ;; 12.30
 ( [ E ] KONS + VNOACCENTROUNDALTA c h VDOLCNOACCENT # = E1 )         ;; 12.30
 ( [ a ] KONS + VNOACCENTROUNDALTA c h VDOLCNOACCENT # = a1 )         ;; 12.30
 ( [ o ] KONS + VNOACCENTROUNDALTA c h VDOLCNOACCENT # = o1 )         ;; 12.30
 ( [ O ] KONS + VNOACCENTROUNDALTA c h VDOLCNOACCENT # = O1 )         ;; 12.30
 ( [ u ] KONS + VNOACCENTROUNDALTA c h VDOLCNOACCENT # = u1 )         ;; 12.30
 ( [ i ] KONS + i3 VNOACCENTROUNDALTA c h VDOLCNOACCENT # = i1 )      ;; 12.35
 ( [ e ] KONS + i3 VNOACCENTROUNDALTA c h VDOLCNOACCENT # = e1 )      ;; 12.35
 ( [ E ] KONS + i3 VNOACCENTROUNDALTA c h VDOLCNOACCENT # = E1 )      ;; 12.35
 ( [ a ] KONS + i3 VNOACCENTROUNDALTA c h VDOLCNOACCENT # = a1 )      ;; 12.35
 ( [ o ] KONS + i3 VNOACCENTROUNDALTA c h VDOLCNOACCENT # = o1 )      ;; 12.35
 ( [ O ] KONS + i3 VNOACCENTROUNDALTA c h VDOLCNOACCENT # = O1 )      ;; 12.35
 ( [ u ] KONS + i3 VNOACCENTROUNDALTA c h VDOLCNOACCENT # = u1 )      ;; 12.35
 ( [ i ] KONS + VNOACCENTROUNDALTA c VDOLCNOACCENT # = i1 )           ;; 12.40
 ( [ e ] KONS + VNOACCENTROUNDALTA c VDOLCNOACCENT # = e1 )           ;; 12.40
 ( [ E ] KONS + VNOACCENTROUNDALTA c VDOLCNOACCENT # = E1 )           ;; 12.40
 ( [ a ] KONS + VNOACCENTROUNDALTA c VDOLCNOACCENT # = a1 )           ;; 12.40
 ( [ o ] KONS + VNOACCENTROUNDALTA c VDOLCNOACCENT # = o1 )           ;; 12.40
 ( [ O ] KONS + VNOACCENTROUNDALTA c VDOLCNOACCENT # = O1 )           ;; 12.40
 ( [ u ] KONS + VNOACCENTROUNDALTA c VDOLCNOACCENT # = u1 )           ;; 12.40
 ( [ i ] KONS + i3 VNOACCENTROUNDALTA c VDOLCNOACCENT # = i1 )        ;; 12.45
 ( [ e ] KONS + i3 VNOACCENTROUNDALTA c VDOLCNOACCENT # = e1 )        ;; 12.45
 ( [ E ] KONS + i3 VNOACCENTROUNDALTA c VDOLCNOACCENT # = E1 )        ;; 12.45
 ( [ a ] KONS + i3 VNOACCENTROUNDALTA c VDOLCNOACCENT # = a1 )        ;; 12.45
 ( [ o ] KONS + i3 VNOACCENTROUNDALTA c VDOLCNOACCENT # = o1 )        ;; 12.45
 ( [ O ] KONS + i3 VNOACCENTROUNDALTA c VDOLCNOACCENT # = O1 )        ;; 12.45
 ( [ u ] KONS + i3 VNOACCENTROUNDALTA c VDOLCNOACCENT # = u1 )        ;; 12.45

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress6a
;  Sets used in the rules
(
  (VDOLCNOACCENT i e E)
  (VNOACCENTROUND i e E a)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ a ] u s t i c i # = a1 )                                         ;; 12.50
 ( [ e ] u r i c i # = e1 )                                           ;; 12.55
 ( [ e ] n t r i c i # = e1 )                                         ;; 12.60
 ( t r [ i ] c VDOLCNOACCENT # = i1 )                                 ;; 12.65
 ( u f f [ i ] c i # = i1 )                                           ;; 12.70
 ( i f [ i ] c i # = i1 )                                             ;; 12.75
 ( [ i ] a c i # = i1 )                                               ;; 12.80
 ( [ a ] i c i # = a1 )                                               ;; 12.85
 ( [ o ] i c i # = o1 )                                               ;; 12.85
 ( [ O ] i c i # = O1 )                                               ;; 12.85
 ( [ a ] u c VDOLCNOACCENT # = a1 )                                   ;; 12.90
 ( [ e ] u c VDOLCNOACCENT # = e1 )                                   ;; 12.95
 ( [ u ] c VDOLCNOACCENT # = u1 )                                     ;; 13.00
 ( [ o ] m a c i # = o1 )                                             ;; 13.05
 ( [ a ] r m a c i # = a1 )                                           ;; 13.10
 ( [ i ] f a c VDOLCNOACCENT # = i1 )                                 ;; 13.15
 ( [ e ] f a c VDOLCNOACCENT # = e1 )                                 ;; 13.15
 ( [ E ] f a c VDOLCNOACCENT # = E1 )                                 ;; 13.15
 ( [ a ] f a c VDOLCNOACCENT # = a1 )                                 ;; 13.15
 ( [ o ] f a c VDOLCNOACCENT # = o1 )                                 ;; 13.15
 ( [ O ] f a c VDOLCNOACCENT # = O1 )                                 ;; 13.15
 ( [ u ] f a c VDOLCNOACCENT # = u1 )                                 ;; 13.15
 ( [ i ] l a c VDOLCNOACCENT # = i1 )                                 ;; 13.20
 ( [ e ] l a c VDOLCNOACCENT # = e1 )                                 ;; 13.20
 ( [ E ] l a c VDOLCNOACCENT # = E1 )                                 ;; 13.20
 ( [ a ] l a c VDOLCNOACCENT # = a1 )                                 ;; 13.20
 ( [ o ] l a c VDOLCNOACCENT # = o1 )                                 ;; 13.20
 ( [ O ] l a c VDOLCNOACCENT # = O1 )                                 ;; 13.20
 ( [ u ] l a c VDOLCNOACCENT # = u1 )                                 ;; 13.20
 ( [ a ] c VDOLCNOACCENT # = a1 )                                     ;; 13.25
 ( [ o ] c VDOLCNOACCENT # = o1 )                                     ;; 13.25
 ( [ O ] c VDOLCNOACCENT # = O1 )                                     ;; 13.25
 ( [ e ] c VDOLCNOACCENT # = e1 )                                     ;; 13.30
 ( [ i ] KONS + VNOACCENTROUND c VDOLCNOACCENT # = i1 )               ;; 13.35
 ( [ e ] KONS + VNOACCENTROUND c VDOLCNOACCENT # = e1 )               ;; 13.35
 ( [ E ] KONS + VNOACCENTROUND c VDOLCNOACCENT # = E1 )               ;; 13.35
 ( [ a ] KONS + VNOACCENTROUND c VDOLCNOACCENT # = a1 )               ;; 13.35
 ( [ o ] KONS + VNOACCENTROUND c VDOLCNOACCENT # = o1 )               ;; 13.35
 ( [ O ] KONS + VNOACCENTROUND c VDOLCNOACCENT # = O1 )               ;; 13.35
 ( [ u ] KONS + VNOACCENTROUND c VDOLCNOACCENT # = u1 )               ;; 13.35
 ( [ i ] KONS + i3 VNOACCENTROUND c VDOLCNOACCENT # = i1 )            ;; 13.35
 ( [ e ] KONS + i3 VNOACCENTROUND c VDOLCNOACCENT # = e1 )            ;; 13.35
 ( [ E ] KONS + i3 VNOACCENTROUND c VDOLCNOACCENT # = E1 )            ;; 13.35
 ( [ a ] KONS + i3 VNOACCENTROUND c VDOLCNOACCENT # = a1 )            ;; 13.35
 ( [ o ] KONS + i3 VNOACCENTROUND c VDOLCNOACCENT # = o1 )            ;; 13.35
 ( [ O ] KONS + i3 VNOACCENTROUND c VDOLCNOACCENT # = O1 )            ;; 13.35
 ( [ u ] KONS + i3 VNOACCENTROUND c VDOLCNOACCENT # = u1 )            ;; 13.35

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress7a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (VNOACCENTDOLCALTA a o O)
  (VNOACCENTALTA i e E a o O)
  (VNOROUNDALTA e E a e1 e1s E1 E1s a1)
)
;  Rules
(
 ( r [ e ] g g e r e # = e1 )                                         ;; 13.45
 ( l [ e ] g g e r e # = e1 )                                         ;; 13.50
 ( [ i ] g l i e r e # = i1 )                                         ;; 13.52
 ( [ e ] g l i e r e # = e1 )                                         ;; 13.52
 ( [ E ] g l i e r e # = E1 )                                         ;; 13.52
 ( [ a ] g l i e r e # = a1 )                                         ;; 13.52
 ( [ o ] g l i e r e # = o1 )                                         ;; 13.52
 ( [ O ] g l i e r e # = O1 )                                         ;; 13.52
 ( [ u ] g l i e r e # = u1 )                                         ;; 13.52
 ( VNOROUNDALTA g g [ e ] r e # = e1 )                                ;; 13.55
 ( VNOACCENTALTA d [ e ] r VNOACCENT # = e1 )                         ;; 13.60
 ( f [ e ] r VNOACCENT # = e1 )                                       ;; 13.65
 ( [ i f e1 ] r VNOACCENT # = i1 f e )                                ;; 13.70
 ( VNOACCENTDOLCALTA l [ e ] r VNOACCENT # = e1 )                     ;; 13.75
 ( i t [ e ] r VNOACCENT # = e1 )                                     ;; 13.80
 ( n t [ e ] r VNOACCENT # = e1 )                                     ;; 13.85
 ( s [ i ] s t e r e # = i1 )                                         ;; 13.90
 ( s t [ e ] r VNOACCENT # = e1 )                                     ;; 13.95
 ( v [ i ] n c e r e # = i1 )                                         ;; 14.00
 ( [ a ] r c e r e # = a1 )                                           ;; 14.05
 ( [ o ] r c e r e # = o1 )                                           ;; 14.05
 ( [ O ] r c e r e # = O1 )                                           ;; 14.05
 ( [ i ] s c e r e # = i1 )                                           ;; 14.10
 ( [ e ] s c e r e # = e1 )                                           ;; 14.10
 ( [ E ] s c e r e # = E1 )                                           ;; 14.10
 ( [ a ] s c e r e # = a1 )                                           ;; 14.10
 ( [ o ] s c e r e # = o1 )                                           ;; 14.10
 ( [ O ] s c e r e # = O1 )                                           ;; 14.10
 ( [ u ] s c e r e # = u1 )                                           ;; 14.10
 ( [ o ] c e r e # = o1 )                                             ;; 14.15
 ( c [ e ] r VNOACCENT # = e1 )                                       ;; 14.20
 ( i [ e ] r VNOACCENT # = e1 )                                       ;; 14.25
 ( [ i ] g n e r e # = i1 )                                           ;; 14.30
 ( [ e ] g n e r e # = e1 )                                           ;; 14.30
 ( [ E ] g n e r e # = E1 )                                           ;; 14.30
 ( [ a ] g n e r e # = a1 )                                           ;; 14.30
 ( c [ e ] n e r e # = e1 )                                           ;; 14.35
 ( g [ e ] n e r e # = e1 )                                           ;; 14.40
 ( v [ e ] n e r e # = e1 )                                           ;; 14.45
 ( n [ e ] r VNOACCENT # = e1 )                                       ;; 14.50
 ( [ o ] r r e r e # = o1 )                                           ;; 14.55
 ( r [ e ] r VNOACCENT # = e1 )                                       ;; 14.60

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress7b
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i f e1 ] r VNOACCENT # = i1 f e )                                ;; 13.70
 ( [ i ] KONS + e r VNOACCENT # = i1 )                                ;; 14.65
 ( [ e ] KONS + e r VNOACCENT # = e1 )                                ;; 14.65
 ( [ E ] KONS + e r VNOACCENT # = E1 )                                ;; 14.65
 ( [ a ] KONS + e r VNOACCENT # = a1 )                                ;; 14.65
 ( [ o ] KONS + e r VNOACCENT # = o1 )                                ;; 14.65
 ( [ O ] KONS + e r VNOACCENT # = O1 )                                ;; 14.65
 ( [ u ] KONS + e r VNOACCENT # = u1 )                                ;; 14.65

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)



(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress8a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (VNOACCENTROUNDACCB i a)
  (VNOACCENTBASACCAB i o O)
  (VDOLCNOACCENT i e E)
  (VNOACCENTROUND i e E a)
)
;  Rules
(
 ( VNOACCENTROUNDACCB c c i [ o ] l VNOACCENT # = o1 )                ;; 14.70
 ( g g i [ o ] l VNOACCENT # = o1 )                                   ;; 14.75
 ( g l i [ o ] l VNOACCENT # = o1 )                                   ;; 14.80
 ( b b i [ o ] l VNOACCENT # = o1 )                                   ;; 14.85
 ( n t [ i ] l VDOLCNOACCENT # = i1 )                                 ;; 14.90
 ( s t [ i ] l VDOLCNOACCENT # = i1 )                                 ;; 14.95
 ( s c [ a ] l VNOACCENTBASACCAB # = a1 )                             ;; 15.00
 ( o c [ e ] l VDOLCNOACCENT # = e1 )                                 ;; 15.15
 ( [ e ] l VNOACCENT # = e1 )                                         ;; 15.05
 ( c i [ e ] l VNOACCENTBASACCAB # = e1 )                             ;; 15.25
 ( d [ i ] l VDOLCNOACCENT # = i1 )                                   ;; 15.55
 ( n [ i ] l VDOLCNOACCENT # = i1 )                                   ;; 15.60
 ( r [ i ] l VDOLCNOACCENT # = i1 )                                   ;; 15.65
 ( v [ i ] l VDOLCNOACCENT # = i1 )                                   ;; 15.70
 ( m [ i ] l a # = i1 )                                               ;; 15.75
 ( r [ o ] l VNOACCENT # = o1 )                                       ;; 15.80
 ( [ a ] l VNOACCENTROUND # = a1 )                                    ;; 15.85

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress8b
;  Sets used in the rules
(
  (VACCENT a1 e1 e1s i1 o1 o1s u1 E1 E1s O1 O1s)
  (VNOACCENTROUND i e E a)
)
;  Rules
(
 ( c [ e1 ] l VNOACCENT # = e )                                       ;; 15.10
 ( i [ e1 ] l VNOACCENT # = e )                                       ;; 15.20
 ( m [ e1 ] l VNOACCENT # = e )                                       ;; 15.30
 ( s [ e1 ] l VNOACCENT # = e )                                       ;; 15.35
 ( t [ e1 ] l VNOACCENT # = e )                                       ;; 15.40
 ( v [ e1 ] l VNOACCENT # = e )                                       ;; 15.45
 ( g [ e1 ] l VNOACCENT # = e )                                       ;; 15.50
 ( b [ a1 ] l VNOACCENTROUND # = a )                                  ;; 15.90
 ( f [ a1 ] l VNOACCENTROUND # = a )                                  ;; 15.95

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress8c
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + VNOACCENT l VNOACCENT # = i1 )                        ;; 16.00
 ( [ e ] KONS + VNOACCENT l VNOACCENT # = e1 )                        ;; 16.00
 ( [ E ] KONS + VNOACCENT l VNOACCENT # = E1 )                        ;; 16.00
 ( [ a ] KONS + VNOACCENT l VNOACCENT # = a1 )                        ;; 16.00
 ( [ o ] KONS + VNOACCENT l VNOACCENT # = o1 )                        ;; 16.00
 ( [ O ] KONS + VNOACCENT l VNOACCENT # = O1 )                        ;; 16.00
 ( [ u ] KONS + VNOACCENT l VNOACCENT # = u1 )                        ;; 16.00
 ( [ i ] KONS + i3 VNOACCENT l VNOACCENT # = i1 )                     ;; 16.05
 ( [ e ] KONS + i3 VNOACCENT l VNOACCENT # = e1 )                     ;; 16.05
 ( [ E ] KONS + i3 VNOACCENT l VNOACCENT # = E1 )                     ;; 16.05
 ( [ a ] KONS + i3 VNOACCENT l VNOACCENT # = a1 )                     ;; 16.05
 ( [ o ] KONS + i3 VNOACCENT l VNOACCENT # = o1 )                     ;; 16.05
 ( [ O ] KONS + i3 VNOACCENT l VNOACCENT # = O1 )                     ;; 16.05
 ( [ u ] KONS + i3 VNOACCENT l VNOACCENT # = u1 )                     ;; 16.05

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s ) 
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress9a
;  Sets used in the rules
(
  (VDOLCNOACCENT i e E)
)
;  Rules
(
 ( g [ i ] m VDOLCNOACCENT # = i1 )                                   ;; 16.10
 ( h [ i ] m VDOLCNOACCENT # = i1 )                                   ;; 16.15
 ( l [ i ] m VDOLCNOACCENT # = i1 )                                   ;; 16.20

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress9b
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + i m VNOACCENT # = i1 )                                ;; 16.25
 ( [ e ] KONS + i m VNOACCENT # = e1 )                                ;; 16.25
 ( [ E ] KONS + i m VNOACCENT # = E1 )                                ;; 16.25
 ( [ a ] KONS + i m VNOACCENT # = a1 )                                ;; 16.25
 ( [ o ] KONS + i m VNOACCENT # = o1 )                                ;; 16.25
 ( [ O ] KONS + i m VNOACCENT # = O1 )                                ;; 16.25
 ( [ u ] KONS + i m VNOACCENT # = u1 )                                ;; 16.25

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress10a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + VNOACCENT p l VNOACCENT # = i1 )                      ;; 16.30
 ( [ e ] KONS + VNOACCENT p l VNOACCENT # = e1 )                      ;; 16.30
 ( [ E ] KONS + VNOACCENT p l VNOACCENT # = E1 )                      ;; 16.30
 ( [ a ] KONS + VNOACCENT p l VNOACCENT # = a1 )                      ;; 16.30
 ( [ o ] KONS + VNOACCENT p l VNOACCENT # = o1 )                      ;; 16.30
 ( [ O ] KONS + VNOACCENT p l VNOACCENT # = O1 )                      ;; 16.30
 ( [ u ] KONS + VNOACCENT p l VNOACCENT # = u1 )                      ;; 16.30
 ( [ i ] KONS + i3 VNOACCENT p l VNOACCENT # = i1 )                   ;; 16.35
 ( [ e ] KONS + i3 VNOACCENT p l VNOACCENT # = e1 )                   ;; 16.35
 ( [ E ] KONS + i3 VNOACCENT p l VNOACCENT # = E1 )                   ;; 16.35
 ( [ a ] KONS + i3 VNOACCENT p l VNOACCENT # = a1 )                   ;; 16.35
 ( [ o ] KONS + i3 VNOACCENT p l VNOACCENT # = o1 )                   ;; 16.35
 ( [ O ] KONS + i3 VNOACCENT p l VNOACCENT # = O1 )                   ;; 16.35
 ( [ u ] KONS + i3 VNOACCENT p l VNOACCENT # = u1 )                   ;; 16.35

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress11a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (VNOACCENTROUND i e E a)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + VNOACCENTROUND t r VNOACCENT # = i1 )                 ;; 16.40
 ( [ e ] KONS + VNOACCENTROUND t r VNOACCENT # = e1 )                 ;; 16.40
 ( [ E ] KONS + VNOACCENTROUND t r VNOACCENT # = E1 )                 ;; 16.40
 ( [ a ] KONS + VNOACCENTROUND t r VNOACCENT # = a1 )                 ;; 16.40
 ( [ o ] KONS + VNOACCENTROUND t r VNOACCENT # = o1 )                 ;; 16.40
 ( [ O ] KONS + VNOACCENTROUND t r VNOACCENT # = O1 )                 ;; 16.40
 ( [ u ] KONS + VNOACCENTROUND t r VNOACCENT # = u1 )                 ;; 16.40
 ( [ i ] KONS + i3 VNOACCENTROUND t r VNOACCENT # = i1 )              ;; 16.45
 ( [ e ] KONS + i3 VNOACCENTROUND t r VNOACCENT # = e1 )              ;; 16.45
 ( [ E ] KONS + i3 VNOACCENTROUND t r VNOACCENT # = E1 )              ;; 16.45
 ( [ a ] KONS + i3 VNOACCENTROUND t r VNOACCENT # = a1 )              ;; 16.45
 ( [ o ] KONS + i3 VNOACCENTROUND t r VNOACCENT # = o1 )              ;; 16.45
 ( [ O ] KONS + i3 VNOACCENTROUND t r VNOACCENT # = O1 )              ;; 16.45
 ( [ u ] KONS + i3 VNOACCENTROUND t r VNOACCENT # = u1 )              ;; 16.45

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress12a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + e b r VNOACCENT # = i1 )                              ;; 16.50
 ( [ e ] KONS + e b r VNOACCENT # = e1 )                              ;; 16.50
 ( [ E ] KONS + e b r VNOACCENT # = E1 )                              ;; 16.50
 ( [ a ] KONS + e b r VNOACCENT # = a1 )                              ;; 16.50
 ( [ o ] KONS + e b r VNOACCENT # = o1 )                              ;; 16.50
 ( [ O ] KONS + e b r VNOACCENT # = O1 )                              ;; 16.50
 ( [ u ] KONS + e b r VNOACCENT # = u1 )                              ;; 16.50
 ( [ i ] KONS + i3 e b r VNOACCENT # = i1 )                           ;; 16.55
 ( [ e ] KONS + i3 e b r VNOACCENT # = e1 )                           ;; 16.55
 ( [ E ] KONS + i3 e b r VNOACCENT # = E1 )                           ;; 16.55
 ( [ a ] KONS + i3 e b r VNOACCENT # = a1 )                           ;; 16.55
 ( [ o ] KONS + i3 e b r VNOACCENT # = o1 )                           ;; 16.55
 ( [ O ] KONS + i3 e b r VNOACCENT # = O1 )                           ;; 16.55
 ( [ u ] KONS + i3 e b r VNOACCENT # = u1 )                           ;; 16.55

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress13a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + u b r VNOACCENT # = i1 )                              ;; 16.60
 ( [ e ] KONS + u b r VNOACCENT # = e1 )                              ;; 16.60
 ( [ E ] KONS + u b r VNOACCENT # = E1 )                              ;; 16.60
 ( [ a ] KONS + u b r VNOACCENT # = a1 )                              ;; 16.60
 ( [ o ] KONS + u b r VNOACCENT # = o1 )                              ;; 16.60
 ( [ O ] KONS + u b r VNOACCENT # = O1 )                              ;; 16.60
 ( [ u ] KONS + u b r VNOACCENT # = u1 )                              ;; 16.60
 ( [ i ] KONS + i3 u b r VNOACCENT # = i1 )                           ;; 16.65
 ( [ e ] KONS + i3 u b r VNOACCENT # = e1 )                           ;; 16.65
 ( [ E ] KONS + i3 u b r VNOACCENT # = E1 )                           ;; 16.65
 ( [ a ] KONS + i3 u b r VNOACCENT # = a1 )                           ;; 16.65
 ( [ o ] KONS + i3 u b r VNOACCENT # = o1 )                           ;; 16.65
 ( [ O ] KONS + i3 u b r VNOACCENT # = O1 )                           ;; 16.65
 ( [ u ] KONS + i3 u b r VNOACCENT # = u1 )                           ;; 16.65

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress14a
;  Sets used in the rules
(
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + o n o # = i1 )                                        ;; 16.70
 ( [ e ] KONS + o n o # = e1 )                                        ;; 16.70
 ( [ E ] KONS + o n o # = E1 )                                        ;; 16.70
 ( [ a ] KONS + o n o # = a1 )                                        ;; 16.70
 ( [ o ] KONS + o n o # = o1 )                                        ;; 16.70
 ( [ O ] KONS + o n o # = O1 )                                        ;; 16.70
 ( [ u ] KONS + o n o # = u1 )                                        ;; 16.70
; ( [ i ] KONS + i3 o n o # = i1 )                                     ;; 16.75
; ( [ e ] KONS + i3 o n o # = e1 )                                     ;; 16.75
; ( [ E ] KONS + i3 o n o # = E1 )                                     ;; 16.75
; ( [ a ] KONS + i3 o n o # = a1 )                                     ;; 16.75
; ( [ o ] KONS + i3 o n o # = o1 )                                     ;; 16.75
; ( [ O ] KONS + i3 o n o # = O1 )                                     ;; 16.75
; ( [ u ] KONS + i3 o n o # = u1 )                                     ;; 16.75

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress15a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (VNOACCENTROUND i e E a)
)
;  Rules
(
 ( m [ a ] n VNOACCENTROUND # = a1 )                                  ;; 16.80

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress15b
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (VNOACCENTROUND i e E a)
)
;  Rules
(
 ( # r o m [ a1 ] n VNOACCENT # = a1 )                                ;; 16.90
 ( o m [ a1 ] n VNOACCENTROUND # = a )                                ;; 16.85
;;  ATT: la regola 16.85 � dopo la .90 per parole come pir'omane 

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress15c
;  Sets used in the rules
(
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( [ i ] KONS + a n o # = i1 )                                        ;; 16.95
 ( [ e ] KONS + a n o # = e1 )                                        ;; 16.95
 ( [ E ] KONS + a n o # = E1 )                                        ;; 16.95
 ( [ a ] KONS + a n o # = a1 )                                        ;; 16.95
 ( [ o ] KONS + a n o # = o1 )                                        ;; 16.95
 ( [ O ] KONS + a n o # = O1 )                                        ;; 16.95
 ( [ u ] KONS + a n o # = u1 )                                        ;; 16.95
 ( [ i ] KONS + i3 a n o # = i1 )                                     ;; 17.00
 ( [ e ] KONS + i3 a n o # = e1 )                                     ;; 17.00
 ( [ E ] KONS + i3 a n o # = E1 )                                     ;; 17.00
 ( [ a ] KONS + i3 a n o # = a1 )                                     ;; 17.00
 ( [ o ] KONS + i3 a n o # = o1 )                                     ;; 17.00
 ( [ O ] KONS + i3 a n o # = O1 )                                     ;; 17.00
 ( [ u ] KONS + i3 a n o # = u1 )                                     ;; 17.00

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress16a
;  Sets used in the rules
((NIL nil)) 
;  Rules
(
 ( [ u ] l m i n e # = u1 )                                           ;; 17.05
 ( [ e ] r c i n e # = e1 )                                           ;; 17.10
 ( g l [ i ] c i n e # = i1 )                                         ;; 17.15
 ( [ i ] m i n e # = i1 )                                             ;; 17.20
 ( [ e ] d i n e # = e1 )                                             ;; 17.25
 ( [ i ] n e # = i1 )                                                 ;; 17.30

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress16b
;  Sets used in the rules
((NIL nil))
;  Rules
(
 ( d [ i1 ] n e # = i )                                               ;; 17.35
 ( g [ i1 ] n e # = i )                                               ;; 17.40

 ( [ i3 ] = i3 )
 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress17a
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (VNOACCENTROUNDALTA e E a)
  (VNOACCENTBASACCAB i o O)
  (VNOACCENTALTA i e E a o O)
  (VDOLCNOACCENT i e E)
  (VNOACCENTROUND i e E a)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
  (KNASANT m n)
)
;  Rules
(
 ( [ i ] n c r o n VNOACCENT # = i1 )                                 ;; 17.50
 ( [ e ] KNASANT m i r VNOACCENTBASACCAB # = e1 )                     ;; 17.55
 ( [ e ] KNASANT v i r VNOACCENTBASACCAB # = e1 )                     ;; 17.60
 ( [ u ] KNASANT m i r VNOACCENTBASACCAB # = u1 )                     ;; 17.65
 ( [ u ] KNASANT v i r VNOACCENTBASACCAB # = u1 )                     ;; 17.70
 ( l [ e ] d o n e # = e1 )                                           ;; 17.75
 ( e m [ o ] r VNOACCENTROUNDALTA # = o1 )                            ;; 17.80
 ( [ a ] u s t VNOACCENT # = a1 )                                     ;; 17.85
 ( [ a ] n g u VNOACCENT # = a1 )                                     ;; 17.90
 ( [ i ] f i s i # = i1 )                                             ;; 17.95
 ( [ o ] f i s i # = o1 )                                             ;; 17.95
 ( [ O ] f i s i # = O1 )                                             ;; 17.95
 ( [ o ] n o m VNOACCENTALTA # = o1 )                                 ;; 18.00
 ( [ i ] g a r o # = i1 )                                             ;; 18.05
 ( [ e ] r e s i # = e1 )                                             ;; 18.10
 ( [ i ] t e s i # = i1 )                                             ;; 18.15
 ( [ a ] t e s i # = a1 )                                             ;; 18.15
 ( [ a ] u g h VDOLCNOACCENT # = a1 )                                 ;; 18.20
 ( [ i ] KONS * f u g h VDOLCNOACCENT # = i1 )                        ;; 18.25
 ( [ e ] KONS * f u g h VDOLCNOACCENT # = e1 )                        ;; 18.25
 ( [ E ] KONS * f u g h VDOLCNOACCENT # = E1 )                        ;; 18.25
 ( [ a ] KONS * f u g h VDOLCNOACCENT # = a1 )                        ;; 18.25
 ( [ o ] KONS * f u g h VDOLCNOACCENT # = o1 )                        ;; 18.25
 ( [ O ] KONS * f u g h VDOLCNOACCENT # = O1 )                        ;; 18.25
 ( [ u ] KONS * f u g h VDOLCNOACCENT # = u1 )                        ;; 18.25
 ( [ i ] KONS * d o g g i # = i1 )                                    ;; 18.30
 ( [ e ] KONS * d o g g i # = e1 )                                    ;; 18.30
 ( [ E ] KONS * d o g g i # = E1 )                                    ;; 18.30
 ( [ a ] KONS * d o g g i # = a1 )                                    ;; 18.30
 ( [ o ] KONS * d o g g i # = o1 )                                    ;; 18.30
 ( [ O ] KONS * d o g g i # = O1 )                                    ;; 18.30
 ( [ u ] KONS * d o g g i # = u1 )                                    ;; 18.30
 ( [ i ] KONS * s o g g i # = i1 )                                    ;; 18.35
 ( [ e ] KONS * s o g g i # = e1 )                                    ;; 18.35
 ( [ E ] KONS * s o g g i # = E1 )                                    ;; 18.35
 ( [ a ] KONS * s o g g i # = a1 )                                    ;; 18.35
 ( [ o ] KONS * s o g g i # = o1 )                                    ;; 18.35
 ( [ O ] KONS * s o g g i # = O1 )                                    ;; 18.35
 ( [ u ] KONS * s o g g i # = u1 )                                    ;; 18.35
 ( [ i ] KONS * t o g g i # = i1 )                                    ;; 18.40
 ( [ e ] KONS * t o g g i # = e1 )                                    ;; 18.40
 ( [ E ] KONS * t o g g i # = E1 )                                    ;; 18.40
 ( [ a ] KONS * t o g g i # = a1 )                                    ;; 18.40
 ( [ o ] KONS * t o g g i # = o1 )                                    ;; 18.40
 ( [ O ] KONS * t o g g i # = O1 )                                    ;; 18.40
 ( [ u ] KONS * t o g g i # = u1 )                                    ;; 18.40
 ( [ i ] KONS * d o g l i # = i1 )                                    ;; 18.45
 ( [ e ] KONS * d o g l i # = e1 )                                    ;; 18.45
 ( [ E ] KONS * d o g l i # = E1 )                                    ;; 18.45
 ( [ a ] KONS * d o g l i # = a1 )                                    ;; 18.45
 ( [ o ] KONS * d o g l i # = o1 )                                    ;; 18.45
 ( [ O ] KONS * d o g l i # = O1 )                                    ;; 18.45
 ( [ u ] KONS * d o g l i # = u1 )                                    ;; 18.45
 ( [ i ] KONS * s o g l i # = i1 )                                    ;; 18.50
 ( [ e ] KONS * s o g l i # = e1 )                                    ;; 18.50
 ( [ E ] KONS * s o g l i # = E1 )                                    ;; 18.50
 ( [ a ] KONS * s o g l i # = a1 )                                    ;; 18.50
 ( [ o ] KONS * s o g l i # = o1 )                                    ;; 18.50
 ( [ O ] KONS * s o g l i # = O1 )                                    ;; 18.50
 ( [ u ] KONS * s o g l i # = u1 )                                    ;; 18.50
 ( [ i ] KONS * t o g l i # = i1 )                                    ;; 18.55
 ( [ e ] KONS * t o g l i # = e1 )                                    ;; 18.55
 ( [ E ] KONS * t o g l i # = E1 )                                    ;; 18.55
 ( [ a ] KONS * t o g l i # = a1 )                                    ;; 18.55
 ( [ o ] KONS * t o g l i # = o1 )                                    ;; 18.55
 ( [ O ] KONS * t o g l i # = O1 )                                    ;; 18.55
 ( [ u ] KONS * t o g l i # = u1 )                                    ;; 18.55
 ( b [ i ] a s i # = i1 )                                             ;; 18.60
 ( m [ i ] a s i # = i1 )                                             ;; 18.65
 ( r [ i ] a s i # = i1 )                                             ;; 18.70
 ( s [ i ] a s i # = i1 )                                             ;; 18.75
 ( t [ i ] a s i # = i1 )                                             ;; 18.80
 ( [ i ] a d VDOLCNOACCENT # = i1 )                                   ;; 18.85
 ( [ i ] f o n VNOACCENTROUND # = i1 )                                ;; 18.90
 ( [ e ] f o n VNOACCENTROUND # = e1 )                                ;; 18.90
 ( [ E ] f o n VNOACCENTROUND # = E1 )                                ;; 18.90
 ( [ a ] f o n VNOACCENTROUND # = a1 )                                ;; 18.90
 ( [ o ] f o n VNOACCENTROUND # = o1 )                                ;; 18.90
 ( [ O ] f o n VNOACCENTROUND # = O1 )                                ;; 18.90
 ( [ u ] f o n VNOACCENTROUND # = u1 )                                ;; 18.90
 ( [ e ] i d VDOLCNOACCENT # = e1 )                                   ;; 18.95
 ( [ E ] i d VDOLCNOACCENT # = E1 )                                   ;; 18.95
 ( [ o ] i d VDOLCNOACCENT # = o1 )                                   ;; 18.95
 ( [ O ] i d VDOLCNOACCENT # = O1 )                                   ;; 18.95
 ( [ a ] c e VNOACCENT # = a1 )                                       ;; 19.00
 ( [ i ] c e VNOACCENT # = i1 )                                       ;; 19.05
 ( [ u ] c e VNOACCENT # = u1 )                                       ;; 19.05
 ( [ i ] n e VNOACCENT # = i1 )                                       ;; 19.10
 ( [ e ] n e VNOACCENT # = e1 )                                       ;; 19.10
 ( [ E ] n e VNOACCENT # = E1 )                                       ;; 19.10
 ( [ a ] n e VNOACCENT # = a1 )                                       ;; 19.10
 ( [ o ] n e VNOACCENT # = o1 )                                       ;; 19.10
 ( [ O ] n e VNOACCENT # = O1 )                                       ;; 19.10
 ( [ u ] n e VNOACCENT # = u1 )                                       ;; 19.10
 ( [ i ] r e VNOACCENT # = i1 )                                       ;; 19.15
 ( [ e ] r e VNOACCENT # = e1 )                                       ;; 19.15
 ( [ E ] r e VNOACCENT # = E1 )                                       ;; 19.15
 ( [ a ] r e VNOACCENT # = a1 )                                       ;; 19.15
 ( [ o ] r e VNOACCENT # = o1 )                                       ;; 19.15
 ( [ O ] r e VNOACCENT # = O1 )                                       ;; 19.15
 ( [ u ] r e VNOACCENT # = u1 )                                       ;; 19.15
 ( [ e ] u m VNOACCENT # = e1 )                                       ;; 19.20
 ( [ E ] u m VNOACCENT # = E1 )                                       ;; 19.20
 ( [ a ] u m VNOACCENT # = a1 )                                       ;; 19.20
 ( [ e ] u r VNOACCENT # = e1 )                                       ;; 19.25
 ( [ E ] u r VNOACCENT # = E1 )                                       ;; 19.25
 ( [ a ] u r VNOACCENT # = a1 )                                       ;; 19.25
 ( [ e ] u s VNOACCENT # = e1 )                                       ;; 19.30
 ( [ E ] u s VNOACCENT # = E1 )                                       ;; 19.30
 ( [ a ] u s VNOACCENT # = a1 )                                       ;; 19.30
 ( [ e ] u t VNOACCENT # = e1 )                                       ;; 19.35
 ( [ E ] u t VNOACCENT # = E1 )                                       ;; 19.35
 ( [ a ] u t VNOACCENT # = a1 )                                       ;; 19.35
 ( [ i ] d u a # = i1 )                                               ;; 19.40
 ( [ a ] d u a # = a1 )                                               ;; 19.40
 ( [ i ] o d VNOACCENTBASACCAB # = i1 )                               ;; 19.45
 ( [ e ] q u i # = e1 )                                               ;; 19.50
 ( [ E ] q u i # = E1 )                                               ;; 19.50
 ( [ o ] q u i # = o1 )                                               ;; 19.50
 ( [ O ] q u i # = O1 )                                               ;; 19.50
;;; ATT: da qui ritornano le caratteristiche originali delle vocali e E u
 ( [ i3 ] = i )                                                       ;; 19.85 ;; i3 diventa i
 ( [ q u ] VOK = k w )                                                ;; 19.90 ;; Fabio come mai questa regola � qui?

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress17b
;  Sets used in the rules
(
  (VNOACCENT a e i o u E O)
  (KNONAS j w p t k b d g f s S v z ts tS dz dZ l L r)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
  (NOACCENT a e E i o O u j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( # NOACCENT * [ i ] KONS * VNOACCENT KONS * # = i1 )                ;; 19.95
 ( # NOACCENT * [ e ] KONS * VNOACCENT KONS * # = e1 )                ;; 19.95
 ( # NOACCENT * [ E ] KONS * VNOACCENT KONS * # = E1 )                ;; 19.95
 ( # NOACCENT * [ a ] KONS * VNOACCENT KONS * # = a1 )                ;; 19.95
 ( # NOACCENT * [ o ] KONS * VNOACCENT KONS * # = o1 )                ;; 19.95
 ( # NOACCENT * [ O ] KONS * VNOACCENT KONS * # = O1 )                ;; 19.95
 ( # NOACCENT * [ u ] KONS * VNOACCENT KONS * # = u1 )                ;; 19.95
 ( # NOACCENT * [ i ] KONS * q u VNOACCENT KONS * # = i1 )            ;; 19.95
 ( # NOACCENT * [ e ] KONS * q u VNOACCENT KONS * # = e1 )            ;; 19.95
 ( # NOACCENT * [ E ] KONS * q u VNOACCENT KONS * # = E1 )            ;; 19.95
 ( # NOACCENT * [ a ] KONS * q u VNOACCENT KONS * # = a1 )            ;; 19.95
 ( # NOACCENT * [ o ] KONS * q u VNOACCENT KONS * # = o1 )            ;; 19.95
 ( # NOACCENT * [ O ] KONS * q u VNOACCENT KONS * # = O1 )            ;; 19.95
 ( # NOACCENT * [ u ] KONS * q u VNOACCENT KONS * # = u1 )            ;; 19.95

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_stress17c
;  Sets used in the rules
(
  (VACCENT a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
  (ALL a e i o u E O a1 e1 i1 o1 u1 E1 O1 j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y)
)
;  Rules
(
 ( # KONS * [ i ] KONS * # = i1 )                                     ;; 20.00
 ( # KONS * [ e ] KONS * # = e1 )                                     ;; 20.00
 ( # KONS * [ E ] KONS * # = E1 )                                     ;; 20.00
 ( # KONS * [ a ] KONS * # = a1 )                                     ;; 20.00
 ( # KONS * [ o ] KONS * # = o1 )                                     ;; 20.00
 ( # KONS * [ O ] KONS * # = O1 )                                     ;; 20.00
 ( # KONS * [ u ] KONS * # = u1 )                                     ;; 20.00
; ( # KONS * [ i1 ] KONS * apo = i )                                   ;; 20.01
; ( # KONS * [ e1 ] KONS * apo = e )                                   ;; 20.01
; ( # KONS * [ E1 ] KONS * apo = E )                                   ;; 20.01
; ( # KONS * [ a1 ] KONS * apo = a )                                   ;; 20.01
; ( # KONS * [ o1 ] KONS * apo = o )                                   ;; 20.01
; ( # KONS * [ O1 ] KONS * apo = O )                                   ;; 20.01
; ( # KONS * [ u1 ] KONS * apo = u )                                   ;; 20.01
 ( VACCENT ALL * [ i1 ] = i )                                         ;; 20.05
 ( VACCENT ALL * [ e1 ] = e )                                         ;; 20.05
 ( VACCENT ALL * [ E1 ] = E )                                         ;; 20.05
 ( VACCENT ALL * [ a1 ] = a )                                         ;; 20.05
 ( VACCENT ALL * [ o1 ] = o )                                         ;; 20.05
 ( VACCENT ALL * [ O1 ] = O )                                         ;; 20.05
 ( VACCENT ALL * [ u1 ] = u )                                         ;; 20.05

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


;;; QUI METTO LE REGOLE GRAFEMA-FONEMA!!!

;;; TRASCRIZIONE GRAFEMA FONEMA


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo1
;  Sets used in the rules
(
  (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (VDOLC i e E i1 e1 E1 e1s E1s)
  (VNODOLC a o O u a1 o1 O1 u1)                                       ;;        CORR.1
  (NONAS a e i o u E O a1 e1 i1 o1 u1 E1 O1 j w p t k b d g f s S v z ts tS dz dZ l L r e1s E1s o1s O1s) 
)
;  Rules
(
 ( [ q u ] VOK = k w )                                                ;; 19.90 ;;copia di quella in stress17a
 ( [ i3 ] = )   ;;toglie la i3 nel caso delle function word
 ( [ g l ] VNODOLC = g l )                                            ;; 20.15  CORR.1
 ( NONAS [ g l ] = L )                                                ;; 20.15
 ( g l [ i ] VOK = )                                                  ;; 20.16  CORR.1
 ( # [ g l ] i e l = L )                                              ;; 20.23
 ( # [ g l ] i e1 l = L )                                             ;; 20.23
 ( [ g n i ] VOK = J1 )                                               ;; 20.25  CORR.2
 ( [ g n i1 ] VOK = J i1 )                                            ;; 20.25  CORR.2
 ( [ g n ] = J )                                                      ;; 20.27
 ( n [ g i ] VOK = dZ )                                               ;; 20.30
 ( n [ g ] VDOLC = dZ )                                               ;; 20.35
 ( [ s c i ] VOK = S )                                                ;; 20.40
 ( [ s c ] VDOLC = S )                                                ;; 20.45
 ( [ g g i ] VOK = dZ dZ )                                            ;; 20.50
 ( [ g g ] VDOLC = dZ dZ )                                            ;; 20.55
 ( [ g i ] VOK = dZ )                                                 ;; 20.60
 ( [ g ] VDOLC = dZ )                                                 ;; 20.65
 ( [ c c i ] VOK = tS tS )                                            ;; 20.70
 ( [ c c ] VDOLC = tS tS )                                            ;; 20.75
 ( [ c i ] VOK = tS )                                                 ;; 20.80
 ( [ c ] VDOLC = tS )                                                 ;; 20.85
 ( c [ c ] = k )                                                      ;; 20.90
 ( [ c ] = k )                                                        ;; 20.90

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ apo ] = apo )
 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo2
;  Sets used in the rules
(
  (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y SS tts ttS ddz ddZ JJ ng:)
  (KOCCLNOBORDCORANT k g)
  (KVOICE j w b d g v z dz dZ m n J ng l L r ddz ddZ JJ ng: LL)
  (ALL a e i o u E O a1 e1 i1 o1 u1 E1 O1 j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y SS tts ttS ddz ddZ JJ ng: LL e1s E1s o1s O1s)
)
;  Rules
(

 ( [ n g ] = ng g )                                                   ;; 20.92  CORR.1 ; simboli che forse verranno sostituiti con + g, + k;;; tipo ( [ n g ] = ng g)
 ( [ n k ] = ng k )                                                   ;; 21.00  CORR.1
 ( [ n f ] = nf f )                                                   ;; Fabio  inserito nuovo fonema
 ( [ n v ] = nf v )                                                   ;; Fabio  inserito nuovo fonema
;;; la 20.92 che segue si puo' ignorare
; ( [ n ] KOCCLNOBORDCORANT = ng )                              (???)  ;; 20.92
 ( # [ z ] VOK KVOICE + VOK = dz )                                    ;; 21.05
 ( # [ z ] VOK VOK = dz )                                             ;; 21.10
 ( [ z z ] = ts ts )                                                  ;; 21.15
 ( VOK [ z ] i VOK = ts ts )                                          ;; 21.20
 ( VOK [ z ] i1 VOK = ts ts )                                         ;; 21.20
 ( VOK [ z ] i # = ts ts )                                            ;; 21.25
 ( VOK [ z ] VOK ALL = dz dz )                                        ;; 21.30
 ( [ z ] = ts )                                                       ;; 21.35
 ( [ h ] = )                                                          ;; 21.45
 ( [ s ] KVOICE = z )                                                 ;; 21.50
 ( VOK [ s ] VOK = z )                                                ;; 21.55
 ( [ y ] = i )                                                        ;; 21.60
 ( [ x ] = k s )                                                      ;; 21.65
 ( KONS [ i ] VOK = j )                                               ;; 21.70
 ( VOK [ i ] VOK = j )                                                ;; 21.73
 ( # [ j ] v a = i )                                                  ;; 21.71
 ( # [ j ] v a1 = i )                                                 ;; 21.71
 ( # r [ j ] VOK ALL = i )                                            ;; 21.75

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ L ] = L )
 
 ( [ apo ] = apo )

 ( [ J1 ] = J1 )                                                      ;; 24.80  CORR.2

 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo3
;  Sets used in the rules
(
  (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
)
;  Rules
(
 ( [ u ] VOK = w )                                                    ;; 21.80

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ nf ] = nf )
 ( [ L ] = L )
 
 ( [ apo ] = apo )

 ( [ J1 ] = J1 )                                                      ;; 24.80  CORR.2

 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo4
;  Sets used in the rules
(
  (VACCENT a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (VDOLC i e E i1 e1 E1 e1s E1s)
)
;  Rules
(
 ( [ w ] VACCENT t VOK # = u )                                        ;; 21.85
 ( [ w ] o1 z VOK # = u )                                             ;; 21.90
 ( [ w ] a1 z VDOLC # = u )                                           ;; 21.95

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ nf ] = nf )
 ( [ L ] = L )
 
 ( [ apo ] = apo )

 ( [ J1 ] = J1 )                                                      ;; 24.80  CORR.2

 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo5
;  Sets used in the rules
(
  (VDOLC i e E i1 e1 E1 e1s E1s)
  (KALTARETROCCLNOANT k g)   ;;; e' uguale a KOCCLNOBORDCORANT
)
;  Rules
(
 ( KALTARETROCCLNOANT [ u ] a1 l VDOLC # = w )                        ;; 21.96

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ nf ] = nf )
 ( [ L ] = L )
 
 ( [ apo ] = apo )

 ( [ J1 ] = J1 )                                                      ;; 24.80  CORR.2

 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo6
;  Sets used in the rules
(
  (VACCENT a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y SS tts ttS ddz ddZ JJ ng:)
  (KNORETR j S SS J JJ)   
;;;
;;;  ATT: non ho messo le consonanti con caratteristica BACK (RETR) = 0!!!
;;;
  (ALL a e i o u E O a1 e1 i1 o1 u1 E1 O1 j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y SS tts ttS ddz ddZ JJ ng: LL)
)
;  Rules
(
 ( ALL KNORETR [ w ] i1 r e # = u )                                   ;; 22.00
 ( ALL r [ w ] i1 r e # = u )                                         ;; 22.05
 ( KONS KONS r [ w ] VACCENT = u )                                    ;; 22.06
 ( [ o1 ] = O1 )                                                      ;; 22.10

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ nf ] = nf )
 ( [ L ] = L )
 
 ( [ apo ] = apo )

 ( [ J1 ] = J1 )                                                      ;; 24.80  CORR.2

 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo7
;  Sets used in the rules
(
  (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (VDOLC i e E i1 e1 E1)
)
;  Rules
(
 ( [ O1 ] tS VDOLC # = o1 )                                           ;; 22.15
 ( [ O1 ] J VOK # = o1 )                                              ;; 22.20
 ( [ O1 ] J o l VOK # = o1 )                                          ;; 22.25
 ( [ O1 ] j VOK # = o1 )                                              ;; 22.30
 ( [ O1 ] n d VOK # = o1 )                                            ;; 22.35
 ( [ O1 ] n VDOLC # = o1 )                                            ;; 22.40
 ( [ O1 ] n a # = o1 )                                                ;; 22.45
 ( [ O1 ] n t VOK # = o1 )                                            ;; 22.50
 ( [ O1 ] n t r VOK = o1 )                                            ;; 22.51
 ( [ O1 ] n ts o l VOK # = o1 )                                       ;; 22.55
 ( [ O1 ] r VDOLC # = o1 )                                            ;; 22.60
 ( [ O1 ] z VOK # = o1 )                                              ;; 22.65
 ( [ O1 ] r # = o1 )                                                  ;; 22.70
 ( [ O1 ] r r e # = o1 )                                              ;; 22.75
 ( f [ O1 ] r m e # = o1 )                                            ;; 22.80
 ( [ O1 ] r s VOK = o1 )                                              ;; 22.81
 ( [ O1 ] r n VOK = o1 )                                              ;; 22.82
 ( [ O1 ] s t VOK = o1 )                                              ;; 22.83
 ( [ O1 ] m b VOK = o1 )                                              ;; 22.84
 ( [ O1 ] m p VOK = o1 )                                              ;; 22.86
 ( [ O1 ] m p r VOK = o1 )                                            ;; 22.87

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ nf ] = nf )
 ( [ L ] = L )
 
 ( [ apo ] = apo )

 ( [ J1 ] = J1 )                                                      ;; 24.80  CORR.2

 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo8
;  Sets used in the rules
((NIL nil))
;  Rules
(
 ( w [ o1 ] = O1 )                                                    ;; 22.85
 ( [ e1 ] = E1 )                                                      ;; 22.90

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ nf ] = nf )
 ( [ L ] = L )
 
 ( [ apo ] = apo )

 ( [ J1 ] = J1 )                                                      ;; 24.80  CORR.2

 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo9
;  Sets used in the rules
(
  (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (VDOLC i e E i1 e1 E1 e1s E1s)
  (VNODOLCALTA a o O a1 o1 O1 o1s O1s)
  (KONS j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y tts ttS ddz ddZ JJ ng: LL)
  (ALL a e i o u E O a1 e1 i1 o1 u1 E1 O1 j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y SS tts ttS ddz ddZ JJ ng: LL e1s E1s o1s O1s)
)
;  Rules
(
 ( VOK KONS * [ E1 ] # = e1 )                                         ;; 22.95
 ( [ E1 ] tS tS o # = e1 )                                            ;; 23.00
 ( [ E1 ] tS tS i # = e1 )                                            ;; 23.05
 ( [ E1 ] dZ dZ o # = e1 )                                            ;; 23.10
 ( [ E1 ] dZ dZ i # = e1 )                                            ;; 23.15
 ( [ E1 ] f i tS VDOLC # = e1 )                                       ;; 23.20
 ( [ E1 ] f i tS tS VDOLC # = e1 )                                    ;; 23.20
 ( m [ E1 ] n t o # = e1 )                                            ;; 23.25
 ( m [ E1 ] n t VDOLC # = e1 )                                        ;; 23.30
 ( ALL [ E1 ] s k o # = e1 )                                          ;; 23.35
 ( ALL [ E1 ] s k i # = e1 )                                          ;; 23.40
 ( [ E1 ] z VOK # = e1 )                                              ;; 23.45
 ( [ E1 ] s s a # = e1 )                                              ;; 23.60
 ( [ E1 ] s s e # = e1 )                                              ;; 23.65
 ( [ E1 ] t VOK # = e1 )                                              ;; 23.70
 ( [ E1 ] v o l VDOLC # = e1 )                                        ;; 23.75
 ( [ E1 ] ts ts a # = e1 )                                            ;; 23.80
 ( [ E1 ] ts ts e # = e1 )                                            ;; 23.85
 ( ALL [ E1 ] t t VOK # = e1 )                                        ;; 23.90
 ( # k w [ E1 ] ALL = e1 )                                            ;; 23.95
 ( ALL [ E1 ] v VNODOLCALTA # = e1 )                                  ;; 24.00
 ( ALL [ E1 ] v a n o # = e1 )                                        ;; 24.05
 ( ALL [ E1 ] m m o # = e1 )                                          ;; 24.10
 ( ALL [ E1 ] v a n o # = e1 )                                        ;; 24.15
 ( ALL r [ E1 ] s t VDOLC # = e1 )                                    ;; 24.20
 ( ALL [ E1 ] r o n o # = e1 )                                        ;; 24.25
 ( ALL r [ E1 ] m o # = e1 )                                          ;; 24.30
 ( ALL r [ E1 ] t e # = e1 )                                          ;; 24.35
 ( ALL [ E1 ] s s i m o # = e1 )                                      ;; 24.40
 ( ALL [ E1 ] s s e r o # = e1 )                                      ;; 24.45
 ( ALL [ E1 ] r # = e1 )                                              ;; 24.50
 ( ALL [ E1 ] r e # = e1 )                                            ;; 24.50
 ( ALL [ E1 ] m p l VOK = e1 )                                        ;; 24.61
 ( ALL [ E1 ] n t r VOK = e1 )                                        ;; 24.62
 ( ALL [ E1 ] r l VOK = e1 )                                          ;; 24.63

 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1s )
 ( [ E1s ] = E1s )
 ( [ o1s ] = o1s )
 ( [ O1s ] = O1s )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ nf ] = nf )
 ( [ L ] = L )
 
 ( [ apo ] = apo )

 ( [ J1 ] = J1 )                                                      ;; 24.80  CORR.2

 )
)


(lts.ruleset
 ;; Assign stress to a vowel when non-exists
 italian_grafo10
;  Sets used in the rules
(
  (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 e1s E1s o1s O1s)
  (ALFVOKOCCL j w)
  (ALL a e i o u E O a1 e1 i1 o1 u1 E1 O1 j w p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y SS tts ttS ddz ddZ JJ ng: LL e1s E1s o1s O1s)
)
;  Rules
(
 ( ALL j [ e1 ] ALL = E1 )                                            ;; 24.60
 ( VOK [ ts ] VOK = ts ts )                                           ;; 24.65
 ( VOK [ dz ] VOK = dz dz )                                           ;; 24.67
 ( VOK [ S ] VOK = S  S )                                             ;; 24.70
 ( VOK [ L ] ALFVOKOCCL VOK = L L )                                   ;; 24.74
 ( VOK [ L ] VOK = L L )                                              ;; 24.75
 ( VOK [ J ] VOK = J J )                                              ;; 24.80
 ( VOK [ J ] ALFVOKOCCL VOK = J J )                                   ;; 24.80  CORR.2
 ( [ J1 ] = J J i )                                                   ;; 24.80  CORR.2


 ( [ i ] = i )
 ( [ e ] = e )
 ( [ E ] = E )
 ( [ a ] = a )
 ( [ o ] = o )
 ( [ O ] = O )
 ( [ u ] = u )
 ( [ i1 ] = i1 )
 ( [ e1 ] = e1 )
 ( [ E1 ] = E1 )
 ( [ a1 ] = a1 )
 ( [ o1 ] = o1 )
 ( [ O1 ] = O1 )
 ( [ u1 ] = u1 )
 ( [ e1s ] = e1 )
 ( [ E1s ] = E1 )
 ( [ o1s ] = o1 )
 ( [ O1s ] = O1 )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ;se c'� ancora qualche q la traslo in k
 ( [ q ] = k )
 
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 ( [ S ] = S )
 ( [ ts ] = ts )
 ( [ tS ] = tS )
 ( [ dz ] = dz )
 ( [ dZ ] = dZ )
 ( [ J ] = J )
 ( [ ng ] = ng )
 ( [ nf ] = nf )
 ( [ L ] = L )
 ( [ apo ] = );;; toglie l'apostrofo
 )
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;   RULESET per la sillabazione!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(lts.ruleset
   sillabe1
   (
    (KONS p t k b d g f s S v z ts tS dz dZ m n J ng l L r q c x h y SS tts ttS ddz ddZ JJ ng: LL)
    (VOK a e i o u E O a1 e1 i1 o1 u1 E1 O1 j w)
    (VOKK a e i o u E O a1 e1 i1 o1 u1 E1 O1)
    (VOK1 a1 e1 i1 o1 u1 E1 O1)
    (VOKAEOI1U1 a e o E O a1 e1 i1 o1 u1 E1 O1)
    (LR l r)
   )
   (
    ( [ p p ] = p - p )
    ( [ t t ] = t - t )
    ( [ k k ] =  k - k )
    ( [ b b ] =  b - b )
    ( [ d d ] =  d - d )
    ( [ g g ] =  g - g )
    ( [ f f ] =  f - f )
    ( [ s s ] =  s - s )
    ( [ S S ] =  S - S )
    ( [ v v ] =  v - v )
    ( [ ts ts ] = ts - ts )
    ( [ tS tS ] =  tS - tS )
    ( [ dz dz ] =  dz - dz )
    ( [ dZ dZ ] =  dZ - dZ )
    ( [ m m ] =  m - m )
    ( [ n n ] =  n - n )
    ( [ J J ] =  J - J )
    ( [ l l ] =  l - l )
    ( [ L L ] =  L - L )
    ( [ r r ] =  r - r )

    ( # [ s ] = s )
    ( [ s ] = - s )
    ( # [ z ] = z )
    ( [ z ] = - z )     ;;; serve per la esse sonora!!!

    ( [ l ] KONS = l - )
    ( [ m ] KONS = m - )
    ( [ n ] KONS = n - )
    ( [ r ] KONS = r - )
    ( [ ng ] KONS = ng - )    
    ( [ nf ] KONS = nf - )    

    ( VOK [ p ] VOK = - p )
    ( VOK [ t ] VOK = - t )
    ( VOK [ k ] VOK = - k )
    ( VOK [ b ] VOK = - b )
    ( VOK [ d ] VOK = - d )
    ( VOK [ g ] VOK = - g )
    ( VOK [ f ] VOK = - f )
    ( VOK [ s ] VOK = - s )
    ( VOK [ S ] VOK = - S )
    ( VOK [ v ] VOK = - v )
    ( VOK [ z ] VOK = - z )
    ( VOK [ ts ] VOK = - ts )
    ( VOK [ tS ] VOK = - tS )
    ( VOK [ dz ] VOK = - dz )
    ( VOK [ dZ ] VOK = - dZ )
    ( VOK [ m ] VOK = - m )
    ( VOK [ n ] VOK = - n )
    ( VOK [ J ] VOK = - J )
    ( VOK [ l ] VOK = - l )
    ( VOK [ L ] VOK = - L )
    ( VOK [ r ] VOK = - r )

    ( VOKK [ j ] = - j )   ;;;  casi particolari di semivocale preceduta da vocale ("ajwo1la")
    ( VOKK [ w ] = - w )   ;;;  N.B.: il gruppo VOKK � diverso da VOK (dentro non ci sono la j e la w)

    ( VOK [ p ] LR = - p )
    ( VOK [ t ] LR = - t )
    ( VOK [ k ] LR = - k )
    ( VOK [ b ] LR = - b )
    ( VOK [ d ] LR = - d )
    ( VOK [ g ] LR = - g )
    ( VOK [ f ] LR = - f )
    ( VOK [ v ] LR = - v )

    ( VOK [ t ] m = t - )  ;;; ho trovato "ritmo" ma non so se ci siano altri casi simili
    ( VOK [ k ] n = k - )  ;;; "tecnica"
    ( VOK [ g ] m = g - )  ;;; "paradigma"
    ( VOK [ b ] d = b - )  ;;; "subdolo"

    ( VOKAEOI1U1 [ e ] = - e )        ;;; questa serie serve per gli iati
    ( VOKAEOI1U1 [ e1 ] = - e1 )
    ( VOKAEOI1U1 [ E ] = - E )
    ( VOKAEOI1U1 [ E1 ] = - E1 )
    ( VOKAEOI1U1 [ a ] = - a )     
    ( VOKAEOI1U1 [ a1 ] = - a1 )
    ( VOKAEOI1U1 [ o ] = - o )
    ( VOKAEOI1U1 [ o1 ] = - o1 )
    ( VOKAEOI1U1 [ O ] = - O )
    ( VOKAEOI1U1 [ O1 ] = - O1 )
    ( VOKAEOI1U1 [ i1 ] = - i1 )
    ( VOKAEOI1U1 [ u1 ] = - u1 )

    ( [ i ] VOKK = i - )   ;;;  regole di divisione degli iati e dei dittonghi discendenti   
    ( [ i ] VOKK = i - )   ;;;  (li dividiamo questi ultimi anche se in teoria andrebbero uniti!)
    ( VOKK [ i ] = - i )   ;;;  Vengono gestiti anche i prefissi "bi" "tri" "ri"
    ( VOKK [ i ] = - i )
    ( [ u ] VOKK = u - )   
    ( [ u ] VOKK = u - )
    ( VOKK [ u ] = - u )   
    ( VOKK [ u ] = - u )

  ( [ b ] = b )
  ( [ c ] = c )
  ( [ d ] = d )
  ( [ f ] = f )
  ( [ g ] = g )
  ( [ h ] = h )
  ( [ j ] = j )
  ( [ k ] = k )
  ( [ l ] = l )
  ( [ m ] = m )
  ( [ n ] = n )
  ( [ p ] = p )
  ( [ q ] = q )
  ( [ r ] = r )
  ( [ s ] = s )
  ( [ t ] = t )
  ( [ v ] = v )
  ( [ w ] = w )
  ( [ x ] = x )
  ( [ y ] = y )
  ( [ z ] = z )
  ;; stressed vowels to themselves
  ( [ a1 ] = a1 )
  ( [ i1 ] = i1 )
  ( [ u1 ] = u1 )
  ( [ e1 ] = e1 )
  ( [ E1 ] = E1 )
  ( [ o1 ] = o1 )
  ( [ O1 ] = O1 )

  ( [ a ] = a )
  ( [ i ] = i )
  ( [ u ] = u )
  ( [ e ] = e )
  ( [ E ] = E )
  ( [ o ] = o )
  ( [ O ] = O )
  
  ( [ S ] = S )
  ( [ ts ] = ts )
  ( [ tS ] = tS )
  ( [ dz ] = dz )
  ( [ dZ ] = dZ )
  ( [ J ] = J )
  ( [ ng ] = ng )
  ( [ nf ] = nf )
  ( [ L ] = L )
 
  ( [ apo ] = apo )
  ( [ - ] = - )       
 )
)


(lts.ruleset
 sillabe2
 ((NIL nil)) 
 (  
  ( - [ - ] = )
  ( [ b ] = b )
  ( [ c ] = c )
  ( [ d ] = d )
  ( [ f ] = f )
  ( [ g ] = g )
  ( [ h ] = h )
  ( [ j ] = j )
  ( [ k ] = k )
  ( [ l ] = l )
  ( [ m ] = m )
  ( [ n ] = n )
  ( [ p ] = p )
  ( [ q ] = q )
  ( [ r ] = r )
  ( [ s ] = s )
  ( [ t ] = t )
  ( [ v ] = v )
  ( [ w ] = w )
  ( [ x ] = x )
  ( [ y ] = y )
  ( [ z ] = z )
  ;; stressed vowels to themselves
  ( [ a1 ] = a1 )
  ( [ i1 ] = i1 )
  ( [ u1 ] = u1 )
  ( [ e1 ] = e1 )
  ( [ E1 ] = E1 )
  ( [ o1 ] = o1 )
  ( [ O1 ] = O1 )

  ( [ a ] = a )
  ( [ i ] = i )
  ( [ u ] = u )
  ( [ e ] = e )
  ( [ E ] = E )
  ( [ o ] = o )
  ( [ O ] = O )
 
  ( [ S ] = S )
  ( [ ts ] = ts )
  ( [ tS ] = tS )
  ( [ dz ] = dz )
  ( [ dZ ] = dZ )
  ( [ J ] = J )
  ( [ ng ] = ng )
  ( [ nf ] = nf )
  ( [ L ] = L )
  
  ( [ apo ] = apo )
  ( [ - ] = - )       
 )
)


;;;
;;;  Function to turn word into lexical entry for Italian
;;;
;;;  First uses lts to get phoneme string then assigns stress if
;;;  there is no stress and then uses a third set of rules to
;;;  mark syllable boundaries, finally converting that list
;;;  to the bracket structure festival requires
;;;

(define (italian_lts word features)
  "(italian_lts WORD FEATURES)
Using various letter to sound rules build a Italian pronunciation of 
WORD."
  
  (let (phones syl stresssyl dword weakened)
    (if (lts.in.alphabet word 'italian)
	(set! dword (lts.apply word 'italian))
   	(set! dword (lts.apply "equis" 'italian)))
    (if (not (string-matches word ".*'")) ;;se non termina con '
     ;;(italian_is_a_content_word word italian_guess_pos)
      (let ()
        ;(print dword)
    	(set! stress1a (lts.apply dword 'italian_stress1a))     
    	;(print stress1a)
    	(set! stress1b (lts.apply stress1a 'italian_stress1b))
    	;(print stress1b)
    	(set! stress1c (lts.apply stress1b 'italian_stress1c))
    	;(print stress1c)
    	(set! stress4a (lts.apply stress1c 'italian_stress4a))
    	;(print stress4a)
    	(set! stress4b (lts.apply stress4a 'italian_stress4b))
    	;(print stress4b)
    	(set! stress4c (lts.apply stress4b 'italian_stress4c))
    	;(print stress4c)
    	(set! stress4d (lts.apply stress4c 'italian_stress4d))
    	;(print stress4d)
    	(set! stress5a (lts.apply stress4d 'italian_stress5a))
    	;(print stress5a)
    	(set! stress5b (lts.apply stress5a 'italian_stress5b))
    	;(print stress5b)
    	(set! stress6a (lts.apply stress5b 'italian_stress6a))
    	;(print stress6a)
    	(set! stress7a (lts.apply stress6a 'italian_stress7a))
    	;(print stress7a)
    	(set! stress7b (lts.apply stress7a 'italian_stress7b))
    	;(print stress7b)
    	(set! stress8a (lts.apply stress7b 'italian_stress8a))
    	;(print stress8a)
    	(set! stress8b (lts.apply stress8a 'italian_stress8b))
    	;(print stress8b)
    	(set! stress8c (lts.apply stress8b 'italian_stress8c))
    	;(print stress8c)
    	(set! stress9a (lts.apply stress8c 'italian_stress9a))
    	;(print stress9a)
    	(set! stress9b (lts.apply stress9a 'italian_stress9b))
    	;(print stress9b)
    	(set! stress10a (lts.apply stress9b 'italian_stress10a))
    	;(print stress10a)
    	(set! stress11a (lts.apply stress10a 'italian_stress11a))
    	;(print stress11a)
    	(set! stress12a (lts.apply stress11a 'italian_stress12a))
    	;(print stress12a)
    	(set! stress13a (lts.apply stress12a 'italian_stress13a))
    	;(print stress13a)
    	(set! stress14a (lts.apply stress13a 'italian_stress14a))
   	;(print stress14a)
    	(set! stress15a (lts.apply stress14a 'italian_stress15a))
    	;(print stress15a)
    	(set! stress15b (lts.apply stress15a 'italian_stress15b))
    	;(print stress15b)
    	(set! stress15c (lts.apply stress15b 'italian_stress15c))
    	;(print stress15c)
    	(set! stress16a (lts.apply stress15c 'italian_stress16a))
    	;(print stress16a)
    	(set! stress16b (lts.apply stress16a 'italian_stress16b))
    	;(print stress16b)
    	(set! stress17a (lts.apply stress16b 'italian_stress17a))
    	;(print stress17a)
    	(set! stress17b (lts.apply stress17a 'italian_stress17b))
    	;(print stress17b)
    	(set! stress17c (lts.apply stress17b 'italian_stress17c))
    	;(print "ultimastress")
    	;(print stress17c)
    	
    	)
    	
    	(set! stress17c dword))
   	
    (set! grafo1 (lts.apply stress17c 'italian_grafo1))
    ;(print grafo1)
    (set! grafo2 (lts.apply grafo1 'italian_grafo2))
    ;(print grafo2)
    (set! grafo3 (lts.apply grafo2 'italian_grafo3))
    ;(print grafo3)
    (set! grafo4 (lts.apply grafo3 'italian_grafo4))
    ;(print grafo4)
    (set! grafo5 (lts.apply grafo4 'italian_grafo5))
    ;(print grafo5)
    (set! grafo6 (lts.apply grafo5 'italian_grafo6))
    ;(print grafo6)
    (set! grafo7 (lts.apply grafo6 'italian_grafo7))
    ;(print grafo7)
    (set! grafo8 (lts.apply grafo7 'italian_grafo8))
    ;(print grafo8)
    (set! grafo9 (lts.apply grafo8 'italian_grafo9))
    ;(print grafo9)
    (set! stresssyl (lts.apply grafo9 'italian_grafo10))
    (set! syl1 (lts.apply stresssyl 'sillabe1))
    ;(print syl1)
    (set! syl2 (lts.apply syl1 'sillabe2))
    ;(print syl2)
    

    (list word
	  nil
	  (italian_tosyl_brackets syl2))))

(define (italian_is_a_content_word word poslist)
  "(italian_is_a_content_word WORD POSLIST)
Check explicit list of function words and return t if this is not
listed."
  (cond
   ((null poslist)
    t)
   ((member_string word (cdr (car poslist)))
    nil)
   (t
    (italian_is_a_content_word word (cdr poslist)))))

(define (italian_fw_gpos word poslist)
  "(italian_is_a_content_word WORD POSLIST)
Check explicit list of function words and return la CLASSE FUNZIONALE O NULLA SE NON C'�"
  (cond
   ((null poslist)
    "content")
   ((member_string word (cdr (car poslist)))
    (car (car poslist)) )
   (t
    (italian_fw_gpos word (cdr poslist)))))

(define (italian_downcase word)
  "(italian_downcase WORD)
Downs case word by letter to sound rules becuase or accented form
this can't use the builtin downcase function."
  (lts.apply word 'italian_downcase))


(define (italian_tosyl_brackets phones)
   "(italian_tosyl_brackets phones)
Takes a list of phones containing - as syllable boundary.  Construct the
Festival bracket structure."
 (let ((syl nil) (syls nil) (p phones) (stress 0))
    (while p
     (set! syl nil)
     (set! stress 0)
     (while (and p (not (eq? '- (car p))))
       (set! syl (cons (car p) syl))
       (if (string-matches (car p) ".*1")
           (set! stress 1))
       (set! p (cdr p)))
     (set! p (cdr p))  ;; skip the syllable separator
     (set! syls (cons (list (reverse syl) stress) syls)))
    (reverse syls)))   
    
(provide 'italian_lts)
