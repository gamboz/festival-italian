;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; LTS regole di trascrizione fonemica per ore date e numeri di telefono 
;;;

(lts.ruleset
   ora_data_tel_token
   (
    (ORD "^" "�") ;"o") ho tolto la o perch� altrimenti mi incasina in pol-2
    (PUNT12 "." ":")
    (DIG 0 1 2 3 4 5 6 7 8 9)
   )
    
   (
    ( # 0 [ 0 ] PUNT12 DIG DIG # = mezzanotte )
    ( # [ 1 2 ] PUNT12 DIG DIG # = mezzogiorno )
    ( # [ 0 ] PUNT12 DIG DIG # = mezzanotte )
    ( # 0 [ 1 ] PUNT12 DIG DIG # = una )
    ( # [ 1 ] PUNT12 DIG DIG # = una )
    
    ( # [ 0 ] DIG PUNT12 DIG DIG # = )
    ( # DIG DIG [ "." "0" "0" ] # = )
    ( # DIG [ "." "0" "0" ] # = )
    ( # DIG DIG [ ":" "0" "0" ] # = )
    ( # DIG [ ":" "0" "0" ] # = )
    ( # DIG [ "." ] DIG DIG # = e )
    ( # DIG DIG [ "." ] DIG DIG # = e )
    ( # DIG [ ":" ] DIG DIG # = e )
    ( # DIG DIG [ ":" ] DIG DIG # = e )
    ( # DIG PUNT12 [ 0 ] DIG # = )
    ( # DIG DIG PUNT12 [ 0 ] DIG # = )
    

   ( # [ 1 ] "/" DIG "/" = primo )   ;;; DATE (1�) 
   ( # [ 1 ] "/" DIG DIG "/" = primo )
   ( # [ 0 1 ] "/" DIG "/" = primo )
   ( # [ 0 1 ] "/" DIG DIG "/" = primo )
   
   ( # [ 1 ] "/" DIG = primo )   ;;; DATE (1�) solo giorno e mese  
   ( # [ 1 ] "/" DIG DIG = primo )
   ( # [ 0 1 ] "/" DIG = primo )
   ( # [ 0 1 ] "/" DIG DIG = primo )
   

    ( # [ 0 ] DIG "/" DIG "/" = )
    ( # [ 0 ] DIG "/" DIG DIG "/" = )
    ( # [ 0 ] DIG "-" DIG "-" = )
    ( # [ 0 ] DIG "-" DIG DIG "-" = )
    ( # DIG "/" [ 0 ] DIG "/" = )   
    ( # DIG DIG "/" [ 0 ] DIG "/" = )
    ( # DIG "-" [ 0 ] DIG "-" = )
    ( # DIG DIG "-" [ 0 ] DIG "-" = )

    ( # DIG [ "/" ] DIG "/" = " ")   
    ( # DIG DIG [ "/" ] DIG "/" = " ")
    ( # DIG [ "/" ] DIG DIG "/" = " ")
    ( # DIG DIG [ "/" ] DIG DIG "/" = " ")
    ( # DIG "/" DIG [ "/" ] = " ")
    ( # DIG DIG "/" DIG [ "/" ] = " ")
    ( # DIG "/" DIG DIG [ "/" ] = " ")
    ( # DIG DIG "/" DIG DIG [ "/" ] = " ")
    ( # DIG [ "-" ] DIG "-" = " ")
    ( # DIG DIG [ "-" ] DIG "-" = " ")
    ( # DIG [ "-" ] DIG DIG "-" = " ")
    ( # DIG DIG [ "-" ] DIG DIG "-" = " ")
    ( # DIG "-" DIG [ "-" ] = " ")
    ( # DIG DIG "-" DIG [ "-" ] = " ")
    ( # DIG "-" DIG DIG [ "-" ] = " ")
    ( # DIG DIG "-" DIG DIG [ "-" ] = " ")
    
    ( # [ 0 ] DIG "/" DIG = ) ;;; solo giorno e mese  
    ( # [ 0 ] DIG "/" DIG DIG = )
    ( # [ 0 ] DIG "-" DIG  = )
    ( # [ 0 ] DIG "-" DIG DIG = )
    ( # DIG "/" [ 0 ] DIG  = )   
    ( # DIG DIG "/" [ 0 ] DIG = )
    ( # DIG "-" [ 0 ] DIG  = )
    ( # DIG DIG "-" [ 0 ] DIG  = )

    ( # DIG [ "/" ] DIG  = " ")   
    ( # DIG DIG [ "/" ] DIG  = " ")
    ( # DIG [ "/" ] DIG DIG  = " ")
    ( # DIG DIG [ "/" ] DIG DIG  = " ")
    ( # DIG [ "-" ] DIG  = " ")
    ( # DIG DIG [ "-" ] DIG  = " ")
    ( # DIG [ "-" ] DIG DIG  = " ")
    ( # DIG DIG [ "-" ] DIG DIG  = " ")
        
    ( [ "'" ] DIG DIG # = millenovecento )  

    ;;TEMPERATURE
    ( # [ 1 "�" c ] # = un grado centigrado )
    ( # "-" [ 1 "�" c ] # = un grado centigrado )
    ( DIG + [ "�" c ] # = gradi centigradi )


 ;;NUMERI ORDINALI
   ( # DIG + [ ORD ] # = oRd)
  
            
    (  # [ "-" ] DIG +  = meno)	
    ( # 0 * [ 0 "," ] DIG # = zero virgola )      ;;; NUMERI CARDINALI (1�) (con la virgola per essere precisi)
    ( # 0 * [ 0 "," ] DIG DIG # = zero virgola )  ;;; si considera la virgola come separatore per la parte frazionaria
    ( # 0 * [ 0 "," ] DIG DIG DIG # = zero virgola )
    ( # 0 * [ 0 "," ] DIG + # = zero virgola & )
    ( DIG [ "," ] DIG # = virgola )
    ( DIG [ "," ] DIG DIG # = virgola )
    ( DIG [ "," ] DIG DIG DIG # = virgola )
    ( DIG [ "," ] DIG # = virgola & )
    ( DIG [ "." ] DIG # = punto )
    ( DIG [ "." ] DIG DIG # = punto )
    ( DIG [ "." ] DIG DIG DIG # = punto )
    ( DIG [ "." ] DIG # = punto & )
    
    ( DIG + [ "," ] DIG + = virgola )
    ( DIG + [ "." ] DIG + = punto )
    ( [ "\." ] = punto)   ;;;qui sarebbero da inserire altri simboli che possono essere all'interno di codici alfa numerici...
    
    

    ( # [ 0 ] DIG + = & 0 )               ;;; NUMERI DI TELEFONO (1�)
    ( # 0 DIG + [ "." ] DIG + = & )       ;;; se il prefisso � diviso dal numero viene trascritto con un solo accento;
    ( # 0 DIG + [ "/" ] DIG + = & )       ;;; altrimenti i numeri vengono pronunciati quattro di seguito e eventualmente
    ( # 0 DIG DIG DIG [ 0 ] = & 0 )       ;;; in un gruppo pi� piccolo gli ultimi
    ( # 0 DIG DIG DIG [ 1 ] = & 1 )       ;;; (tutto questo riguarda l'accento)
    ( # 0 DIG DIG DIG [ 2 ] = & 2 )
    ( # 0 DIG DIG DIG [ 3 ] = & 3 )
    ( # 0 DIG DIG DIG [ 4 ] = & 4 )
    ( # 0 DIG DIG DIG [ 5 ] = & 5 )
    ( # 0 DIG DIG DIG [ 6 ] = & 6 )
    ( # 0 DIG DIG DIG [ 7 ] = & 7 )
    ( # 0 DIG DIG DIG [ 8 ] = & 8 )
    ( # 0 DIG DIG DIG [ 9 ] = & 9 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 0 ] = & 0 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 1 ] = & 1 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 2 ] = & 2 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 3 ] = & 3 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 4 ] = & 4 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 5 ] = & 5 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 6 ] = & 6 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 7 ] = & 7 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 8 ] = & 8 )
    ( # 0 DIG DIG DIG DIG DIG DIG DIG [ 9 ] = & 9 )

    ( # [ 0 ] # = zero )   ;;; serve per gestire lo zero da solo (A. F. 2/10/2000)
   
    ( [ "." ] = "." )
    ( [ "," ] = "," )
    ( [ ":" ] = )
    ( [ - ] = )
    ( [ & ] = & )
    ( [ " " ] = " " )
    ( [ ORD ] = )
   
 ( [ 0 ] = 0 )
 ( [ 1 ] = 1 )
 ( [ 2 ] = 2 )
 ( [ 3 ] = 3 )
 ( [ 4 ] = 4 )
 ( [ 5 ] = 5 )
 ( [ 6 ] = 6 )
 ( [ 7 ] = 7 )
 ( [ 8 ] = 8 )
 ( [ 9 ] = 9 )
 
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
  
 ( [ a ] = a )
 ( [ e ] = e )
 ( [ i ] = i )
 ( [ o ] = o )
 ( [ u ] = u )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 
 ;( [ "'" ] =  ) 
  )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(lts.ruleset
   ora_data_tel2_token
   (
    (DIG 0 1 2 3 4 5 6 7 8 9) 
   )
 
   (
    ( [ " " 1 " " ] = gennaio )     ;;; DATE (2�)
    ( [ " " 2 " " ] = febbraio )   
    ( [ " " 3 " " ] = marzo )   
    ( [ " " 4 " " ] = aprile )   
    ( [ " " 5 " " ] = maggio )         
    ( [ " " 6 " " ] = giugno )            
    ( [ " " 7 " " ] = luglio )   
    ( [ " " 8 " " ] = agosto )   
    ( [ " " 9 " " ] = settembre )   
    ( [ " " 1 0 " " ] = ottobre )   
    ( [ " " 1 1 " " ] = novembre )   
    ( [ " " 1 2 " " ] = dicembre )   
    
    
    ( [ " " 1 0 ] = ottobre )   ;;; DATE (2�) solo giorno e mese
    ( [ " " 1 1 ] = novembre )   
    ( [ " " 1 2 ] = dicembre )   
    ( [ " " 1 ] = gennaio )     
    ( [ " " 2 ] = febbraio )   
    ( [ " " 3 ] = marzo )   
    ( [ " " 4 ] = aprile )   
    ( [ " " 5 ] = maggio )         
    ( [ " " 6 ] = giugno )            
    ( [ " " 7 ] = luglio )   
    ( [ " " 8 ] = agosto )   
    ( [ " " 9 ] = settembre )   
    
    
    ( [ " " ] DIG DIG " "  =  / )
    (  " "  DIG DIG [ " " ]  =  /)
    
       


    ( # DIG [ "." ] DIG DIG DIG = )        ;;; NUMERI CARDINALI (2�)
    ( # DIG DIG [ "." ] DIG DIG DIG = )    ;;; si � considerato il punto come separatore delle migliaia o milioni
    ( # DIG DIG DIG [ "." ] DIG DIG DIG = )
    ( DIG DIG DIG [ "." ] DIG DIG DIG = )

    ( & DIG DIG [ 0 ] DIG DIG = & 0 )      ;;; NUMERI DI TELEFONO (2�)
    ( & DIG DIG [ 1 ] DIG DIG = & 1 )      ;;; i gruppi da cinque e da sei cifre vengono divisi
    ( & DIG DIG [ 2 ] DIG DIG = & 2 )      ;;; in uno da quattro e in uno con le rimanenti 
    ( & DIG DIG [ 3 ] DIG DIG = & 3 )      ;;; (tutto questo riguarda l'accento)
    ( & DIG DIG [ 4 ] DIG DIG = & 4 )
    ( & DIG DIG [ 5 ] DIG DIG = & 5 )
    ( & DIG DIG [ 6 ] DIG DIG = & 6 )
    ( & DIG DIG [ 7 ] DIG DIG = & 7 )
    ( & DIG DIG [ 8 ] DIG DIG = & 8 )
    ( & DIG DIG [ 9 ] DIG DIG = & 9 )
    ( & DIG DIG DIG DIG [ 0 ] DIG = & 0 )
    ( & DIG DIG DIG DIG [ 1 ] DIG = & 1 )
    ( & DIG DIG DIG DIG [ 2 ] DIG = & 2 )
    ( & DIG DIG DIG DIG [ 3 ] DIG = & 3 )
    ( & DIG DIG DIG DIG [ 4 ] DIG = & 4 )
    ( & DIG DIG DIG DIG [ 5 ] DIG = & 5 )
    ( & DIG DIG DIG DIG [ 6 ] DIG = & 6 )
    ( & DIG DIG DIG DIG [ 7 ] DIG = & 7 )
    ( & DIG DIG DIG DIG [ 8 ] DIG = & 8 )
    ( & DIG DIG DIG DIG [ 9 ] DIG = & 9 )
 
    
    ( [ ORD ] =  )
    ( [ "," ] = "," )
    ( [ ":" ] = )
    ( [ & ] = & )
    ( [ " " ] = " " )
    
    ( [ mezzogiorno ] = mezzogiorno )
    ( [ mezzanotte ] = mezzanotte )
    ( [ una ] = una )
    ( [ millenovecento ] = millenovecento )
    ( [ primo ] = primo )
    ( [ zero ] = zero )
    ( [ virgola ] = virgola )
    ( [ punto ] = punto )
    ( [ meno ] = meno )
    ( [ un ] = un )
    ( [ grado ] = grado )
    ( [ gradi ] = gradi )
    ( [ centigrado ] = centigrado )
    ( [ centigradi ] = centigradi )
    ( [ oRd ] = oRd)
    
 ( [ 0 ] = 0 )
 ( [ 1 ] = 1 )
 ( [ 2 ] = 2 )
 ( [ 3 ] = 3 )
 ( [ 4 ] = 4 )
 ( [ 5 ] = 5 )
 ( [ 6 ] = 6 )
 ( [ 7 ] = 7 )
 ( [ 8 ] = 8 )
 ( [ 9 ] = 9 )
 
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
  
 ( [ a ] = a )
 ( [ e ] = e )
 ( [ i ] = i )
 ( [ o ] = o )
 ( [ u ] = u )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
        
   )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(lts.ruleset
   ora_data_tel3_token
   (
    (DIG 0 1 2 3 4 5 6 7 8 9) 
   )
   
   (
    ( & DIG * [ 0 ] # = zero )            ;;; NUMERI DI TELEFONO (3�)
    ( & DIG * [ 1 ] # = uno )                 ;;; trascrizione con gli accenti
    ( & DIG * [ 2 ] # = due )
    ( & DIG * [ 3 ] # = tre )
    ( & DIG * [ 4 ] # = quattro )
    ( & DIG * [ 5 ] # = cinque )
    ( & DIG * [ 6 ] # = sei )
    ( & DIG * [ 7 ] # = sette )
    ( & DIG * [ 8 ] # = otto )
    ( & DIG * [ 9 ] # = nove )
    ( & DIG * [ 0 ] & = zero )           
    ( & DIG * [ 1 ] & = uno )
    ( & DIG * [ 2 ] & = due )
    ( & DIG * [ 3 ] & = tre )
    ( & DIG * [ 4 ] & = quattro )
    ( & DIG * [ 5 ] & = cinque )
    ( & DIG * [ 6 ] & = sei )
    ( & DIG * [ 7 ] & = sette )
    ( & DIG * [ 8 ] & = otto )
    ( & DIG * [ 9 ] & = nove )
    ( & DIG * [ 0 ] DIG + = zero )        ;;; trascrizione senza gli accenti
    ( & DIG * [ 1 ] DIG + = uno )
    ( & DIG * [ 2 ] DIG + = due )
    ( & DIG * [ 3 ] DIG + = tre )
    ( & DIG * [ 4 ] DIG + = quattro )
    ( & DIG * [ 5 ] DIG + = cinque  )
    ( & DIG * [ 6 ] DIG + = sei )
    ( & DIG * [ 7 ] DIG + = sette )
    ( & DIG * [ 8 ] DIG + = otto )
    ( & DIG * [ 9 ] DIG + = nove )
    
    ( [ "," ] = "," )
    ( [ ":" ] = )
    ( [ & ] = )
    ( [ " " ] = " " )   
    ( [ / ] = / )   
    
   
    ( [ mezzogiorno ] = mezzogiorno )
    ( [ mezzanotte ] = mezzanotte )
    ( [ una ] = una )
    ( [ millenovecento ] = millenovecento )
    ( [ primo ] = primo )
    ( [ zero ] = zero )
    ( [ virgola ] = virgola )
    ( [ punto ] = punto )
    ( [ un ] = un )
    ( [ meno ] = meno )
    ( [ grado ] = grado )
    ( [ gradi ] = gradi )
    ( [ centigrado ] = centigrado )
    ( [ centigradi ] = centigradi )
    
    ( [ gennaio ] = gennaio)
    ( [ febbraio ] = febbraio )
    ( [ marzo ] = marzo )
    ( [ aprile ] = aprile )
    ( [ maggio ] = maggio )
    ( [ giugno ] = giugno )
    ( [ luglio ] = luglio )
    ( [ agosto ] = agosto )
    ( [ settembre ] = settembre )
    ( [ ottobre ] = ottobre )
    ( [ novembre ] = novembre )
    ( [ dicembre ] = dicembre )
    ( [ oRd ] = oRd)
    
 ( [ 0 ] = 0 )
 ( [ 1 ] = 1 )
 ( [ 2 ] = 2 )
 ( [ 3 ] = 3 )
 ( [ 4 ] = 4 )
 ( [ 5 ] = 5 )
 ( [ 6 ] = 6 )
 ( [ 7 ] = 7 )
 ( [ 8 ] = 8 )
 ( [ 9 ] = 9 )
 
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
 ( [ � ] = � )
  
 ( [ a ] = a )
 ( [ e ] = e )
 ( [ i ] = i )
 ( [ o ] = o )
 ( [ u ] = u )
 ( [ b ] = b )
 ( [ c ] = c )
 ( [ d ] = d )
 ( [ f ] = f )
 ( [ g ] = g )
 ( [ h ] = h )
 ( [ j ] = j )
 ( [ k ] = k )
 ( [ l ] = l )
 ( [ m ] = m )
 ( [ n ] = n )
 ( [ p ] = p )
 ( [ q ] = q )
 ( [ r ] = r )
 ( [ s ] = s )
 ( [ t ] = t )
 ( [ v ] = v )
 ( [ w ] = w )
 ( [ x ] = x )
 ( [ y ] = y )
 ( [ z ] = z )
 
 
 )
)


(provide 'italian_oredatetel)