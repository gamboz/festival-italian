;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; PHONESET PER L'ITALIANO 
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sono stati inseriti i fonemi della lingua italiana usando la notazione SAMPA. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defPhoneSet
italian
;;; Caratteristiche dei foni
(;; vowel or consonant
   (vc + -)  
   ;; vowel length: short long diphthong schwa
   (vlng s l d a 0)
   ;; vowel height: high mid low
   (vheight 1 2 3 -)
   ;; vowel frontness: front mid back
   (vfront 1 2 3 -)
   ;; lip rounding
   (vrnd + -)
   ;; consonant type: stop fricative affricative nasal liquid
   (ctype s f a n l 0)
   ;; place of articulation: labial alveolar palatal labio-dental dental velar
   (cplace l a p b d v 0)
   ;; consonant voicing
   (cvox + -)
   ;; accentazione
   (acc + - 0)
   ;; geminate: nel senso che il fonema pu� o meno geminarsi
   (gem + - 0)
   ;;semivocali/semiconsonanti
   (semi + -)
   )

;;; Insieme dei foni (41 fonemi)

(;;  VOCALI
 (i + s 1 1 - 0 0 - - 0 - )
 (e + s 2 1 - 0 0 - - 0 - ) ;;  e chiusa
 (E + s 2 1 - 0 0 - - 0 - ) ;;  e aperta
 (a + s 3 2 - 0 0 - - 0 - )
 (o + s 2 3 + 0 0 - - 0 - ) ;;  o chiusa
 (O + s 2 3 + 0 0 - - 0 - ) ;;  o aperta 
 (u + s 1 3 + 0 0 - - 0 - )
(i1 + s 1 1 - 0 0 - + 0 - )
(e1 + l 2 1 - 0 0 - + 0 - ) ;;  e chiusa
(E1 + l 2 1 - 0 0 - + 0 - ) ;;  e aperta
(a1 + l 3 2 - 0 0 - + 0 - )
(o1 + l 2 3 + 0 0 - + 0 - ) ;;  o chiusa
(O1 + l 2 3 + 0 0 - + 0 - ) ;;  o aperta 
(u1 + l 1 3 + 0 0 - + 0 - )

;;  SEMI-VOCALI (SEMI-CONSONANTI)
 (j - s 1 1 - 0 0 + - 0 + )
 (w - s 1 3 + 0 0 + - 0 + )

;;  OCCLUSIVE (O PLOSIVE)
 (p - 0 - - - s l - 0 + - )
 (t - 0 - - - s a - 0 + - )
 (k - 0 - - - s v - 0 + - )
 (b - 0 - - - s l + 0 + - )
 (d - 0 - - - s a + 0 + - )
 (g - 0 - - - s v + 0 + - )

;;  FRICATIVE
 (f - 0 - - - f b - 0 + - )
 (s - 0 - - - f a - 0 + - )
 (S - 0 - - - f p - 0 + - )
 (v - 0 - - - f b + 0 + - )
 (z - 0 - - - f a + 0 - - )
 (Z - 0 - - - f p + 0 - - )

;;  AFFRICATE
(ts - 0 - - - a a - 0 + - )
(tS - 0 - - - a p - 0 + - )
(dz - 0 - - - a a + 0 + - )
(dZ - 0 - - - a p + 0 + - )

;;  NASALI
 (m - 0 - - - n l + 0 + - )
 (n - 0 - - - n a + 0 + - )
 (J - 0 - - - n p + 0 + - )
(ng - 0 - - - n v + 0 - - )
(nf - 0 - - - n b + 0 - - )

;;  LIQUIDE (O APPROSSIMANTI)
 (l - 0 - - - l a + 0 + - )
 (L - 0 - - - l p + 0 + - )
 (r - 0 - - - l a + 0 + - )

;;SILENZIO
 (# - 0 - - - 0 0 - 0 0 - )

;;  LE GEMINATE hanno le stesse features delle loro controparti singole. (noi non le usiamo)

)
)

(PhoneSet.silences '(#))

(provide 'italian_phoneset)