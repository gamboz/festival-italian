;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Parole funzione
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Part of speech down by crude lookup using gpos 
;;;

(set! italian_guess_pos 
'((art_def ;ARTICLES Definite
	il lo la i gli le "l'")
(art_indef ;ARTICLES Indefinite
	un uno una "un'")
(pre_pro ;PREPOSITIONS Proper	
	di a da in con su per tra fra)
(pre_con ;PREPOSITIONS Contracted	
	al   allo   alla   ai   agli   alle   "all'"
	dal  dallo  dalla  dai  dagli  dalle  "dall'"
	del  dello  della  dei  degli  delle  "dell'"
	nel  nello  nella  nei  negli  nelle  "nell'"
	sul  sullo  sulla  sui  sugli  sulle  "sull'")
(pre_imp ;PREPOSITIONS Improper
	contro presso sopra sotto dietro dentro dopo
	lungo salvo secondo
	durante mediante stante nonostante
	dato eccetto tolto)
(pre_pp ;PREPOSITIONS Prepositional phrase
	prima di 	davanti a 	fino a	per mezzo di	di la  "d�" 
	di qua da incontro di in "qualit�" di senza di
	riguardo a	 a fine di 	in mezzo a 	rispetto a 
	insieme con 	accanto a 	in quanto a	 invece di 
	in luogo di)
(dadj_pos ;DEFINITE ADJECTIVES Possessive
	mio mia mie miei tuo tua tue tuoi
	suo sua sue suoi nostro nostra nostre nostri
	vostro vostra vostre vostri loro altrui
	proprio propria proprie propri)
(dadj_dem ;DEFINITE ADJECTIVES Demonstrative
	questo questa queste questi quello quel quella quelle
	quelli stesso stessa stesse stessi altro altra altre
	altri medesimo medesima medesime medesimi tale cotale
	codesto codesta codeste codesti siffatto)
(dadj_int ;DEFINITE ADJECTIVES Interrogative
	che quale qual quanto quanta quante quanti)	
(dadj_ind ;DEFINITE ADJECTIVES Indefinite
	alcun alcuno alcuna alcune alcuni altro altra altre
	altri alquanto alquanta alquante alquanti altrettanto
	altrettanta altrettante altrettanti certo certa certe 
	certi diverso diversa diverse diversi molto molta 
	molte molti parecchio parecchia parecchie parecchi
	poco poca poche pochi quanto quanta quante quanti
	taluno taluna talune taluni tanto tanta tante tanti
	troppo troppa troppe troppi tutto tutta tutte tutti
	ciascun ciascuno ciascuna nessuno nessun nessuna
	niuno niuna tale ogni qualche qualunque qualsiasi
	qualsivoglia)
(dadj_num ;DEFINITE ADJECTIVES Numeral
	uno un una due undici venti cento
	primo prima prime primi secondo seconda sedonde secondi
	decimo decima centesimo centesima
	doppio doppia doppie doppi duplice centuplo centuplice
	ambo ambedue entrambi)
(prono_per ;PRONOUNS Personal
	io me mi tu te ti noi ce ci voi ve vi egli ella esso
	essa esse essi loro lui gli se si ne lei la le li "s�")
(prono_dem ;PRONOUNS Demonstrative
	lo la le li ne ci "ci�" coloro colui colei tale costui
	costei costoro questo questa queste questi codesto 
	codesta codeste codesti quello quelle quella quelli
	stesso stessa stesse stessi medesimo medesima medesime
	medesimi)
(prono_int ;PRONOUNS Interrogative
	chi che quale quanto)
(prono_rel ;PRONOUNS Relative
	che 	il quale	 cui)
(prono_mix ;PRONOUNS Mixted
	chi quanto chiunque)
(prono_ind ;PRONOUNS Indefinite
	uno un una qualcuno qualcuna qualcosa ognuno 
	ognuna niente nulla altri certuni chicchessia
	checchessia alcuno alcuna alcune alcuni ciascuno
	ciascuna taluno taluni altro altra altre altri
	certo certa certe certi nessuno nessun nessuna
	veruno poco molto parecchio troppo tutto tanto
	quanto alquanto altrettanto)
(conjcoo_cop ;CONJUNCTIONS COORDINATIVE Copulative
	e ed anche "n�" neppure neanche nemmeno)
(conjcoo_dis ;CONJUNCTIONS COORDINATIVE Disjunctive
	o ossia ovvero oppure)
(conjcoo_adv ;CONJUNCTIONS COORDINATIVE Adversative
	ma "per�" invece tuttavia cionondimeno pure peraltro
	con tutto "ci�")
(conjcoo_dec ;CONJUNCTIONS COORDINATIVE Declarative
	"cio�" infatti ossia invero	 vale a dire)
(conjcoo_con ;CONJUNCTIONS COORDINATIVE Conclusive
	dunque "perci�" pertanto quindi "sicch�"	 per la qual cosa)
(conjcoo_adj ;CONJUNCTIONS COORDINATIVE Adjective
	inoltre pure ancora	 oltre a "ci�")
(conjcoo_corr ;CONJUNCTIONS COORDINATIVE	Correlative
	e 	"n�" 	o 	come "cos�"	 tanto quanto
	non solo ma anche	 tanto "pi�" quanto "pi�")
(sub_dec ;SUBORDINATIVE Declarative
	che come)
(sub_temp ;SUBORDINATIVE Temporal
	quando come appena "allorch�" "finch�" allorquando
	nel tempo che	 tosto che	 prima che	 dopo che	 che	
	ognivolta che)
(sub_caus ;SUBORDINATIVE Causal
	"poich�" "perch�" "giacch�" siccome 	 dato che)
(sub_fin ;SUBORDINATIVE Final		
	"affinch�" "perch�" "accioch�")
(sub_cond ;SUBORDINATIVE Conditional
	se "purch�" qualora	 posto che	  in caso che)
(sub_conc ;SUBORDINATIVE Concessive
	"bench�" quantunque sebbene	 anche se	 per quanto
	nonostante che)
(sub_cons ;SUBORDINATIVE Consecutive
	"sicch�"	 tale che)
(sub_comp ;SUBORDINATIVE Comparative
	cos� come	 tanto come 	 "pi�" che 	 meglio che
	piuttosto che)
(sub_mod ;SUBORDINATIVE Modal
	come siccome quasi comunque	 a quel modo che	 secondo che
	come se	 quasi che)
(sub_exc ;SUBORDINATIVE Exceptive
	"fuorch�"	 eccetto che	 se non che	 salvo che	 senza che)
(adv_quan ;ADVERBS Quantity
	molto poco troppo parecchio alquanto piu meno almeno
	oltre misura	 "all'"incirca	 "press'"a poco pressappoco
	quasi circa punto assai abbastanza affatto "altres�"
	oltremodo)
(adv_pla ;ADVERBS Place
	qui qua "quass�" "quaggi�"	 di qui	 "cost�" "cost�" "l�" "l�"
	"laggi�" ivi	 di "l�"	 per di "l�"	 altrove fuori dappertutto
	dentro	 a destra	 a sinistra	 vicino lontano su "gi�" oltre
	ove dove donde dovunque)
(adv_time ;ADVERBS Time
	ieri oggi domani dopodomani dopo poi precedentemente
	prima sempre ora tuttora ancora finora adesso "gi�"
	immediatamente 	subito talvolta 	raramente spesso mai sovente
	tardi presto quotidianamente giornalmente settimanalmente
 	mensilmente annualmente)
(adv_aff ;ADVERBS Afferm
	si certo sicuro appunto certamente)
(adv_neg ;ADVERBS Nega
	no non giammai niente affatto nemmeno neppure)
(adv_pro ;ADVERBS Probalistici
	forse probabilmente possibilmente eventualmente)
(adv_agg ;ADVERBS Aggiunti
	anche pure inoltre perfino)	
;;VERBI AUSILIARI
;;ESSERE
(v_es_in_p ;VERBO ESSERE INDICATIVO PRESENTE
	sono sei � siamo siete) 
(v_es_in_pp ;VERBO ESSERE INDICATIVO PASSATO PROSSIMO
	stato stati)
(v_es_in_pr ;VERBO ESSERE INDICATIVO PASSATO REMOTO
	fui fosti fu fummo foste furono)
(v_es_in_i ;VERBO ESSERE INDICATIVO IMPERFETTO
	ero eri era eravamo eravate erano)
(v_es_in_f ;VERBO ESSERE INDICATIVO FUTURO SEMPLICE
	sar� sarai sar� saremo sarete saranno)
(v_es_co_p ;VERBO ESSERE CONGIUNTIVO PRESENTE
	sia siamo siate siano)
(v_es_co_i ;VERBO ESSERE CONGIUNTIVO IMPERFETTO
	fossi fosse fossimo foste fossero)
(v_es_cd_p ;VERBO ESSERE CONDIZIONALE PRESENTE
	sarei saresti sarebbe saremmo sareste sarebbero)
(v_es_im_p ;VERBO ESSERE IMPERATIVO PRESENTE
	sii sia siamo siate siano)
(v_es_ge_p ;VERBO ESSERE GERUNDIO PRESENTE
	essendo)
;;AVERE
(v_av_in_p ;VERBO AVERE INDICATIVO PRESENTE
	ho hai ha abbiamo avete hanno)
(v_av_in_pp ;VERBO AVERE INDICATIVO PASSATO PROSSIMO
	avuto)
(v_av_in_pr ;VERBO AVERE INDICATIVO PASSATO REMOTO
	ebbi avesti ebbe avemmo aveste ebbero)
(v_av_in_i ;VERBO AVERE INDICATIVO IMPERFETTO
	avevo avevi aveva avevamo avevate avevano)
(v_av_in_f ;VERBO AVERE INDICATIVO FUTURO SEMPLICE
 	avr� avrai avr� avremo avrete avranno)
(v_av_co_p ;VERBO AVERE CONGIUNTIVO PRESENTE
	abbia abbiamo abbiate abbiano)
(v_av_co_i ;VERBO AVERE CONGIUNTIVO IMPERFETTO
	avessi avesse avessimo aveste avessero)
(v_av_cd_p ;VERBO AVERE CONDIZIONALE PRESENTE
	avrei avresti avrebbe avremmo avreste avrebbero)
(v_av_im_p ;VERBO AVERE IMPERATIVO PRESENTE
	abbi abbia abbiamo abbiate abbiano)
(v_av_ge_p ;VERBO AVERE GERUNDIO PRESENTE
	avendo)

;;DOVERE POTERE VOLERE
(v_dpv_in_p ;VERBO DOVERE POTERE VOLERE INDICATIVO PRESENTE
	devo debbo posso voglio devi puoi vuoi
	deve pu� vuole dobbiamo possiamo vogliamo
	dovete potete volete devono debbono possono vogliono)
(v_dpv_in_pp ;VERBO DOVERE POTERE VOLERE INDICATIVO PASSATO PROSSIMO
	dovuto dovuta dovuti dovute potuto potuta potuti potute voluto voluta voluti volute)
(v_dpv_in_pr ;VERBO DOVERE POTERE VOLERE INDICATIVO PASSATO REMOTO
	dovei dovetti potei volli dovesti potesti volesti dov� dovette pot� volle 
	dovemmo potemmo volemmo doveste poteste voleste doverono dovettero potettero poterono vollero)
(v_dpv_in_im ;VERBO DOVERE POTERE VOLERE INDICATIVO IMPERFETTO
	dovevo potevo volevo dovevi potevi volevi
	doveva poteva voleva dovevamo potevamo volevamo
	dovevate potevate volevate dovevano potevano volevano)
(v_dpv_in_f ;VERBO DOVERE POTERE VOLERE INDICATIVO FUTURO SEMPLICE
	dovr� potr� vorr� dovrai potrai vorrai dovr� potr� vorr� 
	dovremo potremo vorremo dovrete potrete vorrete dovranno potranno vorranno)
(v_dpv_co_p ;VERBO DOVERE POTERE VOLERE CONGIUNTIVO PRESENTE
	debba deva possa voglia dobbiamo possiamo vogliamo dobbiate possiate vogliate debbano possano vogliano)
(v_dpv_co_i ;VERBO DOVERE POTERE VOLERE CONGIUNTIVO IMPERFETTO
	dovessi	potessi volessi dovesse potesse volesse dovessimo potessimo volessimo doveste poteste voleste dovessero	potessero volessero)
(v_dpv_cd_p ;VERBO DOVERE POTERE VOLERE CONDIZIONALE PRESENTE
	dovrei potrei vorrei dovresti potresti vorresti dovrebbe potrebbe vorrebbe 
	dovremmo potremmo voremmo dovreste potreste vorreste dovrebbero potrebbero vorrebbero)
(v_dpv_im_p ;VERBO DOVERE POTERE VOLERE IMPERATIVO PRESENTE
	voglia vogli vogliate)
(v_dpv_ge_p ;VERBO DOVERE POTERE VOLERE GERUNDIO PRESENTE
	dovendo	potendo volendo)
)	
)




(provide 'italian_gpos)