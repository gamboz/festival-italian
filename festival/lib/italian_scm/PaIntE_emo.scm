(define (PaIntE_norm painte top base)
"(define PaIntE_norm painte top base)"
;(nth 0 painte) ;il primo parametro 
(list (nth 0 painte) (nth 1 painte) (nth 2 painte) (/ (nth 3 painte) (- top base) ) (/ (nth 4 painte) (- top base)) (/ (- (nth 5 painte) base) (- top base)) )
)

(define (PaIntE_denorm painte top base)
"(define PaIntE_norm painte top base)"
;(nth 0 painte) ;il primo parametro 
(list (nth 0 painte) (nth 1 painte) (nth 2 painte) (* (nth 3 painte) (- top base) ) (* (nth 4 painte) (- top base)) (+ (* (nth 5 painte) (- top base)) base))
)

(define (add_painte_vector v1 v2)
  "(add_vector v1 v2)"
  (list  
   (+ (nth 0 v1) (nth 0 v2)) 
   (+ (nth 1 v1) (nth 1 v2)) 
   (+ (nth 2 v1) (nth 2 v2)) 
   (+ (nth 3 v1) (nth 3 v2)) 
   (+ (nth 4 v1) (nth 4 v2)) 
   (+ (nth 5 v1) (nth 5 v2))))

(define (sub_painte_vector v1 v2)
  "(sub_vector v1 v2)"
  (list  
   (- (nth 0 v1) (nth 0 v2)) 
   (- (nth 1 v1) (nth 1 v2)) 
   (- (nth 2 v1) (nth 2 v2)) 
   (- (nth 3 v1) (nth 3 v2)) 
   (- (nth 4 v1) (nth 4 v2)) 
   (- (nth 5 v1) (nth 5 v2))))

(define (s_molt_painte_vector s v)
  "(add_vector v1 v2)"
  (list  
   (* (nth 0 v) s)
   (* (nth 1 v) s)
   (* (nth 2 v) s)
   (* (nth 3 v) s)
   (* (nth 4 v) s)
   (* (nth 5 v) s)))

(define (coerenza_painte v)
(if  (< (nth 5 v) 0)
     (list  
      (nth 0 v) 
      (nth 1 v) 
      (nth 2 v)
      (- (nth 3 v) (nth 5 v))
      (- (nth 4 v) (nth 5 v))
      (- (nth 5 v) (nth 5 v)))
     v))
     
     
(define (PaIntE_Dnorm_CW_vq_syl utt)
"(PaIntE_Dnorm_CW_vq_syl utt)"

(let ((s (utt.relation.first utt 'Syllable)))
  (while s 
	 (if (not (string-equal (item.feat s 'R:SylStructure.lisp_Accent_boundary_syl) "s"))
	     (let ( 
		   (painte_neu (wagon_predict s PaIntE_CW_vq_tree)) 
		   (painte_EDnorm (wagon_predict s EDnorm_PaIntE_CW_vq_tree))
		   )

	       (if (defvar debug_mode) 
		   (if debug_mode 
		       (let ()
			 (format t "painte_EDnorm\n")
			 (print painte_EDnorm))))

	       (set! painte_Enorm (coerenza_painte (sub_painte_vector 
				(PaIntE_norm painte_neu top_neu base_neu) 
				painte_EDnorm)))

	       (set! painte_E (PaIntE_denorm painte_Enorm top_emo base_emo))
	       
	       (set! painte (add_painte_vector 
			     (s_molt_painte_vector Efactor painte_E) 
			     (s_molt_painte_vector (- 1 Efactor) painte_neu)))

	        (if (defvar debug_mode) 
		   (if debug_mode 
		       (let ()
			 (format t "neu_norm\n")
			 (print (PaIntE_norm painte_neu top_neu base_neu))
			 (format t "E_norm\n")
			 (print  painte_Enorm)
			 (format t "painte\n")
			 (print painte)
			 (format t "-*------\n"))))

		(set_painte utt s painte))) 
	 (set! s (item.next s)))))



