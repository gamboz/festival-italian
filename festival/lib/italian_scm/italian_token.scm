;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Tokenization rules for Italian
;;;
;;;  Particularly numbers and symbols.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    
(define (italian_number name)
"(italian_number name)
Convert a string of digits into a list of words saying the number."
  (if (string-matches name "0")
      (list "zero")
      (italian_number_from_digits (symbolexplode name))))

(define (just_zeros digits)
"(just_zeros digits)
If this only contains 0s then we just do something different."
 (cond
  ((not digits) t)
  ((string-equal "0" (car digits))
   (just_zeros (cdr digits)))
  (t nil)))
  
(define (last_ordinal digits)
  "(last_ordinal digits)
  Trasforma una lista di numeri fine-ordinali (ultima cifra) nellla corrispettiva trascrizione"
     (let ((l (length digits)))
    (cond
     ((equal? l 0)
      nil)
     ((string-equal (car digits) "0")
      (last_ordinal (cdr digits)))
     ((equal? l 1);; single digit
      (cond 
       ((string-equal (car digits) "0") (list "zeresimo -T ((dz E1) 1) ((r o) 0)" ))
       ((string-equal (car digits) "1") (list "unesimo -T ((u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
       ((string-equal (car digits) "2") (list "duesimo -T ((d u) 0) ((E1) 1) ((z i) 0) ((m o) 0)" ))
       ((string-equal (car digits) "3") (list "treesimo -T ((t r e) 0) ((E1) 1) ((z i) 0) ((m o) 0)"))
       ((string-equal (car digits) "4") (list "quattresimo -T ((k w a t) 0) ((t r E1) 1) ((z i) 0) ((m o) 0)"))
       ((string-equal (car digits) "5") (list "cinquesimo -T ((tS i ng) 0) ((k w E1) 1) ((z i) 0) ((m o) 0)"))
       ((string-equal (car digits) "6") (list "seiesimo -T ((s e) 0) ((j E1) 1) ((z i) 0) ((m o) 0)"))
       ((string-equal (car digits) "7") (list "settesimo -T ((s e t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
       ((string-equal (car digits) "8") (list "ottesimo -T ((o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
       ((string-equal (car digits) "9") (list "novesimo -T ((n o) 0) ((v E1) 1) ((z i) 0) ((m o) 0)"))
       ;; fill in the rest
       (t (list "equis")))))))
      
(define (italian_number_from_digits_token digits)
  "(italian_number_from_digits_token digits)
Trasforma una lista di numeri nella trascrizione cardinale o ordinale."
(if (equal? (length digits) 0)
     nil)
(if (equal? (last digits) '(oRd))
 (let ()
  (if (and (equal? (length digits) 2) (string-equal (car digits) "0")) 
   (list "zeresimo -T ((dz e) 0) ((r E1) 1) ((z i) 0) ((m o) 0)")
   (italian_num_token_ordinal (reverse (cdr (reverse digits))))))
 (if (and (equal? (length digits) 1) (string-equal (car digits) "0"))
   (list "zero -T ((dz E1) 1) ((r o) 0)")
   (italian_num_token_normal digits))))

(define (italian_num_token_ordinal digits)
"(italian_num_token_ordinal digits)
Trasforma una lista di numeri nella trascrizione ordinale"
  (let ((l (length digits))) ;numeri ordinali
    (cond
     ((equal? l 0)
      nil)
     ((string-equal (car digits) "0")
      (italian_num_token_ordinal (cdr digits)))
     ((equal? l 1);; single digit
      (cond 
       ((string-equal (car digits) "0") (list "zeresimo -T ((dz e) 0) ((r E1) 1) ((z i) 0) ((m o) 0)" ))
       ((string-equal (car digits) "1") (list "primo -T ((p r i1) 1) ((m o) 0)"))
       ((string-equal (car digits) "2") (list "secondo -T ((s e) 0) ((k o1 n) 1) ((d o) 0)" ))
       ((string-equal (car digits) "3") (list "terzo -T ((t E1 r) 1) ((ts o) 0)"))
       ((string-equal (car digits) "4") (list "quarto -T ((k w a1 r) 1) ((t o) 0)"))
       ((string-equal (car digits) "5") (list "quinto -T ((k w i1 n) 1) ((t o) 0)"))
       ((string-equal (car digits) "6") (list "sesto -T ((s E1) 1) ((s t o) 0)"))
       ((string-equal (car digits) "7") (list "settimo -T ((s E1 t) 1) ((t i) 0) ((m o) 0)"))
       ((string-equal (car digits) "8") (list "ottavo -T ((o t) 0) ((t a1) 1) ((v o) 0)"))
       ((string-equal (car digits) "9") (list "nono -T ((n O1) 1) ((n o) 0)"))
       ;; fill in the rest
       (t (list "equis"))));; $$$ what should say?
     ((equal? l 2);; less than 100
      (cond
       ((string-equal (car digits) "0");; 0x
	(last_ordinal (cdr digits)))
	
       ((string-equal (car digits) "1");; 1x
	(cond
	 ((string-equal (car (cdr digits)) "0") (list "decimo -T ((d E1) 1) ((tS i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "1") (list "undicesimo -T ((u n) 0) ((d i) 0) ((tS E1) 1) ((z i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "2") (list "dodicesimo -T ((d o) 0) ((d i) 0) ((tS E1) 1) ((z i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "3") (list "tredicesimo -T ((t r e) 0) ((d i) 0) ((tS E1) 1) ((z i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "4") (list "quattordicesimo -T ((k w a t) 0) ((t o r) 0) ((d i) 0) ((tS E1) 1) ((z i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "5") (list "quindicesimo -T ((k w i n) 0) ((d i) 0) ((tS E1) 1) ((z i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "6") (list "sedicesimo -T ((s e) 0) ((d i) 0) ((tS E1) 1) ((z i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "7") (list "diciassettesimo -T ((d i) 0) ((tS a s) 0) ((s e t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "8") (list "diciottesimo -T ((d i) 0) ((tS o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	 ((string-equal (car (cdr digits)) "9") (list "diciannovesimo -T ((d i) 0) ((tS a n) 0) ((n o) 0) ((v E1) 1) ((z i) 0) ((m o) 0)"))))
     
       ((string-equal (car digits) "2");; 2x
	(cond ((string-equal (car (cdr digits)) "0") (list "ventesimo -T ((v e n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))     ;;; con la E aperta andrebbe
		((string-equal (car (cdr digits)) "1") (list "ventunesimo -T ((v e n) 0) ((t u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "ventottesimo -T ((v e n) 0) ((t o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	    (t (list (un_str_num "venti -T ((v e n) 0) ((t i) 0)" (creastringa (last_ordinal (cdr digits))))))))

       ((string-equal (car digits) "3");; 2x
       	(cond 	((string-equal (car (cdr digits)) "0") (list "trentesimo -T ((t r e n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
       		((string-equal (car (cdr digits)) "1") (list "trentunesimo -T ((t r e n) 0) ((t u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "trentottesimo -T ((t r e n) 0) ((t o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	    (t (list (un_str_num "trenta -T ((t r e n) 0) ((t a) 0)" (creastringa (last_ordinal (cdr digits))))))))

       ((string-equal (car digits) "4");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "quarantesimo -T ((k w a) 0) ((r a n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "1") (list "quarantunesimo -T ((k w a) 0) ((r a n) 0) ((t u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "quarantottesimo -T ((k w a) 0) ((r a n) 0) ((t o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	    (t (list (un_str_num "quaranta -T ((k w a) 0) ((r a n) 0) ((t a) 0)" (creastringa (last_ordinal (cdr digits))))))))

       ((string-equal (car digits) "5");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "cinquantesimo -T ((tS i ng) 0) ((k w a n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "1") (list "cinquantunesimo -T ((tS i ng) 0) ((k w a n) 0) ((t u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "cinquantottesimo -T ((tS i ng) 0) ((k w a n) 0) ((t o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	    (t (list (un_str_num "cinquanta -T ((tS i ng) 0) ((k w a n) 0) ((t a) 0)" (creastringa (last_ordinal (cdr digits))))))))

       ((string-equal (car digits) "6");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "sessantesimo -T ((s e s) 0) ((s a n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "1") (list "sessantunesimo -T ((s e s) 0) ((s a n) 0) ((t u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "sessantottesimo -T ((s e s) 0) ((s a n) 0) ((t o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	    (t (list (un_str_num "sessanta -T ((s e s) 0) ((s a n) 0) ((t a) 0)" (creastringa (last_ordinal (cdr digits))))))))

       ((string-equal (car digits) "7");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "settantesimo -T ((s e t) 0) ((t a n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "1") (list "settantunesimo -T ((s e t) 0) ((t a n) 0) ((t u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "settantottesimo -T ((s e t) 0) ((t a n) 0) ((t o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	    (t (list (un_str_num "settanta -T ((s e t) 0) ((t a n) 0) ((t a) 0)" (creastringa (last_ordinal (cdr digits))))))))

       ((string-equal (car digits) "8");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "ottantesimo -T ((o t) 0) ((t a n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "1") (list "ottantunesimo -T ((o t) 0) ((t a n) 0) ((t u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "ottantottesimo -T ((o t) 0) ((t a n) 0) ((t o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	    (t (list (un_str_num "ottanta -T ((o t) 0) ((t a n) 0) ((t a) 0)" (creastringa (last_ordinal (cdr digits))))))))

       ((string-equal (car digits) "9");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "novantesimo -T ((n o) 0) ((v a n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "1") (list "novantunesimo -T ((n o) 0) ((v a n) 0) ((t u) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "novantottesimo -T ((n o) 0) ((v a n) 0) ((t o t) 0) ((t E1) 1) ((z i) 0) ((m o) 0)"))
	    (t (list (un_str_num "novanta -T ((n o) 0) ((v a n) 0) ((t a) 0)" (creastringa (last_ordinal (cdr digits))))))))

       ))

     ((equal? l 3);; in the hundreds
	(cond 	((and (string-equal (car digits) "0"))
			(italian_num_token_ordinal (cdr digits)))
		((and (string-equal (car digits) "1") (string-equal (car (cdr digits)) "0") (string-equal (car (cdr (cdr digits))) "0")) 			(append (list "centesimo -T ((tS e n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)") (italian_num_token_ordinal (cdr digits))))
		((string-equal (car digits) "1") 
			(append (list "cento -T ((tS e1 n) 1) ((t o) 0)") (italian_num_token_ordinal (cdr digits))))
		((and (string-equal (car (cdr digits)) "0") (string-equal (car (cdr (cdr digits))) "0"))
			(append (italian_num_token_normal (list (car digits))) (list "centesimo -T ((tS e n) 0) ((t E1) 1) ((z i) 0) ((m o) 0)")))
		(t (append (italian_num_token_normal (list (car digits))) 
	(list "cento -T ((tS e1 n) 1) ((t o) 0)") 
		(italian_num_token_ordinal (cdr digits)))))   
	)

     ((< l 7)
      (let ((sub_thousands 
	     (list 
	      (car (cdr (cdr (reverse digits))))
	      (car (cdr (reverse digits)))
	      (car (reverse digits))))
	    (thousands (reverse (cdr (cdr (cdr (reverse digits)))))))
	
	(set! x (italian_num_token_normal thousands))
	(append 
	 (cond 	((and (equal? l 4) (string-equal  (car (reverse thousands)) "1") (string-equal (car sub_thousands) "0") (string-equal (car (cdr sub_thousands)) "0") (string-equal (car (cdr (cdr sub_thousands))) "0")) 
	 		(list "millesimo -T ((m i l) 0) ((l E1) 1) ((z i) 0) ((m o) 0)"))
	 	((and (equal? l 4) (string-equal  (car (reverse thousands)) "1")) 
	 		(list "mille -T ((m i1 l) 1) ((l e) 0)")) 
	 	((and (string-equal (car sub_thousands) "0") (string-equal (car (cdr sub_thousands)) "0") (string-equal (car (cdr (cdr sub_thousands))) "0")) 
	 		(append x (list "millesimo -T ((m i l) 0) ((l E1) 1) ((z i) 0) ((m o) 0)")))
	 	(t
	 		(append x (list "mila -T ((m i1) 1) ((l a) 0)"))))
	 (italian_num_token_ordinal sub_thousands))))
; Fabio fin qui
     ((< l 13)
      (let ((sub_million 
	     (list 
	      (car (cdr (cdr (cdr (cdr (cdr(reverse digits)))))))
	      (car (cdr (cdr (cdr (cdr (reverse digits))))))
	      (car (cdr (cdr (cdr (reverse digits)))))
	      (car (cdr (cdr (reverse digits))))
	      (car (cdr (reverse digits)))
	      (car (reverse digits))
	      ))
	    (millions (reverse (cdr (cdr (cdr (cdr (cdr (cdr (reverse digits))))))))))
	(set! x (italian_num_token_normal millions))
	
	(append
	 (cond 	((and (equal? l 7) (string-equal (car (reverse millions)) "1") (string-equal (car sub_million) "0") (string-equal (car (cdr sub_million)) "0") (string-equal (car (cdr (cdr sub_million))) "0") (string-equal (car (cdr (cdr (cdr sub_million)))) "0") (string-equal (car (cdr (cdr (cdr (cdr sub_million))))) "0") (string-equal (car (cdr (cdr (cdr (cdr (cdr sub_million)))))) "0") ) 
	     		(list "un -T ((u1 n) 1)" "milionesimo -T ((m i) 0) ((l j o) 0) ((n E1) 1) ((z i) 0) ((m o) 0)"))
	     	((and (equal? l 7) (string-equal (car sub_million) "0") (string-equal (car (cdr sub_million)) "0") (string-equal (car (cdr (cdr sub_million))) "0") (string-equal (car (cdr (cdr (cdr sub_million)))) "0") (string-equal (car (cdr (cdr (cdr (cdr sub_million))))) "0") (string-equal (car (cdr (cdr (cdr (cdr (cdr sub_million)))))) "0") ) 
	     		(append x (list "milionesimo -T ((m i) 0) ((l j o) 0) ((n E1) 1) ((z i) 0) ((m o) 0)")))
	     	((and (equal? l 7) (string-equal (car (reverse millions)) "1"))
	     		(list "un -T ((u1 n) 1)" "milione -T ((m i) 0) ((l j o1) 1) ((n e) 0)"))
	     	(t
	     		(append x (list "milioni -T ((m i) 0) ((l j o1) 1) ((n i) 0)"))))
	 (italian_num_token_ordinal sub_million))))
   (t
      (list "un" "numero" "davvero" "esageratesimo" )))))
      
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (italian_num_token_normal digits)
"(italian_num_token_normal digits)
Trasforma una lista di numeri nella trascrizione ordinale"
  
  (let ((l (length digits))) ;numeri cardinali
    (cond
     ((equal? l 0)
      nil)
     ((string-equal (car digits) "0")
      (italian_num_token_normal (cdr digits)))
     ((equal? l 1);; single digit
      (cond 
       ((string-equal (car digits) "0") (list "zero -T ((dz E1) 1) ((r o) 0)" ))
       ((string-equal (car digits) "1") (list "uno -T ((u1) 1) ((n o) 0)"))
       ((string-equal (car digits) "2") (list "due -T ((d u1) 1) ((e) 0)" ))
       ((string-equal (car digits) "3") (list "tre -T ((t r E1) 1)"))
       ((string-equal (car digits) "4") (list "quattro -T ((k w a1 t) 1) ((t r o) 0)"))
       ((string-equal (car digits) "5") (list "cinque -T ((tS i1 ng) 1) ((k w e) 0)"))
       ((string-equal (car digits) "6") (list "sei -T ((s E1) 1) ((i) 0)"))
       ((string-equal (car digits) "7") (list "sette -T ((s E1 t) 1) ((t e) 0)"))
       ((string-equal (car digits) "8") (list "otto -T ((O1 t) 1) ((t o) 0)"))
       ((string-equal (car digits) "9") (list "nove -T ((n O1) 1) ((v e) 0)"))
       ;; fill in the rest
       (t (list "equis"))));; $$$ what should say?
     ((equal? l 2);; less than 100
      (cond
       ((string-equal (car digits) "0");; 0x
	(italian_num_token_normal (cdr digits)))
     
       ((string-equal (car digits) "1");; 1x
	(cond
	 ((string-equal (car (cdr digits)) "0") (list "dieci -T ((d j E1) 1) ((tS i) 0)"))
	 ((string-equal (car (cdr digits)) "1") (list "undici -T ((u1 n) 1) ((d i) 0) ((tS i) 0)"))
	 ((string-equal (car (cdr digits)) "2") (list "dodici -T ((d o1) 1) ((d i) 0) ((tS i) 0)"))
	 ((string-equal (car (cdr digits)) "3") (list "tredici -T ((t r e1) 1) ((d i) 0) ((tS i) 0)"))
	 ((string-equal (car (cdr digits)) "4") (list "quattordici -T ((k w a t) 0) ((t o1 r) 1) ((d i) 0) ((tS i) 0)"))
	 ((string-equal (car (cdr digits)) "5") (list "quindici -T ((k w i1 n) 1) ((d i) 0) ((tS i) 0)"))
	 ((string-equal (car (cdr digits)) "6") (list "sedici -T ((s e1) 1) ((d i) 0) ((tS i) 0)"))
	 ((string-equal (car (cdr digits)) "7") (list "diciassette -T ((d i) 0) ((tS a s) 0) ((s E1 t) 1) ((t e) 0)"))
	 ((string-equal (car (cdr digits)) "8") (list "diciotto -T ((d i) 0) ((tS O1 t) 1) ((t o) 0)"))
	 ((string-equal (car (cdr digits)) "9") (list "diciannove -T ((d i) 0) ((tS a n) 0) ((n O1) 1) ((v e) 0)"))))
     
       ((string-equal (car digits) "2");; 2x
	(cond ((string-equal (car (cdr digits)) "0") (list "venti -T ((v E1 n) 1) ((t i) 0)"))     ;;; con la E aperta andrebbe
		((string-equal (car (cdr digits)) "1") (list "ventuno -T ((v e n) 0) ((t u1) 1) ((n o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "ventotto -T ((v e n) 0) ((t O1 t) 1) ((t o) 0)"))
	    (t (list (un_str_num "venti -T ((v e n) 0) ((t i) 0)" (creastringa (italian_num_token_normal (cdr digits))))))))

       ((string-equal (car digits) "3");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "trenta -T ((t r e1 n) 1) ((t a) 0)"))
		((string-equal (car (cdr digits)) "1") (list "trentuno -T ((t r e n) 0) ((t u1) 1) ((n o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "trentotto -T ((t r e n) 0) ((t O1 t) 1) ((t o) 0)"))
	    (t (list (un_str_num "trenta -T ((t r e n) 0) ((t a) 0)" (creastringa (italian_num_token_normal (cdr digits))))))))

       ((string-equal (car digits) "4");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "quaranta -T ((k w a) 0) ((r a1 n) 1) ((t a) 0)"))
		((string-equal (car (cdr digits)) "1") (list "quarantuno -T ((k w a) 0) ((r a n) 0) ((t u1) 1) ((n o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "quarantotto -T ((k w a) 0) ((r a n) 0) ((t O1 t) 1) ((t o) 0)"))
	    (t (list (un_str_num "quaranta -T ((k w a) 0) ((r a n) 0) ((t a) 0)" (creastringa (italian_num_token_normal (cdr digits))))))))

       ((string-equal (car digits) "5");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "cinquanta -T ((tS i ng) 0) ((k w a1 n) 1) ((t a) 0)"))
		((string-equal (car (cdr digits)) "1") (list "cinquantuno -T ((tS i ng) 0) ((k w a n) 0) ((t u1) 1) ((n o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "cinquantotto -T ((tS i ng) 0) ((k w a n) 0) ((t O1 t) 1) ((t o) 0)"))
	    (t (list (un_str_num "cinquanta -T ((tS i ng) 0) ((k w a n) 0) ((t a) 0)" (creastringa (italian_num_token_normal (cdr digits))))))))

       ((string-equal (car digits) "6");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "sessanta -T ((s e s) 0) ((s a1 n) 1) ((t a) 0)"))
		((string-equal (car (cdr digits)) "1") (list "sessantuno -T ((s e s) 0) ((s a n) 0) ((t u1) 1) ((n o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "sessantotto -T ((s e s) 0) ((s a n) 0) ((t O1 t) 1) ((t o) 0)"))
	    (t (list (un_str_num "sessanta -T ((s e s) 0) ((s a n) 0) ((t a) 0)" (creastringa (italian_num_token_normal (cdr digits))))))))

       ((string-equal (car digits) "7");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "settanta -T ((s e t) 0) ((t a1 n) 1) ((t a) 0)"))
		((string-equal (car (cdr digits)) "1") (list "settantuno -T ((s e t) 0) ((t a n) 0) ((t u1) 1) ((n o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "settantotto -T ((s e t) 0) ((t a n) 0) ((t O1 t) 1) ((t o) 0)"))
	    (t (list (un_str_num "settanta -T ((s e t) 0) ((t a n) 0) ((t a) 0)" (creastringa (italian_num_token_normal (cdr digits))))))))

       ((string-equal (car digits) "8");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "ottanta -T ((o t) 0) ((t a1 n) 1) ((t a) 0)"))
		((string-equal (car (cdr digits)) "1") (list "ottantuno -T ((o t) 0) ((t a n) 0) ((t u1) 1) ((n o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "ottantotto -T ((o t) 0) ((t a n) 0) ((t O1 t) 1) ((t o) 0)"))
	    (t (list (un_str_num "ottanta -T ((o t) 0) ((t a n) 0) ((t a) 0)" (creastringa (italian_num_token_normal (cdr digits))))))))

       ((string-equal (car digits) "9");; 2x
	(cond 	((string-equal (car (cdr digits)) "0") (list "novanta -T ((n o) 0) ((v a1 n) 1) ((t a) 0)"))
		((string-equal (car (cdr digits)) "1") (list "novantuno -T ((n o) 0) ((v a n) 0) ((t u1) 1) ((n o) 0)"))
		((string-equal (car (cdr digits)) "8") (list "novantotto -T ((n o) 0) ((v a n) 0) ((t O1 t) 1) ((t o) 0)"))
	    (t (list (un_str_num "novanta -T ((n o) 0) ((v a n) 0) ((t a) 0)" (creastringa (italian_num_token_normal (cdr digits))))))))

       ))

     ((equal? l 3);; in the hundreds
	(cond ((string-equal (car digits) "1") (append (list "cento -T ((tS e1 n) 1) ((t o) 0)") (italian_num_token_normal (cdr digits))))
		(t (append (italian_num_token_normal (list (car digits))) 
		(list "cento -T ((tS e1 n) 1) ((t o) 0)") 
		(italian_num_token_normal (cdr digits)))))   
	)

     ((< l 7)
      (let ((sub_thousands 
	     (list 
	      (car (cdr (cdr (reverse digits))))
	      (car (cdr (reverse digits)))
	      (car (reverse digits))))
	    (thousands (reverse (cdr (cdr (cdr (reverse digits)))))))
	(set! x (italian_num_token_normal thousands))
	(append
	 (if (and (equal? l 4) (string-equal  (car (reverse thousands)) "1")) (list "mille -T ((m i1 l) 1) ((l e) 0)") (append x (list "mila -T ((m i1) 1) ((l a) 0)")))

	 (italian_num_token_normal sub_thousands))))
; Fabio fin qui
     ((< l 13)
      (let ((sub_million 
	     (list 
	      (car (cdr (cdr (cdr (cdr (cdr(reverse digits)))))))
	      (car (cdr (cdr (cdr (cdr (reverse digits))))))
	      (car (cdr (cdr (cdr (reverse digits)))))
	      (car (cdr (cdr (reverse digits))))
	      (car (cdr (reverse digits)))
	      (car (reverse digits))
	      ))
	    (millions (reverse (cdr (cdr (cdr (cdr (cdr (cdr (reverse digits))))))))))
	(set! x (italian_num_token_normal millions))
	(append
	 (if (and (equal? l 7) (string-equal  (car (reverse millions)) "1")) 
	     (list "un -T ((u1 n) 1)" "milione -T ((m i) 0) ((l j o1) 1) ((n e) 0)") 
	     (append x (list "milioni -T ((m i) 0) ((l j o1) 1) ((n i) 0)")))
	 (italian_num_token_normal sub_million))))

     (t
      (list "un" "numero" "davvero" "esagerato" )))))
      


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ATT: aggiungo i define che mi servono per date ore e n� telefono
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(define (creastringa listfon)
"Crea una srtinga da una lista (creastringa listfon)"

;	(let (stringa)
	(set! stringa "")
	(while listfon
		(set! stringa (string-append stringa (car listfon)))
		(set! listfon (cdr listfon)))
	(set! stringa stringa)); ) ;;; ho aggiunto la parentesi del "let" e l'ho anche tolta perche'
                                 ;;; con il let mi aggiungeva il nil a inizio parola

(define (mangia_apostrofi token)
	(list (string-append (string-before token "'") "'") (string-after token "'")))

(define (un_str_num s1 s2)
"Unisce due stringhe tipo num "
(set! true_word_str_1 (string-before s1 " -T"))
(set! true_word_str_2 (string-before s2 " -T"))
(set! trasc_word_str_1 (string-after s1 " -T "))
(set! trasc_word_str_2 (string-after s2 " -T "))
(set! str_out (string-append true_word_str_1 true_word_str_2 " -T " trasc_word_str_1 " " trasc_word_str_2))
)


(define (mangia_acronimi str)
"Modulo che tratta gli acronimi, guarda se togliendo i punti trova la pronuncia nel lessico sotto gli acronimi altrimenti pronuncia 
lettera per lettera"
;(print str) 
;(print (lex.lookup (string-remove str ".") 'token))

(if (string-equal (car (cdr (lex.lookup (string-remove str ".") 'token))) 'ACR) 
(list (string-remove str ".")) 
(let (lista atom)
	(while (string-matches str ".*\\.+.*")
	(set! atom (string-before str "."))
	(if (string-matches atom ".*[0-9].*")
		(set! lista (append lista (mangia_numeri_token atom) ))
		(set! lista (append lista (list atom ))))
	(set! str (string-after str ".")))
	(if (string-matches str ".*[0-9].*")
		(set! lista (append lista (mangia_numeri_token str)))
		(set! lista (append lista (list str)))))))
		
(define (mangia_www str)
(let (lista atom)
	(while (string-matches str ".*\\.+.*")
	(set! atom (string-before str "."))
	(if (or (string-matches atom ".*[0-9].*") (and (not (string-matches atom ".*[aeiou�������].*")) (not (string-equal atom "www"))))
		(set! lista (append lista (mangia_numeri_token atom) (list ".")))
		(set! lista (append lista (list atom "." ))))
	(set! str (string-after str ".")))
	(if (or (string-matches str ".*[0-9].*") (and (not (string-matches str ".*[aeiou�������].*")) (not (string-equal str "www"))))
		(set! lista (append lista (mangia_numeri_token str)))
		(set! lista (append lista (list str))))))


(define (mangia_numeri_token oradatatel)

	;(print oradatatel)
	(set! num1 (lts.apply oradatatel 'ora_data_tel_token))
	;(print num1)
	(set! num2 (lts.apply num1 'ora_data_tel2_token))
	;(print num2)
	(set! ordt (lts.apply num2 'ora_data_tel3_token))
     	;(print ordt)

(set! listafinale)
(set! numero)
(let ()
	(while (car ordt)
		(if (or (string-matches (car ordt) "[0-9]") (string-matches (car ordt) "oRd"))
			
			(set! numero (append numero (list (car ordt))))
				
			(let ()
			(if numero 
				(set! listafinale (append listafinale (italian_number_from_digits_token numero))))
			(set! numero)
			(set! listafinale (append listafinale (list (creastringa (list (car ordt))))))
			))
		(set! ordt (cdr ordt))))
	(if numero 
	(set! listafinale (append listafinale (italian_number_from_digits_token numero))))
;(print listafinale)
listafinale
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (italian_token_to_words token name)
  "(italian_token_to_words TOKEN NAME)
Returns a list of words for the NAME from TOKEN.  This primarily
allows the treatment of numbers, money etc."
;(print token)
;(print name)


  (cond
   ;((string-matches name ".+'.+");caso parole con apostrofi
   ;(mangia_apostrofi (apply string-append (lts.apply name 'italian_downcase))))
      
   ((string-matches name "www\\(\\.\\)+.*")
   (let ()
   	(item.set_feat token "pos" "Internet_Adress")
   	;(print "Indirizzo Internet")
   	(mangia_www (apply string-append (lts.apply name 'italian_downcase)))))
   ((string-matches (apply string-append (lts.apply name 'italian_downcase)) "\\([a-z]\\(\\.\\)\\)+[a-z]")
   (let ()
   	(item.set_feat token "pos" "Acronimo")
   	;(print "Acronimo")
   	;(print (apply string-append (lts.apply name 'italian_downcase)))
   	(mangia_acronimi (apply string-append (lts.apply name 'italian_downcase)))))
      	
   ((string-matches name ".*[0-9].*")
    (let ()
    ;(print name)
    (item.set_feat token "pos" "Num+Alfa")
    ;(print "Num+Alfa")
    
    (cond 
        ((string-matches name ".+\-.+") ;caso numeri elencati con il meno
        (let ()
        ;(print "num-num")
        (set! e1 (string-before name "-"))
    	(set! e2 (string-after name "-"))
    	;(print (append (mangia_numeri_token (apply string-append (lts.apply e1 'italian_downcase))) (mangia_numeri_token (apply string-append (lts.apply e2 'italian_downcase)))))
    	(append (mangia_numeri_token (apply string-append (lts.apply e1 'italian_downcase))) (mangia_numeri_token (apply string-append (lts.apply e2 'italian_downcase))))))
    
    	((string-matches name ".+'.+") ;caso parole con apostrofi
    		(let ()
    		 ;(print "apo+num") 
    		 (set! e1 (string-append (string-before name "'") "'")) 
    		 (set! e2 (string-after name "'"))
    		 ;(print (append (list e1) (mangia_numeri_token (apply string-append (lts.apply e2 'italian_downcase)))))
    		 (append (list e1) (mangia_numeri_token (apply string-append (lts.apply e2 'italian_downcase))))))  	
        (t 
        	(mangia_numeri_token (apply string-append (lts.apply name 'italian_downcase)))))))
   
   ((not (string-matches (apply string-append (lts.apply name 'italian_downcase)) ".*[aeiou�������].*"))
   (let ()
   	(item.set_feat token "pos" "Spell_No_Vowel")
   	;(print "Spell_No_Vowel")
        ;(mangia_numeri_token (apply string-append (lts.apply name 'italian_downcase))))); questo era il vecchio modo
        (lts.apply name 'italian_downcase)))
     
   
   ;per il caso delle parole multiple unite con il meno
    ((string-matches name ".+\-.+")
     (let ()
       	(item.set_feat token "pos" "multi-word_meno")
   	;(print "Multi-Word_Meno")
     	(set! e1 (string-before name "-"))
    	(set! e2 (string-after name "-"))
    	(list (apply string-append (lts.apply e1 'italian_downcase)) (apply string-append (lts.apply e2 'italian_downcase)))))
    	
   
   ((or (string-matches name ".*[!-&]+.*") (string-matches name ".*[(-/]+.*") (string-matches name ".*[:-@]+.*") (string-matches name ".*[\[-_]+.*") (string-matches name ".*[{-~]+.*") (string-matches name ".*[��]+.*"))
   (let ()
    ;(print "Punteggiatura Isolata e altri demoni")
    (lts.apply name 'italian_downcase)))
   
     
   ((not (lts.in.alphabet name 'italian_downcase))
    ;; It contains some other than the lts can deal with
    (let ((subwords))
      (print "Simboli non inseriti in  italian_downcase");;;guarda gli altri synth
      (item.set_feat token "pos" "nn")
      (mapcar
       (lambda (letter)
	 ;; might be symbols or digits
	 (set! subwords
	       (append
		subwords
		(cond
		 ((string-matches letter "[0-9]")
		  (italian_number letter))
		 ((string-matches letter "[A-Z�������]")
		    (italian_downcase letter))
		 (t
		  (lts.apply letter 'italian_downcase))))))
       (symbolexplode name))
      subwords))
   (t
    (let ()
    ;(print "Normal")
    (list (apply string-append (lts.apply name 'italian_downcase)))))))

(provide 'italian_token)

