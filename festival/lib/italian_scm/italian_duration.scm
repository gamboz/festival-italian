;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;                Istituto di Fonetica e Dialettologia                    ;;
;;                 Consiglio Nazionale delle Ricerche                     ;;
;;                            Padova Italy                                ;;
;;                        Copyright (c) 2000                              ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IFD-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        						  ;;
;;           Centro per la ricerca scientifica e tecnologica              ;;
;;         	             Povo (TN) - Italy				  ;;
;;			    Copyright (c) 2000                            ;;
;;									  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<--IRST-->;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Regole di Durata 
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Cart-tree dei coefficienti di variazione della durata dei fonemi 
;;;

(set! italian_dur_tree
 '
 ((name is #)
 ((p.name is 0);inizio 
  ((0.1))
  ((p.R:SylStructure.parent.parent.pbreak is BB)
   ((3.0))  
   ((p.R:SylStructure.parent.parent.pbreak is B)
      ((2.0))
      ((p.R:SylStructure.parent.parent.pbreak is mB)
       ((1.0))
       ((0.2))))));questa 0.2 � per pause prima di tutte FW, ma non viene usata ora.      
   ((R:SylStructure.parent.R:Syllable.p.syl_break > 1 ) ;; clause initial inizio frase
    ((R:SylStructure.parent.stress is 1)
     ((R:SylStructure.parent.parent.FW is content)
      ((1.5))
      ((1.2)))
     ((1.2)))
    ((R:SylStructure.parent.syl_break > 1)   ;; clause final fine frase
     ((R:SylStructure.parent.stress is 1)
      ((R:SylStructure.parent.parent.FW is content)
       ((1.5))
       ((1.2)))
      ((1.2)))
     ((R:SylStructure.parent.stress is 1)
      ((R:SylStructure.parent.parent.FW is content)
       ((ph_vc is +)
        ((1.2))
        ((1.0)))
       ((1.0)))
      ((1.0)))))))


   
   ;;;;;;;TIPO FONEMI
   ;; VOCALI
   ;; VOCALI ACCENTATE
    ;;come e1
    ;;come o1
   ;; SEMICONSONATI
   ;; OCCLUSIVE
   ;; FRICATIVE
   ;; AFFRICATE
   ;; NASALI
    ;;come n
    ;;come n
   ;; LIQUIDE
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                               ;; 
;; Durate in secondi dei singoli fonemi. 					 ;;
;; RAI_BEST			                                                 ;;
;;                                                                               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(set! italian_pc_phone_data_RAI_BEST
'(

   ;; SILENZIO

   (# 0.0 0.250)

   ;; VOCALI
   ( i 0.0 0.054020 )
( e 0.0 0.057566 )
( E 0.0 0.084771 )
( a 0.0 0.056224 )
( o 0.0 0.059528 )
( O 0.0 0.098561 )
( u 0.0 0.064850 )
    ;; VOCALI ACCENTATE

( i1 0.0 0.091067 )
( e1 0.0 0.083307 )
( E1 0.0 0.083307 );;come e1
( a1 0.0 0.105688 )
( o1 0.0 0.095902 )
( O1 0.0 0.095902 );;come o1
( u1 0.0 0.084333 )
    ;; SEMICONSONATI
( j 0.0 0.055193 )
( w 0.0 0.046978 )
    ;; OCCLUSIVE
( p 0.0 0.076030 )
( t 0.0 0.072617 )
( k 0.0 0.078765 )
( b 0.0 0.072836 )
( d 0.0 0.073897 ) ;;0.063897 )
( g 0.0 0.065447 )
   ;; FRICATIVE
( f 0.0 0.096601 )
( s 0.0 0.085388 )
( S 0.0 0.119017 )
( v 0.0 0.056631 )
( z 0.0 0.074726 )
( Z 0.0 0.047513 )
  ;; AFFRICATE
( ts 0.0 0.121779 )
( tS 0.0 0.113838 )
( dz 0.0 0.093175 )
( dZ 0.0 0.089604 )
  ;; NASALI
( m 0.0 0.063178 )
( n 0.0 0.051876 )
( J 0.0 0.096601 )
( nf 0.0 0.051876 ) ;;come n
( ng 0.0 0.051876 ) ;;come n
  ;; LIQUIDE
( l 0.0 0.049613 )
( L 0.0 0.090888 )
( r 0.0 0.046175 ) ;;0.036175 )
))
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                               ;; 
;; Durate in secondi dei singoli fonemi. 					 ;;
;; PHONE_1			                                                 ;;
;;                                                                               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(set! italian_pc_phone_data_PHONE_1
'(

   ;; SILENZIO

   (# 0.0 0.250)

   ;; VOCALI
( i 0.0 0.080156 )
( e 0.0 0.085486 )
( E 0.0 0.121719 )
( a 0.0 0.082946 )
( o 0.0 0.088932 )
( O 0.0 0.142759 )
( u 0.0 0.076568 )
   ;; VOCALI ACCENTATE
( i1 0.0 0.155388 )
( e1 0.0 0.113172 )
( E1 0.0 0.113172 ) ;;come e1
( a1 0.0 0.143442 )
( o1 0.0 0.133890 )
( O1 0.0 0.133890 ) ;;come o1
( u1 0.0 0.102053 )
     ;; SEMICONSONATI
( j 0.0 0.075669 )
( w 0.0 0.060802 )
   ;; OCCLUSIVE
( p 0.0 0.093604 )
( t 0.0 0.094833 )
( k 0.0 0.099231 )
( b 0.0 0.084757 )
( d 0.0 0.075920 )
( g 0.0 0.076376 )
  ;; FRICATIVE
( f 0.0 0.113369 )
( s 0.0 0.118522 )
( S 0.0 0.145759 )
( v 0.0 0.066930 )
( z 0.0 0.087579 )
( Z 0.0 0.051860 )
  ;; AFFRICATE
( ts 0.0 0.154344 )
( tS 0.0 0.140999 )
( dz 0.0 0.114343 )
( dZ 0.0 0.103459 )
  ;; NASALI
( m 0.0 0.076759 )
( n 0.0 0.071905 )
( J 0.0 0.126993 )
( nf 0.0 0.071905 ) ;;come n
( ng 0.0 0.071905 ) ;;come n
  ;; LIQUIDE
( l 0.0 0.062560 )
( L 0.0 0.117011 )
( r 0.0 0.046692 )
))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                               ;; 
;; Durate in secondi dei singoli fonemi. 					 ;;
;; RAI 										 ;;
;;                                                                               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(set! italian_pc_phone_data_RAI
'(

   ;; SILENZIO

   (# 0.0 0.250)

;; VOCALI
( i 0.0 0.054031 )
( e 0.0 0.059121 )
( E 0.0 0.084883 )
( a 0.0 0.058380 )
( o 0.0 0.059817 )
( O 0.0 0.098137 )
( u 0.0 0.065620 )
;; VOCALI ACCENTATE
( i1 0.0 0.088281 )
( e1 0.0 0.085122 )
( E1 0.0 0.085122 )  ;;come e1
( a1 0.0 0.105172 )
( o1 0.0 0.091365 )
( O1 0.0 0.091365 )  ;;come o1
( u1 0.0 0.083760 )
;; SEMICONSONATI
( j 0.0 0.046920 )
( w 0.0 0.047307 )
 ;; OCCLUSIVE
( p 0.0 0.079783 )
( t 0.0 0.073662 )
( k 0.0 0.081919 )
( b 0.0 0.076515 )
( d 0.0 0.084341 ) ;;0.064341 )
( g 0.0 0.070379 )
 ;; FRICATIVE
( f 0.0 0.091285 )
( s 0.0 0.080080 )
( S 0.0 0.128325 )
( v 0.0 0.057051 )
( z 0.0 0.073293 )
( Z 0.0 0.049883 )
;; AFFRICATE
( ts 0.0 0.124355 )
( tS 0.0 0.109976 )
( dz 0.0 0.108032 )
( dZ 0.0 0.091393 )
 ;; NASALI
( m 0.0 0.059509 )
( n 0.0 0.049916 )
( J 0.0 0.082751 )
( nf 0.0 0.049916 ) ;;come n
( ng 0.0 0.049916 ) ;;come n

( l 0.0 0.048308 )
( L 0.0 0.076278 )
( r 0.0 0.058771 ) ;;0.038771 )

))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                               ;; 
;; Durate in secondi dei singoli fonemi. 					 ;;
;; APASCII                                               			 ;;
;;                                                                               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(set! italian_pc_phone_data_APASCII
'(

   ;; SILENZIO

   (# 0.0 0.250)

   ;; VOCALI
( i 0.0 0.068668 )
( e 0.0 0.071547 )
( E 0.0 0.108427 )
( a 0.0 0.069062 )
( o 0.0 0.075366 )
( O 0.0 0.126187 )
( u 0.0 0.084663 )
   ;; VOCALI ACCENTATE
( i1 0.0 0.114300 )
( e1 0.0 0.103577 )
( E1 0.0 0.103577 ) ;;uguale e1
( a1 0.0 0.137248 )
( o1 0.0 0.133987 )
( O1 0.0 0.133987 ) ;;uguale o1
( u1 0.0 0.111363 )
   ;; SEMICONSONATI
( j 0.0 0.081939 )
( w 0.0 0.060907 )
   ;; OCCLUSIVE
( p 0.0 0.094294 )
( t 0.0 0.092843 )
( k 0.0 0.098337 )
( b 0.0 0.093296 )
( d 0.0 0.083636 )
( g 0.0 0.083503 )
  ;; FRICATIVE
( f 0.0 0.132089 )
( s 0.0 0.116380 )
( S 0.0 0.152795 )
( v 0.0 0.074393 )
( z 0.0 0.099818 )
( Z 0.0 0.060816 )
  ;; AFFRICATE
( ts 0.0 0.154079 )
( tS 0.0 0.154293 )
( dz 0.0 0.121023 )
( dZ 0.0 0.117698 )
  ;; NASALI
( m 0.0 0.088073 )
( n 0.0 0.069636 )
( J 0.0 0.130018 )
( nf 0.0 0.069636 )
( ng 0.0 0.069636 )
  ;; LIQUIDE
( l 0.0 0.066376 )
( L 0.0 0.125126 )
( r 0.0 0.043980 )
))

   ;; Le GEMINATE vengono trattate come sequenze di due fonemi singoli uguali!
   ;; Per questo le righe sottostanti sono state "commentate".

   ;(tts 0.0 0.080)
   ;(ddz 0.0 0.080)
   ;(ttS 0.0 0.080)
   ;(ddZ 0.0 0.135)
   ;(ng: 0.0 0.080)
   ;(SS 0.0 0.080)
   ;(JJ 0.0 0.080)
   ;(LL 0.0 0.080)

   ;(kk 0.0 0.100)
   ;(bb 0.0 0.080)
   ;(dd 0.0 0.080)
   ;(ff 0.0 0.080)
   ;(ll 0.0 0.080)
   ;(mm 0.0 0.160)
   ;(nn 0.0 0.080)
   ;(pp 0.0 0.080)
   ;(rr 0.0 0.080)
   ;(tt 0.0 0.080)
   ;(vv 0.0 0.080)
   ;(gg 0.0 0.080)
   ;(ss 0.0 0.080)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                               ;; 
;; Durate in secondi dei singoli fonemi. Sono stati ricopiati i dati riguardanti ;;
;; lo spagnolo e per i nuovi fonemi � stata scelta una durata intermedia sempre  ;;
;; uguale. (19/09/2000 A. F.)                                                    ;;
;;                                                                               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(set! italian_pc_phone_data
'(

   ;; SILENZIO

   (# 0.0 0.250)

   ;; VOCALI

   (a 0.0 0.080)
   (e 0.0 0.080)
   (E 0.0 0.080)
   (i 0.0 0.070)
   (o 0.0 0.080)
   (O 0.0 0.080)
   (u 0.0 0.070)

   ;; VOCALI ACCENTATE

   (a1 0.0 0.090)
   (e1 0.0 0.090)
   (E1 0.0 0.090)
   (i1 0.0 0.080)
   (o1 0.0 0.090)
   (O1 0.0 0.090)
   (u1 0.0 0.080)

   ;; SEMIVOCALI O SEMICONSONANTI

   (j 0.0 0.100)
   (w 0.0 0.080)

   ;; OCCLUSIVE

   (p 0.0 0.100)
   (t 0.0 0.085)
   (k 0.0 0.100)
   (b 0.0 0.065)
   (d 0.0 0.060)
   (g 0.0 0.080)

   ;; FRICATIVE

   (f 0.0 0.100)
   (s 0.0 0.110)
   (S 0.0 0.080)
   (v 0.0 0.080)
   (z 0.0 0.080)

   ;; AFFRICATE

   (ts 0.0 0.080)
   (tS 0.0 0.080)
   (dz 0.0 0.080)
   (dZ 0.0 0.135)

   ;; NASALI

   (m 0.0 0.070)
   (n 0.0 0.080)
   (J 0.0 0.110)
   (ng 0.0 0.110)
   (nf 0.0 0.110)

   ;; LIQUIDE

   (l 0.0 0.080)
   (L 0.0 0.080)
   (r 0.0 0.030)   

   ;; VARIE

   (x 0.0 0.130)
   (y 0.0 0.110)
   (Z 0.0 0.080) 
   (M 0.0 0.080)

   ;; Le GEMINATE vengono trattate come sequenze di due fonemi singoli uguali!
   ;; Per questo le righe sottostanti sono state "commentate".

   ;(tts 0.0 0.080)
   ;(ddz 0.0 0.080)
   ;(ttS 0.0 0.080)
   ;(ddZ 0.0 0.135)
   ;(ng: 0.0 0.080)
   ;(SS 0.0 0.080)
   ;(JJ 0.0 0.080)
   ;(LL 0.0 0.080)

   ;(kk 0.0 0.100)
   ;(bb 0.0 0.080)
   ;(dd 0.0 0.080)
   ;(ff 0.0 0.080)
   ;(ll 0.0 0.080)
   ;(mm 0.0 0.160)
   ;(nn 0.0 0.080)
   ;(pp 0.0 0.080)
   ;(rr 0.0 0.080)
   ;(tt 0.0 0.080)
   ;(vv 0.0 0.080)
   ;(gg 0.0 0.080)
   ;(ss 0.0 0.080)

   (+ 0.0 0.080)
   
))

(provide 'italian_duration)
