;;Italian files we need
(require 'italian_phoneset)
(require 'italian_lts)
(require 'italian_lexicon)
(require 'italian_lexicon_irst)
(require 'irst2sampa)
(require 'italian_phrasing)
(require 'italian_token)
(require 'italian_oredatetel)
(require 'italian_gpos)
(require 'italian_module)
(require 'ita_map)
;; mbrola files
(require 'italian_mbrola)
;;Functions
(require 'fdefine)
(require 'functions)
;APML
(Parameter.set 'Emotive_Post_Method 'Emotive_One)
(require 'apml-mode)
;(require 'maps) ;spostato nelle singole voci

(provide 'italian_require)
