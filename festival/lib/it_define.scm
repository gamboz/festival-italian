;;;(print "Sono in C:/Festival/1.4.3/festival/lib/it_define.scm")

; E' per avere le utterance dato il testo
; E' una semplice modifica di SayText per non
; sentire la voce sintetizzata.
; La funzione e' chiamata da Phdur_save

(define (Text2Utt text)
"(Text2Utt TEXT)
TEXT, is converted to utterance."
   (utt.synth (eval (list 'Utterance 'Text text))))

; E' per salvare su file i fonemi data una
; stringa di testo anziche' un file

(define (Phdur_save_text . args)
"Sintetizza la frase e calcola la durata dei fonemi
UTILIZZO: (Phdur_save_text 'text 'filetextout) " 
	(set! txt (nth 0 args))
	(set! fileout (nth 1 args))
	;(print txt)
	(set! u (Text2Utt txt))
	(set! fp (fopen fileout 'w))
	(Phdur u fp)
	(fclose fp))
	
; E' per salvare su file i fonemi dato del
; testo in un file

(define (Phdur_save . args)
"Sintetizza la frase e calcola la durata dei fonemi
UTILIZZO: (Phdur_save 'txt 'filetextout) " 
	(set! fn (nth 0 args))
	(set! fileout (nth 1 args))
	
	(set! fd (fopen fn "r"))
	(set! arr "a")
	(set! txt "")
	(while (fread arr fd)
		(set! txt (string-append txt arr)))
	(fclose fd)
	;(print txt)
	(set! u (Text2Utt txt))
	(set! fp (fopen fileout 'w))

	(Phdur u fp)
	(fclose fp))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (toend item) 
"Applicazione ricorsiva per scorrere dall' inizio alla fine una lista di item"
  (if item
      (begin
       (print (item.name item))

       (toend (item.next item)))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (toend_dur item fp) 
"Applicazione ricorsiva per scorrere dall' inizio alla fine una lista di item"
  (if item
      (begin
       ;(print (item.name item))
       (if (item.prev item)
      		(set! d (- (item.feat item 'end) (item.feat (item.prev item) 'end)))
      		(set! d (item.feat item 'end)))
       ;(print d)
       
       (format fp "%s %1.4f\n" (item.name item) d)
       (toend_dur (item.next item) fp))))
;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (Phdur u fp)
"Data un utterance calcola la durata dei fonemi e stampa nel file di indice festival iterno fp
UTILIZZO  (Phdur u fp)"
	(begin
		(set! i_seg (utt.relation.first u 'Segment))
		(toend_dur i_seg fp)
		)
	)

;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (trascrizione text)
	(set! ut(eval (list 'Utterance 'Text text)))
	(set! utt1 (utt.synth ut))
	(set! seg1 (utt.relation.first utt1 'Segment))
	(toend seg1))
;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (Str_end item)
"Ritorna la trascrizione in stringa di una lista di item fino alla fine"
	(set! tra "")
	(while item . (
  		(set! tra (string-append tra (item.name item)))
  		(set! tra (string-append tra " "))
  		(set! item (item.next item)))
  		)
  	(set! tmp tra)
       	)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (List_end item)
"Ritorna la trascrizione in lista di una lista di item fino alla fine"
	(set! tra ())
	(while item . (
		(set! tra (append tra (list (read-from-string (item.name item)))))
  		(set! item (item.next item)))
  		)
  	(set! tmp tra)
       	)  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
(define (Trascrivi text)
"Ritorna la trascrizione fonetica di text in lista"
	(set! ut(eval (list 'Utterance 'Text text)))
	(set! utt1 (utt.synth ut))
	(set! seg1 (utt.relation.first utt1 'Segment))
	;(List_end seg1)
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;      
(define (Trascrivi_str text)
"Ritorna la trascrizione fonetica di text in stringa (da utilizzare per trascrizione fonetiche lunghe)"
	(set! ut(eval (list 'Utterance 'Text text)))
	(set! utt1 (utt.synth ut))
	(set! seg1 (utt.relation.first utt1 'Segment))
	(Str_end seg1))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
(define (ProvaTraPhones . args)
"Sintesi con durata e frequenza fondamentale costante (dopo aver trascritto il testo): 
Utilizzo: (ProvaTraPhones text [FP_duration FP_F0])
Default = Durata: 100 ms, Frequenza fondamentale: 120 Hz" 
	(if (and (nth 1 args)(nth 2 args)) (begin 
		(print (string-append "Set FP Durata: " (string-append (nth 1 args) (string-append ", Frequenza fondamentale: " (nth 2 args)))))
		(set! FP_duration (nth 1 args))
		(set! FP_F0 (nth 2 args))
		))
	(set! phn (Trascrivi (nth 0 args)))
	(print phn)
	(SayPhones phn)
	(set! FP_duration 100)
	(set! FP_F0 120)
	(print "Frase Phones Sintetizzata."))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (ProvaTraPhonesSave . args)
"Sintesi con durata e frequenza fondamentale costante + save wav (dopo aver trascritto il testo): 
Utilizzo: (ProvaTraPhonesSave text filename [FP_duration FP_F0])
Default = Durata: 100 ms, Frequenza fondamentale: 120 Hz" 
	(if (and (nth 2 args)(nth 3 args)) (begin 
		(print (string-append "Set FP Durata: " (string-append (nth 2 args) (string-append ", Frequenza fondamentale: " (nth 3 args)))))
		(set! FP_duration (nth 2 args))
		(set! FP_F0 (nth 3 args))
		))
	(if (nth 1 args) 
		(begin
			(set! phn (Trascrivi (nth 0 args)))
			(print phn)
			(set! frasephn (utt.synth (eval (list 'Utterance 'Phones phn))))
			(utt.play frasephn)
			(utt.save.wave frasephn (nth 1 args))
			(set! FP_duration 100)
			(set!  FP_F0 120)
			(print (string-append "Frase Phones Sintetizzata. " (string-append "Wave salvata in " (nth 1 args))))
			)
		(begin
			(set! FP_duration 100)
			(set!  FP_F0 120)
			(print "Sintassi Errata")
			)
		)
	)
		;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (ProvaTextSave . args)
"Sintesi con intonazione e prosodia + save wav: 
Utilizzo: (ProvaTextSave text filename)"
	(if (nth 1 args) 
		(begin
			(set! phn (Trascrivi (nth 0 args)))
			;(print phn)
			;(set! frasetxt (utt.synth (eval (list 'Utterance 'Text (nth 0 args)))))
			;(utt.play frasetxt)
			;(print (string-append "Writing file " (string-append (nth 1 args) "...")))
			;(utt.save.wave frasetxt (nth 1 args))
			;(print (string-append "Frase Text Sintetizzata. " (string-append "Wave salvata in " (nth 1 args))))
			)
		(begin
			(print "Sintassi Errata")
			)
		)
	)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (ProvaPhonesSave . args)
"Sintesi con durata e frequenza fondamentale costante + save wav l'input � una lista di fonemi: 
Utilizzo: (ProvaTraPhonesSave phn filename [FP_duration FP_F0])
Default = Durata: 100 ms, Frequenza fondamentale: 120 Hz" 
	(if (and (nth 2 args)(nth 3 args)) (begin 
		(print (string-append "Set FP Durata: " (string-append (nth 2 args) (string-append ", Frequenza fondamentale: " (nth 3 args)))))
		(set! FP_duration (nth 2 args))
		(set! FP_F0 (nth 3 args))
		))
	(if (nth 1 args) 
		(begin
			(set! phn (nth 0 args))
			(print phn)
			(set! frasephn (utt.synth (eval (list 'Utterance 'Phones phn))))
			(utt.play frasephn)
			(utt.save.wave frasephn (nth 1 args))
			(set! FP_duration 100)
			(set!  FP_F0 120)
			(print (string-append "Frase Phones Sintetizzata. " (string-append "Wave salvata in " (nth 1 args))))
			)
		(begin
			(set! FP_duration 100)
			(set!  FP_F0 120)
			(print "Sintassi Errata")
			)
		)
	)
;;;;;;;;;;;;;;;;;;;;;;;;;
(define (tx . args)
"Legge dal file provatxt.txt - oppure da un file fn - la frase da sintetizzare e utilizza intonazione e prosodia, se c'� anche fileout salva la forma d'onda in fileout
Utilizzo: (tx [fn] [fileout])"
	(set! fn (nth 0 args))
	(set! fileout (nth 1 args))
	
	(if fn (set! filename fn)
		(set! filename "provatxt.txt"))
	(set! fd (fopen filename "r"))
	(set! arr "a")
	(set! txt "")
	(while (fread arr fd)
		(set! txt (string-append txt arr)))
	(fclose fd)
	;(print txt)
	(if fileout 
		(Trascrivi txt)
		;(ProvaTextSave txt fileout)
		; Per non fargli sintetizzare la voce l'ho commentato
		;(SayText txt)
		)
	)
;;;;;;;;;;;;;;;;;;;
(define (px . args)
"Legge dal file provaphn.txt-oppure da fn-la frase da sintetizzare e utilizza intonazione e durata costante, se c'� anche fileout salva la forma d'onda in fileout
Utilizzo: (px [fn] [fileout])"
	(set! fn (nth 0 args))
	(set! fileout (nth 1 args))

	(if fn (set! filename fn)
		(set! filename "provaphn.txt"))
	(load filename)
	(print phn)
	(if fileout 
		(ProvaPhonesSave phn fileout)
		(SayPhones phn)
		)
	
	)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (emu_utt u basename)
"Salva in H:/Sintesi/emu/ i file necessari di una utterance per la visualizzazione con emu speech database
UTILIZZO:
(emu UTT BASEFILENAME)"
	(set! dir_emu "H:/Sintesi/emu/")
	(utt.save.segs u (string-append dir_emu (string-append basename ".lab")))
	(utt.save.wave u (string-append dir_emu (string-append basename ".wav")))
	(track.save (item.feat (utt.relation.first u 'f0) 'f0) (string-append dir_emu (string-append basename ".esps")) 'esps)
	(set! command1 (string-append (string-append "C:/CSLU/Festival/1.4.1/speech_tools/main/ch_track.exe H:/Sintesi/emu/" (string-append basename ".esps")) (string-append " -otype ascii -o H:/Sintesi/emu/" (string-append basename ".txt"))))
	(set! command2 (string-append (string-append "C:/CSLU/Festival/1.4.1/speech_tools/main/ch_track.exe H:/Sintesi/emu/" (string-append basename ".txt -s 0.01")) (string-append " -otype ssff -o H:/Sintesi/emu/" (string-append basename ".ssff"))))
	(system command1)
	(set! i_seg (utt.relation.last u 'Segment))
	(set! n (item.feat i_seg 'dur_factor))
	(set! tp (car (cdr (cdr (car italian_pc_phone_data)))));;tempo pausa
	(set! n (- (/ (* tp 3) 0.01) 1))
	(set! fp1 (fopen (string-append dir_emu (string-append basename ".txt")) 'a))
	
	(while (>= n 0)
		(set! n (- n 1))
		(format fp1 "%1.5f \n" 0)
		)
	(fclose fp1)
	(system command2)
)
;;;;;;;;;;;;;;;;;;;;;;;;	
(define (emu text basename)
"Salva in H:/Sintesi/emu/ i file necessari di una utterance per la visualizzazione con emu speech database
UTILIZZO:
(emu TXT BASEFILENAME)"
	
	(set! u (eval (list 'Utterance 'Text text)))
	(set! u (utt.synth u))
	(utt.play u)
	(if (not (member 'f0 (utt.relationnames u)))
      		(targets_to_f0 u))
	(set! dir_emu "H:/Sintesi/emu/")
	(utt.save.segs u (string-append dir_emu (string-append basename ".lab")))
	(utt.save.wave u (string-append dir_emu (string-append basename ".wav")))
	;(utt.save.f0 u (string-append dir_emu (string-append basename ".esps")))
	(track.save (item.feat (utt.relation.first u 'f0) 'f0) (string-append dir_emu (string-append basename ".esps")) 'esps)
	(set! command1 (string-append (string-append "C:/CSLU/Festival/1.4.1/speech_tools/main/ch_track.exe H:/Sintesi/emu/" (string-append basename ".esps")) (string-append " -otype ascii -o H:/Sintesi/emu/" (string-append basename ".txt"))))
	(set! command2 (string-append (string-append "C:/CSLU/Festival/1.4.1/speech_tools/main/ch_track.exe H:/Sintesi/emu/" (string-append basename ".txt -s 0.01")) (string-append " -otype ssff -o H:/Sintesi/emu/" (string-append basename ".ssff"))))
	(system command1)
	(set! i_seg (utt.relation.last u 'Segment))
	(set! n (item.feat i_seg 'dur_factor))
	(set! tp (car (cdr (cdr (car italian_pc_phone_data)))));;tempo pausa
	(set! n (- (/ (* tp 3) 0.01) 1))
	(set! fp1 (fopen (string-append dir_emu (string-append basename ".txt")) 'a))
	
	(while (>= n 0)
		(set! n (- n 1))
		(format fp1 "%1.5f \n" 0)
		)
	(fclose fp1)
	(system command2)
)

(define (sillaba parola)
"Sillaba la parola interponendo - tra ogni sillaba calcolata con lts sillabe 1 e 2" 
 (lts.apply (lts.apply parola 'sillabe1) 'sillabe2))

(define (sillaba_brachets parola_sampa)
"Sillaba la parola_sampa: ingresso lista [es: (k a n e)]; 
l'uscita � strutturata per festival" 
  (italian_tosyl_brackets (lts.apply (lts.apply parola_sampa 'sillabe1) 'sillabe2)))

(define (sillaba_brachets parola_sampa)
"Sillaba la parola_sammpa con lts sillabe 1 e 2: ingresso lista [es: (k a n e)];
l'uscita � strutturata per festival" 
  (italian_tosyl_brackets (lts.apply (lts.apply parola_sampa 'sillabe1) 'sillabe2)))

